$(document).ready(function() {
    var baseUrl = $('input[name="siteUrl"]').val();
	
	$("#province").change(function(e){
		var prov_id = e.target.value;
		console.log(prov_id);
		$.get('getCityByProvince/' + prov_id, function(data, status){
			$('#city').empty();
			$.each(data, function(index, cities){
				$('#city').append('<option value="'+ cities +'">'+ cities +'</option>');
			});
		});
	});
			
	//get client details when selected advance
    $(document).on('change', 'select[name="clients[]"]', function() {
        var selectField = $(this);

        if (selectField.val() == '')
            return false;

        $.get(baseUrl + '/clientDetail/' + selectField.val(), {}, function(response) {
            if (response.client != 'undefined') {
                var client = response.client;
                var formRow = selectField.parents('.main-form-row');
				//alert(JSON.stringify(client));
                formRow.find('.recipientName').html(client["name"]);
                formRow.find('.clientEmail').html(client["email"]);
                formRow.find('.clientPhone').html(client["phone"]);
                formRow.find('.clientAddress').html(client["billing_address"]);
                formRow.find('.clientCity').html(client["city"]);
                formRow.find('.clientProvince').html(client["province"]);

                if (client.zipcode)
                    formRow.find('.clientZipcode').html(' - ' + client["zipcode"]);

                formRow.find('.clientInfo').removeClass('hide');
            }else{
				formRow.find('.clientInfo').classList.add('hide');
			}
        }).fail(function(response) {
            if (response.status == 401)
                window.location.replace(baseUrl + '/auth/login');

            $.notify(
                {icon: 'close', message: 'Gagal mengambil data klien'},
                {type: 'danger', timer: 500, placement: {from: 'top', align: 'center'}}
            );

        });
    });


    //remove invoice
    $(document).on('click', '.removeInvoiceBtn', function() {
        var numrow  = $('.main-form-row').length;
        var formrow = $(this).parents('.main-form-row');

        if (numrow > 1) {
            var invoiceNumber = formrow.find('.tempInvoiceNumber').html();

            if (invoiceNumber) {
                var laraToken   = $('input:hidden[name="_token"]').val();

                $.ajax({
                    method: 'DELETE',
                    url   : baseUrl + '/invoice/new/simple/delete-number',
                    data  : {invoiceNumber: invoiceNumber, _token: laraToken},
                    success: function() {
                        //nothing here
                    }
                });
            }

            formrow.fadeOut(300, function() {
                formrow.remove();
            });
        } else {
            var form = $(this).parents('form');
            form[0].reset();

            form.find('.clientInfo').addClass('hide');
            form.find('.invoice-item-row:not(:first)').remove();
            form.find('.subtotal').html('');
            form.find('.grandtotal').html('');

            form.find('select[name="client[]"]').val('').trigger('change');
        }
    });


    //remove invoice item
    $(document).on('click', '.removeItemBtn', function() {
        var table = $(this).parents('.invoice-item-table');
        var numrow = table.find('.invoice-item-row').length;
        var row = $(this).parents('.invoice-item-row');

        if (numrow > 1) {
            row.fadeOut(300, function() {
                row.remove();
                table.find('input[name="total[]"]:first').trigger('change');
            });

        } else {
            row.find('input').val('');
            row.find('input[name="total[]"]').trigger('change');
        }
    });


    //add invoice item
    $(document).on('click', '.addItemBtn', function() {
        var table = $(this).parents('.invoice-item-table');
        var firstRow = table.find('.invoice-item-row:first');
        var cloneRow = firstRow.clone();
        cloneRow.find('input').val('');

        cloneRow.hide().appendTo( table.find('tbody') ).fadeIn(300);

        //setThousandSeparator( cloneRow.find('.thousand-separator') );    //restore thousandseparator
    });


	//alert(document.getElementById ( "invoiceNumber" ).innerText);
    //calculate total
    $(document).on('change', 'input[name="total[]"], input[name="discount[]"]', function() {
        var subtotal   = 0;
        var parent = $(this).parents('table');
        var totalRows = parent.find('input[name="total[]"]');
		
        totalRows.each(function() {
            var value = $(this).val();
			
            if (value) {
                value = $(this).val().replace(/\./g, '');
                subtotal += parseInt( value );
            }
        });

        parent.find('.subtotal').html(subtotal);
        

        var discount = parent.find('input[name="discount[]"]').val();

        if (!discount)
            discount = 0;

        //var discountPrice = discount;
        var grandtotal = Math.ceil(subtotal - discount);

        parent.find('.grandTotal').html(grandtotal);
        

    });

    //submit simple invoice one by one using ajax
    $('#simpleInvoiceForm').submit(function(e) {
        e.preventDefault();

        var isDraft = 0;
        $btn = $(this).find('button[type=submit]:focus');
        if($btn.data('draft') == 1) {
            isDraft = 1;
        }

        var formUrl   = $(this).attr('action');
        var laraToken = $('input[name="_token"]').val();
        var ajaxData  = [];  //notice this is array
        var numberOfInvoice = 0;

        $('#simple-invoice-form').each(function(index) {
            var invoiceRow = $(this);
            var invoiceItemRows = invoiceRow.find('.invoice-item-row');
            var data = {};
            numberOfInvoice++;

            data.invoiceItems      = [];
            data.invoiceNumber 	   = $('#invoiceDiv span').html();
            data.invoiceDate       = $('#invoiceDate').val();
            data.dueDate           = $('#invoiceDueDate').val();

            invoiceItemRows.each(function(index) {
                var item = {
                    name       	: $(this).find('input[name="name[]"]').val(),
                    description	: $(this).find('input[name="description[]"]').val(),
                    count		: $(this).find('input[name="countItem[]"]').val(),
					ppn      	: $(this).find('input[name="ppn[]"]').val().replace(/\./g, ''),
					subTotal    : $(this).find('input[name="subTotal[]"]').val().replace(/\./g, ''),
                    total      	: $(this).find('input[name="total[]"]').val().replace(/\./g, '')
                };

                data.invoiceItems.push(item);
            });

            data.discount         	= $(this).find('input[name="discount[]"]').val();
            data.notes            	= $(this).find('textarea[name="notes[]"]').val();
            data.template         	= $(this).find('input[name="template[]"]:checked').val();
            data.recipientName 		= $('#recipientName').html();
            data.recipientEmail 	= $('#clientEmail').html();
            data.recipientPhone 	= $('#clientPhone').html();
            data.recipientAddress 	= $('#clientAddress').html();
            data.recipientProvince	= $('#clientProvince').html();
            data.recipientCity		= $('#clientCity').html();
            data.recipientZipcode	= $('#clientZipcode').html().replace(/-|\s/g,"");
            data.recipientEmailCc 	= $(this).find('input[name="recipientEmailCC[]"]').val();
            data.sendDate 		  	= $('#sendDate').val();
			data.isRecurring      	= $(this).find('input[name="isRecurring[]"]:checked').val();
			data.isSendEmail      	= $(this).find('input[name="isSendEmail[]"]').prop('checked') ? 1 : 0;
			data.isSendSMS      	= $(this).find('input[name="isSendSms[]"]').prop('checked') ? 1 : 0;
			data.invoiceId 			= $('input[name="invoiceId"]').val();
			
            data.isDraft 	= isDraft;
			data._token  	= laraToken;
			data.invTotal	= document.getElementById("subtotal").innerText;
			
			//alert(baseUrl);
			//alert(data.template);
			//alert(JSON.stringify(data));
			
            ajaxData.push(data);
			
        });
		
		$.post( formUrl, ajaxData[0], function(response) {
            if (!response.success) {
                console.log(response);
                swal(
                      'Good job!',
                      'You clicked the button!',
                      'success'
                    )
                return alert("failed");
            }

            let timerInterval
                swal({
                  title: 'Finbox E-Invoicing',
                  html: 'Processing generate e-invoice !!',
                  timer: 2000,
                  onOpen: () => {
                    swal.showLoading()
                    timerInterval = setInterval(() => {
                      swal.getContent().querySelector('strong')
                        .textContent = swal.getTimerLeft()
                    }, 100)
                  },
                  onClose: () => {
                    clearInterval(timerInterval)
                  }
                }).then((result) => {
                  if (
                    // Read more about handling dismissals
                    swal('Your e-invoice has been sent..!',
                          ' ',
                          'success'),
                    location.reload()
                  ) {
                    console.log('I was closed by the timer')
                  }
                })

            

        }).fail(function(response) {
            if (response.status == 401)
				alert("failed 2");
                //window.location.replace(baseUrl + '/auth/login');

        });
    })
	

    // update due date min if invoice date is changed
    $('#invoice-form-invoice-date').on('dp.change', function(e) {
        var currentDate = e.date;
        $('#invoice-form-due-date').data('DateTimePicker').minDate(currentDate.add(1, 'days'));
    });

    // fire at the page load
    var paymentLink = 'http://finpay.id/some';
    updateSmsPreview(paymentLink);

    $('#sms-content').keydown(function(e) {
        var val = $('#sms-preview').val();
        if (val.length >= 160) {
            if (e.which != 8) {
                e.preventDefault();
                return false;
            }
        }
    });

    // sms preview
    $('#sms-content').keyup(function(e) {
        var val = $(this).val();
        // add with payment link
        val = val + ' ' + paymentLink;
        if (val.length > 160) {
            val = val.substring(0, 160);
        }

        updateSmsPreview(val);
    });

    function updateSmsPreview(val) {
        // update statistic
        $('#sms-length').text(val.length);
        $('#sms-preview').val(val);
    }

    // display sms content / preview
    $('#invoice-form-send-sms').on('change', function(){
        if($(this).is(':checked')) {
            $('#sms-content-row').show();
        } else {
            $('#sms-content-row').hide();
        }
    });


    // display invoice send time if later is choosen
    $('input[name="send_time[]"]').on('change', function(){
        var val = $('input[name="send_time[]"]:checked').val();

        if (val == 'later') {
            $('#invoice-form-row').show();
            $('#invoice-form-send_time_date').attr('required', 'required');
        } else {
            $('#invoice-form-row').hide();
            $('#invoice-form-send_time_date').removeAttr('required');
        }
    });

    // UPDATING Invoice scripts
    // 
    // If client is set , trigger change (to fetch data)
    if ($('select[name="client[]"]').val() != '') {
        $('select[name="client[]"]').trigger('change');
    }

    // trigger change to calculate subtotal
    $('input[name="total[]"]:first').trigger('change');
});
