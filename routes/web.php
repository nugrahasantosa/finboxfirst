<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PageController@homePage');

Route::get('/test', function () {
    return view('test');
});

Route::get('/confirmation', function () {
    return view('auth/confirmation');
});

Route::get('/verifikasi', function () {
    return view('auth/verifikasi');
});

Route::get('/homepage', 'PageController@homePage');




//Route::get('ajaxDashboard', 'PageController@ajaxDashboard');

Route::get('/contact_us', 'PageController@contact_us');
Route::get('/faq', 'PageController@faq');

Route::get('auth/logout', 'Auth\LoginController@logout');

// Registration
Route::get('auth/register', 'Auth\RegisterController@register');
Route::post('auth/postRegister', 'Auth\RegisterController@postRegister');
Route::get('auth/confirmation/{token}', 'Auth\RegisterController@confirmation')->name('confirmation');

// Authentication
Route::get('auth/login', 'Auth\LoginController@login')->name('login');
Route::post('auth/postLogin', 'Auth\LoginController@postLogin');

// Password reset link request routes...
Route::get('password/forgot', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/forgot', 'Auth\ForgotPasswordController@sendResetLinkEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

//paymentpage
Route::get('link/{invoiceNumber}', 'PaymentPageController@getPaymentLink');
Route::get('paymentPage/{data}', 'PaymentPageController@index');

//payment method
Route::post('finpaycode', 'PaymentPageController@reqPaymentMethod');
Route::post('paymentOptions', 'PaymentPageController@paymentProcessor');

//send Invoice
Route::get('sendInvoice/{data}', 'InvoiceController@sendInvoice');

Route::group(['middleware' => ['web','auth:web']], function(){
	Route::get('infoBiller/{id}','RegisterController@isComplete');
	Route::get('/ajaxDashboard', 'PageController@ajaxDashboard');
	Route::post('/ajaxDashboard', 'PageController@ajaxDashboard');
	Route::get('profile/langkah1', 'PageController@profileLangkah1')->name('step1');
	Route::get('profile/langkah2', 'PageController@profileLangkah2');

	Route::get('upload','InvoiceController@view');
	Route::post('upload/preview','InvoiceController@previewInvoiceUpload');
	Route::get('mid/{invocieNumber}','PaymentController@getBillerMid');
	
	Route::get('/profile', 'PageController@profileLangkah1');
	Route::get('/akun', 'PageController@ringkasanAkun')->name('ringkasanAkun');
	Route::post('profile/edit', 'PageController@updateRingkasanAkun');
	Route::post('profile/edit/user', 'PageController@updateRingkasanAkunUser');
	Route::post('/akun/edit/password', 'PageController@updatePassword')->name('updatePassword');
	Route::any('/invoiceList', 'InvoiceController@invoiceList');
	Route::any('/invoiceDraf', 'InvoiceController@invoiceDraf');
	Route::get('/previewDraft/{inv_id}', 'InvoiceController@previewDraft');

	// After Login
	Route::get('dashboard', 'PageController@dashboard')->name('dashboard');
	Route::post('register','RegisterController@completeRegistration');
	
	//uploadInvoice
	Route::get('invoiceBulk','PageController@InvoiceBulk');
	Route::post('uploadInvoice','InvoiceController@InvoiceUpload');
	
	//createSimpleInvoice
	Route::post('createSimpleInvoice','InvoiceController@createSimpleInvoice');
	
	
	Route::get('invoiceSatuan','PageController@invoiceSatuan');
	
	Route::get('invoiceSatuan/success','PageController@invoiceSatuanSuccess');
	
	Route::get('invoiceSatuan/fail','PageController@invoiceSatuanFail');
	
	Route::get('client/{billerId}','ClientController@getClient');
	Route::get('clientDetail/{clientId}','ClientController@getCLientById');
	
	Route::get('getCityByProvince/{province}', 'CityController@getByProvince');
});

