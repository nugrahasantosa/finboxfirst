<?php

namespace Finbox\MyClass;

use Log;
use Mail;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;


class MessagingHandler
{
	
	public function sendEmailConfirmation($emailTemplate,$templateParameter,$fromAddress,$fromName,$toAddress,$toName,$subject,$ccAddressesArray){
		try {
            Mail::send($emailTemplate, (array) $templateParameter, function($m) use($fromAddress, $fromName, $toAddress, $toName, $subject, $ccAddressesArray) {
                $m->from($fromAddress, $fromName);
                $m->to($toAddress, $toName);
                $m->subject($subject);

                if ($ccAddressesArray)
                    $m->cc($ccAddressesArray);
				
            });

            Log::info('Email '.$subject.' sent to '.$toAddress);
			return true;
        } catch (\Exception $e) {
            Log::error('Error sending '.$subject.' mail to '.$toAddress.'. Error : '.$e->getMessage());
			
            return $e->getMessage();
        }
		
		if( count(Mail::failures()) > 0 ) {
           $eror =  "There was one or more failures. They were: <br />";
           foreach(Mail::failures as $email_address) {
               $eror = $eror .  " - $email_address <br />";
            }
            return $eror;
        }
		
        return true;
	}
	
    public function sendEmail($emailTemplate, $templateParameter, $fromAddress, $fromName, $toAddress, $toName, $subject, $ccAddressesArray, $attachment = null)
    {
        try {
            Mail::send($emailTemplate, (array) $templateParameter, function($m) use($fromAddress, $fromName, $toAddress, $toName, $subject, $ccAddressesArray, $attachment) {
                $m->from($fromAddress, $fromName);
                $m->to($toAddress, $toName);
                $m->subject($subject);

                if ($ccAddressesArray)
                    $m->cc($ccAddressesArray);

                if ($attachment !== null) {
                    $m->attach($attachment);
                }
            });

            Log::info('Email '.$subject.' sent to '.$toAddress);
        } catch (\Exception $e) {
            Log::error('Error sending email '.$subject.' to '.$toAddress.'. Error : '.$e->getMessage());

            return false;
        }

        return true;
    }
	
	public function sendSMSProd($destPhone, $smsMessage, $guzzleOptions)
    {
        $server    = env('SMS_SERVER_PROD');

        $client = new Client(['verify' => false]);  //bypass https warning

        try {
            $response = $client->request('GET', $server.'/'.$destPhone.'/'.$smsMessage, $guzzleOptions);
            Log::info('SMS Prod sent to '.$destPhone.' with message: '.$smsMessage);
        } catch (\Exception $e) {
            Log::error( 'Error sending SMS Prod (with message: '.$smsMessage.') to '.$destPhone.'. Error : '.$e->getResponse()->getBody()->getContents() );
			//return 'Sms send Failed';
        }

        return true;
    }
	
	public function emailApi($emailTemplate,$data,$fromAddress,$fromName,$toAddress,$toName,$subject,$ccAddressesArray, $attachment = null){
			
		try {
            Mail::send($emailTemplate, $data, function($m) use($fromAddress, $fromName, $toAddress, $toName, $subject, $ccAddressesArray,$attachment) {
                $m->from($fromAddress, $fromName);
                $m->to($toAddress, $toName);
                $m->subject($subject);
				
				Log::info('Email '.$subject.' sent to '.$toAddress);
                
				if ($ccAddressesArray) {
                    $m->cc($ccAddressesArray);
					Log::info('Email '.$subject.' cc to '.$toAddress);
				}
				
				if ($attachment !== null) {
                    $m->attach($attachment);
                }
				
            });
		} catch (\Exception $e) {
            Log::error('Error sending email '.$subject.' to '.$toAddress.'. Error : '.$e->getMessage());

            return $e->getMessage();
		
        }
            
		// Laravel tells us exactly what email addresses failed, let's send back the first
		$fail = Mail::failures();
		if(!empty($fail)) 
			return($fail);

		return true;
	}

}