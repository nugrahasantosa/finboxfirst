<?php

namespace Finbox\Http\Controllers;

use File;
use Input;
use Hash;
use DB;
use Validator;
use Auth;
use Finbox\User;
use Finbox\UserInfo;
use Finbox\Clients;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Carbon\Carbon;

use Finbox\MyClass\CityProvince;

//invoice
use Log;
use Finbox\InvoicePayment;
use Finbox\Client as BillerClient;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;

use Finbox\Http\Requests;
use Finbox\Http\Controllers\Controller;

use Finbox\Http\Controllers\InvoiceController;
use Intervention\Image\ImageManagerStatic as Image;

class PageController extends Controller
{
	protected function dashboard(Request $request)
	{
		$biller_id=Auth::id();
		
		$data['check'] = $this->checkLock();
		//print_r($data['target']);die;
		$data['billersInfo'] = DB::select("
		select biller_id, comp_name, comp_npwp, address, city, province, owner_name, logo, comp_phone, bank_name, bank_acc_no, bank_acc_name, bank_cabang  
		from billers_info where biller_id=:b2",["b2"=>$biller_id]);
		
        return view('dashboard', $data);
    }
	
	protected function ajaxDashboard(Request $request)
	{
        return response()->json(['response' => 'This is get method']);
	}
	
	protected function profileLangkah1(Request $request){
		$biller_id=Auth::id();
		if($this->isComplete()){
			$data['active']=null;
		}else{
			$data['active']	="active";
		}
		$data['check'] = $this->checkLock();
		$data['name'] 	= UserInfo::whereBillerId($biller_id)->first(['comp_name']);
        return view('profile.langkah1',$data);
    }
	protected function contact_us(Request $request)
	{
        return view('contact_us.contact_us');
    }
	
	protected function faq(Request $request)
	{
		$data['check'] = $this->checkLock();
        return view('faq.faq', $data);
    }
	
	
	private function isComplete(){
		$biller_id=Auth::id();
		
		if(!empty(UserInfo::whereBillerId($biller_id)->first()))
			return true;
			
		return null;
	}
	
	private function checkLock(){
		$biller_id=Auth::id();
		if($this->isComplete()){
			$data['lock']	=null;
			$data['modal']	=null;
			$data['target']	=null;
		}else{
			$data['lock']	="lock";
			$data['modal']	="modal";
			$data['target']	=".pullUp-modal";
		}
		return $data;
	}
	
	protected function InvoiceBulk()
	{
		$data['check'] = $this->checkLock();
        return view('invoice.invoice_bulk', $data);
    }
	
	protected function homePage()
	{
        return view('home');
    }
	
	protected function invoiceSatuan(){
		$invoice 		= new InvoiceController();
		$invoiceNumber 	= $invoice->getInvoice(Auth::id());
		$invoiceId 		= $invoice->getInvoiceId();
		
		$check 	= $this->checkLock();
		$client = Clients::whereBillerId(Auth::id())->get();
		
		$clients= [];
		foreach($client as $key){
			$clients[$key->id]= $key->name;
		}
		
		$clients +=['Pilih Client'=>'Pilih Client'];
		$salutation = ['Tn'=>'Tn','Ny'=>'Ny'];
		
		$provinces = CityProvince::getProvinces();
        $cities    = CityProvince::getCities();
        ksort($cities);

        $period=date('m/d/Y');
        $dueDate=date('m/d/Y');
		
		$data=compact('check','invoiceNumber','invoiceId','clients','client','salutation','provinces','cities','period','dueDate');
		
        return view('invoice.invoice_satuan', $data);
    }	
	
	protected function ringkasanAkun(Request $request){
		$biller_id=Auth::id();
		$data['check'] = $this->checkLock();
		if($this->isComplete()){
			$data['active']=null;
			$data['provinces'] = CityProvince::getProvinces();
			$data['cities']    = CityProvince::getCities();
			$data['banks'] = [
			'0' => [
					  'BANK BCA' => 'BANK BCA',
					  'BANK Mandiri' => 'BANK Mandiri',
					  'BANK BRI' => 'BANK BRI',
					  'BANK BNI' => 'BANK BNI',
				]];
			$data['billers'] = DB::select("
			select id, name, address, email, created_at, updated_at, logo
			from billers where id=:b1",["b1"=>$biller_id]);
			
			$data['billersInfo'] = DB::select("
			select biller_id, comp_name, comp_npwp, address, city, province, owner_name, logo, comp_phone, bank_name, bank_acc_no, bank_acc_name, bank_cabang  
			from billers_info where biller_id=:b2",["b2"=>$biller_id]);
			if(!empty($data['billersInfo'][0]->bank_name)){
				$data['bank_name'] = $data['billersInfo'][0]->bank_name;
			}else{
				$data['bank_name'] = null;
			}
			return view('profile.ringkasan_akun',$data);
		}else{
			$data['active']	="active";
			return view('profile.ringkasan_akun_customer',$data);
		}
		
    }
	
	protected function updateRingkasanAkunUser(Request $request){
		$user = new File;
		$biller_id = Auth::id();
		if ($request->isMethod('post')) {
			$validator = Validator::make($request->all(), [
			'name' 				=> 'max:100',
			'email' 			=> 'max:100',
			'address' 			=> 'max:255',
			'img_account'		=> 'image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=1024,min_height=1024',
			]);
		}

			$name 		= $request['name'];
			$email 		= $request['email'];
			$address 	= $request['address'];
			
		if ($validator->fails()) {
			if(Input::hasFile('img_account')){
				$file_img_account 	= Input::File('img_account');
				$img_account		= $file_img_account->getClientOriginalName();
				$file_img_account->move(base_path().'/public/img/privacy/'.$biller_id.'/img_account/',$img_account);
				$update = DB::table('billers')
				->where('id', $biller_id)
				->update(['name' => $name,'email' => $email, 'address' => $address, 'logo'=> $img_account]);
				if($update){
					session()->flash('msg','Profile Updated');
					return \Redirect::route('ringkasanAkun');
				}else{
					session()->flash('incorrect','Failed Update Data !!');
					return \Redirect::route('ringkasanAkun');
				}
			}else{
				$update = DB::table('billers')
				->where('id', $biller_id)
				->update(['name' => $name,'email' => $email, 'address' => $address]);
				if($update){
					session()->flash('msg','Profile Updated');
					return \Redirect::route('ringkasanAkun');
				}else{
					session()->flash('incorrect','Failed Update Data !!');
					return \Redirect::route('ringkasanAkun');
				}
			}
		}else{
			if(Input::hasFile('img_account')){
				$file_img_account 	= Input::File('img_account');
				$img_account		= $file_img_account->getClientOriginalName();
				$image_resize_img_account = Image::make($file_img_account->getRealPath());  	
				$image_resize_img_account->resize(300, 300);
				$image_resize_img_account->save(public_path('/img/privacy/'.$biller_id. '/img_account/' .$img_account));
				$update = DB::table('billers')
				->where('id', $biller_id)
				->update(['name' => $name,'email' => $email, 'address' => $address, 'logo'=> $img_account]);
					if($update){
						session()->flash('msg','Profile Updated');
						return \Redirect::route('ringkasanAkun');
					}else{
						session()->flash('incorrect','Failed Update Data !!');
						return \Redirect::route('ringkasanAkun');
					}
			}else{
				$update = DB::table('billers')
				->where('id', $biller_id)
				->update(['name' => $name,'email' => $email, 'address' => $address]);
				if($update){
					session()->flash('msg','Profile Updated');
					return \Redirect::route('ringkasanAkun');
				}else{
					session()->flash('incorrect','Failed Update Data !!');
					return \Redirect::route('ringkasanAkun');
				}
			}
		}
    }
	
	protected function updateRingkasanAkun(Request $request){
		$user = new File;
		$biller_id = Auth::id();
		$file=Input::File('logo');
		if ($request->isMethod('post')) {
			$validator = Validator::make($request->all(), [
			'name' 				=> 'max:100',
			'address_acc' 		=> 'max:255',
			'owner_name' 		=> 'max:100',
			'email' 			=> 'max:50',
			'comp_name' 		=> 'max:100',
			'address' 			=> 'max:255',
			'province' 			=> 'max:15',
			'city' 				=> 'max:50',
			'comp_phone' 		=> 'max:20',
			'comp_npwp' 		=> 'max:50',
			'bank_name' 		=> 'max:50',
			'bank_acc_no' 		=> 'max:50',
            'bank_cabang' 		=> 'max:50',
			'logo' 				=> 'image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=1024,min_height=1024',
			'img_account'		=> 'image|mimes:jpeg,png,jpg,gif,svg|dimensions:min_width=1024,min_height=1024',
			]);
		}

			$name		 = $request['name'];
			$address_acc = $request['address_acc'];
			$owner_name	 = $request['owner_name'];
			$email 		= $request['email'];
			$comp_name 	= $request['comp_name'];
			$address = $request['address'];
			$province = $request['province'];
			$city = $request['city'];
			$comp_phone = $request['comp_phone'];
			$comp_npwp = $request['comp_npwp'];
			$bank_name = $request['bank_name'];
			$bank_acc_no = $request['bank_acc_no'];
			$bank_cabang = $request['bank_cabang'];
			
		if ($validator->fails()) {
			if(Input::hasFile('logo') || Input::hasFile('img_account')){
				if(Input::hasFile('logo')){
					$file_logo  		= Input::File('logo');
					$logo				= $file_logo->getClientOriginalName();
					$file_logo->move(base_path().'/public/img/privacy/'.$biller_id.'/comp_logo_img/',$logo);
					$update = DB::table('billers_info')
					->where('biller_id', $biller_id)
					->update(['owner_name' => $owner_name, 'comp_name' => $comp_name, 'address' => $address, 'province' => $province, 'city' => $city, 'comp_phone' => $comp_phone, 'comp_npwp' => $comp_npwp, 'bank_name' => $bank_name, 'bank_acc_no' => $bank_acc_no, 'bank_cabang' =>$bank_cabang, 'logo'=> $logo]);
					$update = DB::table('billers')
					->where('id', $biller_id)
					->update(['name' => $name, 'address' => $address_acc, 'email' => $email]);
						if($update){
							session()->flash('msg','Profile Updated');
							return \Redirect::route('ringkasanAkun');
						}else{
							session()->flash('incorrect','Failed Update Data !!');
							return \Redirect::route('ringkasanAkun');
						}
				}
				if(Input::hasFile('img_account')){
					$file_img_account 	= Input::File('img_account');
					$img_account		= $file_img_account->getClientOriginalName();
					$file_img_account->move(base_path().'/public/img/privacy/'.$biller_id.'/img_account/',$img_account);
					$update = DB::table('billers_info')
					->where('biller_id', $biller_id)
					->update(['owner_name' => $owner_name, 'comp_name' => $comp_name, 'address' => $address, 'province' => $province, 'city' => $city, 'comp_phone' => $comp_phone, 'comp_npwp' => $comp_npwp, 'bank_name' => $bank_name, 'bank_acc_no' => $bank_acc_no, 'bank_cabang' =>$bank_cabang]);
					$update = DB::table('billers')
					->where('id', $biller_id)
					->update(['name' => $name, 'address' => $address_acc, 'email' => $email, 'logo'=> $img_account]);
						if($update){
							session()->flash('msg','Profile Updated');
							return \Redirect::route('ringkasanAkun');
						}else{
							session()->flash('incorrect','Failed Update Data !!');
							return \Redirect::route('ringkasanAkun');
						}
				}
			}else{
				$update = DB::table('billers_info')
				->where('biller_id', $biller_id)
				->update(['owner_name' => $owner_name, 'comp_name' => $comp_name, 'address' => $address, 'province' => $province, 'city' => $city, 'comp_phone' => $comp_phone, 'comp_npwp' => $comp_npwp, 'bank_name' => $bank_name, 'bank_acc_no' => $bank_acc_no, 'bank_cabang' =>$bank_cabang]);
				$update = DB::table('billers')
				->where('id', $biller_id)
				->update(['name' => $name, 'address' => $address_acc, 'email' => $email]);
				
				if($update){
					session()->flash('msg','Profile Updated');
					return \Redirect::route('ringkasanAkun');
				}else{
					session()->flash('incorrect','Failed Update Data !!');
					return \Redirect::route('ringkasanAkun');
				}
			}
		}else{
			if(Input::hasFile('logo') || Input::hasFile('img_account')){
				if(Input::hasFile('logo')){
					$file_logo =Input::File('logo');
					$logo				= $file_logo->getClientOriginalName();
					
					$image_resize_logo = Image::make($file_logo->getRealPath());  	
					$image_resize_logo->resize(300, 300);
					$image_resize_logo->save(public_path('/img/privacy/'.$biller_id. '/comp_logo_img/' .$logo));
					$update = DB::table('billers_info')
					->where('biller_id', $biller_id)
					->update(['owner_name' => $owner_name, 'comp_name' => $comp_name, 'address' => $address, 'province' => $province, 'city' => $city, 'comp_phone' => $comp_phone, 'comp_npwp' => $comp_npwp, 'bank_name' => $bank_name, 'bank_acc_no' => $bank_acc_no, 'bank_cabang' =>$bank_cabang, 'logo'=> $logo]);
					$update = DB::table('billers')
					->where('id', $biller_id)
					->update(['name' => $name, 'address' => $address_acc, 'email' => $email]);
						if($update){
							session()->flash('msg','Profile Updated');
							return \Redirect::route('ringkasanAkun');
						}else{
							session()->flash('incorrect','Failed Update Data !!');
							return \Redirect::route('ringkasanAkun');
						}
				}
				if(Input::hasFile('img_account')){
					$file_img_account 	= Input::File('img_account');
					$img_account		= $file_img_account->getClientOriginalName();
					$image_resize_img_account = Image::make($file_img_account->getRealPath());  	
					$image_resize_img_account->resize(300, 300);
					$image_resize_img_account->save(public_path('/img/privacy/'.$biller_id. '/img_account/' .$img_account));
					$update = DB::table('billers_info')
					->where('biller_id', $biller_id)
					->update(['owner_name' => $owner_name, 'comp_name' => $comp_name, 'address' => $address, 'province' => $province, 'city' => $city, 'comp_phone' => $comp_phone, 'comp_npwp' => $comp_npwp, 'bank_name' => $bank_name, 'bank_acc_no' => $bank_acc_no, 'bank_cabang' =>$bank_cabang]);
					$update = DB::table('billers')
					->where('id', $biller_id)
					->update(['name' => $name, 'address' => $address_acc, 'email' => $email, 'logo' => $img_account]);
						if($update){
							session()->flash('msg','Profile Updated');
							return \Redirect::route('ringkasanAkun');
						}else{
							session()->flash('incorrect','Failed Update Data !!');
							return \Redirect::route('ringkasanAkun');
						}
				}
			}else{
				
				$update = DB::table('billers_info')
				->where('biller_id', $biller_id)
				->update(['owner_name' => $owner_name, 'comp_name' => $comp_name, 'address' => $address, 'province' => $province, 'city' => $city, 'comp_phone' => $comp_phone, 'comp_npwp' => $comp_npwp, 'bank_name' => $bank_name, 'bank_acc_no' => $bank_acc_no, 'bank_cabang' =>$bank_cabang]);
				
				$update = DB::table('billers')
				->where('id', $biller_id)
				->update(['name' => $name, 'address' => $address_acc, 'email' => $email]);
				if($update){
					session()->flash('msg','Profile Updated');
					return \Redirect::route('ringkasanAkun');
				}else{
					session()->flash('incorrect','Failed Update Data !!');
					return \Redirect::route('ringkasanAkun');
				}
			}
		}
    }
	
	protected function updatePassword(Request $request){
		$data['check'] = $this->checkLock();
		$oldPassword = $request->oldPassword;
		$biller_id = Auth::id();
		
		if(Hash::check($oldPassword, $request->user()->password)){
			
			$update = DB::table('billers')
				->where('id',$biller_id)
				->update(['password' => Hash::make($request->newPassword)]);
				
			session()->flash('msg','Password Changed');
			return \Redirect::route('ringkasanAkun');
		}else{
			session()->flash('incorrect','Old Password didnt match !');
			return \Redirect::route('ringkasanAkun');
		}
	}
	
	protected function invoiceSatuanSuccess()
	{
		$data['check'] = $this->checkLock();
        return view('invoice.success_invoice', $data);
    }	
	
	protected function invoiceSatuanFail()
	{
		$data['check'] = $this->checkLock();
        return view('invoice.fail_invoice', $data);
    }
	
	protected function invoiceList(Request $request)
	{
		$data['check'] = $this->checkLock();
		
		if($request->has('start') && $request->has('end')){
			$start = $request['start'];
			$end= $request['end'];
		}else{
			$start=date("dmY",strtotime("-7 day"));
			$end=date("dmY",strtotime("+1 day"));
		}
		
		$user_id=Auth::id();
		
		$data['listInvoice']=DB::select("
		select invoice_number, recipient_name, recipient_email,created_at, due_date, total, invoice_status, payment_status, discount  
		from invoices where created_at between to_date(:b1,'ddmmyyyy') and to_date(:b2,'ddmmyyyy') and biller_id=:b3 order by created_at desc",["b1" => $start, "b2"=> $end,"b3"=>$user_id]);
		
		/*$trxInvoice = $data['listInvoice'][0]->invoice_number;
		
		$data['listPayment']= InvoicePayment::whereTrxInvoice($trxInvoice)->first(['fin_method']);*/
		
		$data['start']= $start;
		$data['end']=$end;
        return view('report.invoice_list', $data);
    }
	
	protected function invoiceDraf(Request $request)
	{
		$data['check'] = $this->checkLock();
		
		if($request->has('start') && $request->has('end')){
			$start = $request['start'];
			$end= $request['end'];
		}else{
			$start=date("dmY",strtotime("-7 day"));
			$end=date("dmY",strtotime("+1 day"));
		}
		
		$user_id=Auth::id();
		
		$data['drafInvoice']=DB::select("
		select invoice_number, recipient_name, recipient_email, recipient_phone, due_date, total, invoice_status, payment_status, discount  
		from invoices where payment_status is null and created_at between to_date(:b1,'ddmmyyyy') and to_date(:b2,'ddmmyyyy') and biller_id=:b3 and trim(recipient_email)!='##default##'",["b1" => $start, "b2"=> $end,"b3"=>$user_id]);
		
		$data['start']= $start;
		$data['end']=$end;
        return view('report.invoice_draf', $data);
    }
	
	public function previewDraft($inv_id){
		$data['check'] = $this->checkLock();
		$user_id=Auth::id();
		//get payment link
        if (env('APP_ENV') != 'local') {
            $client    = new Client(['verify' => false]);  //bypass https warning
            $serverUrl = env('PAYMENT_LINK_GENERATOR_URL');

            try {
                $response = $client->request('GET', $serverUrl.'?invoice='.$inv_id, ['proxy' => env('OUTGOING_PROXY_URL')]);
                $response = $response->getBody()->getContents();

                if(!$response) {
                    Log::error('createInvoiceSimple: Error creating Payment Link. Error : Result Null');

                    return response('Gagal membuat Payment Link: Result Null', 500);
                }

                Log::info('createInvoiceSimple: Payment Link Created: '.$response);

            } catch (\Exception $e) {
                Log::error( 'createInvoiceSimple: Error creating Payment Link. Error : '.$e->getMessage() );

                return response('Gagal membuat Payment Link: '.$e->getMessage(), 500);
            }

        } else
            $response = url('invoice/payment/'.$inv_id);
		
		$data['payment_link']=$response;
		
		$data['billerInfo']=DB::select("select a.*, b.terms, b.notes from billers a, billers_terms b where a.id= b.billers_id(+) and a.id=:b1",["b1" => $user_id]);
		
		$data['invoiceInfo']=DB::select("select * from invoices where invoice_number=:b1",["b1" => $inv_id]);
		
		$data['previewInvoice']=DB::select("select * from invoices a, invoice_items b where a.id=b.invoice_id and a.invoice_number=:b1 and a.payment_status is null",["b1" => $inv_id]);
		
		
		return view('invoice.draft.templateTwo',$data);
	}
	
}