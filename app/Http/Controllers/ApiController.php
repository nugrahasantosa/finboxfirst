<?php

namespace Finbox\Http\Controllers;

use Auth;
use Hash;
use Log;
use DB;

use Finbox\User;
use Finbox\Invoice;
use Finbox\MyClass\MessagingHandler as Messaging;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller{
    //
	public function accessToken(Request $request){

       $validate = $this->validations($request,"login");

       if($validate["error"]){
		   return $this->prepareResult(false, [], $validate['errors'],"Error while validating user");
	   }

       $user = User::where("email",$request->email)->first();

       if($user){
		   if (Hash::check($request->password,$user->password)){
			   
			   return $this->prepareResult(true, ["accessToken" => $user->createToken('Todo App')->accessToken], [],"User Verified");
			}else{
				
			   return $this->prepareResult(false, [], ["password" => "Wrong passowrd"],"Password not matched");
			}
			
		}else{
			
			return $this->prepareResult(false, [], ["email" => "Unable to find user"],"User not found");
		}
	}



//Next, I will create a function through which a user can get,create,update and delete todo items.

/**

    * Get a validator for an incoming Todo request.

    *

    * @param  \Illuminate\Http\Request  $request

    * @param  $type

    * @return \Illuminate\Contracts\Validation\Validator

    */
	public function validations($request,$type){

       $errors = [ ];

       $error = false;

       if($type == "login"){
		   
		   $validator = Validator::make($request->all(),[
				'email' => 'required|email|max:255',
				'password' => 'required',
			]);
			
	   if($validator->fails()){
				$error = true;
				$errors = $validator->errors();
			}
			
       }elseif($type == "create todo"){
		   $validator = Validator::make($request->all(),[
			'todo' => 'required',
			'description' => 'required',
			'category' => 'required'
			]);
			
			if($validator->fails()){
				$error = true;
				$errors = $validator->errors();
			}

       }elseif($type == "update todo"){
		   $validator = Validator::make($request->all(),[
				'todo' => 'filled',
				'description' => 'filled',
				'category' => 'filled'
			]);
			
			if($validator->fails()){
				$error = true;
				$errors = $validator->errors();
				
			}

       }

       return ["error" => $error,"errors"=>$errors];
	   
	}
	
	/**

    * Display a listing of the resource.

    *

    * @param  \Illuminate\Http\Request  $request

    * @return \Illuminate\Http\Response

    */

	private function prepareResult($status, $data, $errors,$msg){

       return ['status' => $status,'data'=> $data,'message' => $msg,'errors' => $errors];
	}
	
	
	/**

    * Display a listing of the resource.

    *

    * @param  \Illuminate\Http\Request  $request

    * @return \Illuminate\Http\Response

    */

	public function index(Request $request){
		return $this->prepareResult(true, $request->user()->todo()->get(), [],"All user todos");
	}
	
	
	/**

    * Display the specified resource.

    *

    * @param  \App\Todo  $todo

    * @return \Illuminate\Http\Response

    */

	public function show(Request $request,Todo $todo){

       if($todo->user_id == $request->user()->id){
		   
		   return $this->prepareResult(true, $todo, [],"All results fetched");
		}else{
		   return $this->prepareResult(false, [], "unauthorized","You are not authenticated to view this todo");
		}
	}
	
	/**

    * Store a newly created resource in storage.

    *

    * @param  \Illuminate\Http\Request  $request

    * @return \Illuminate\Http\Response

    */

	public function store(Request $request){
		$error = $this->validations($request,"create todo");
		
		if ($error['error']) {
			return $this->prepareResult(false, [], $error['errors'],"Error in creating todo");
		} else {
			$todo = $request->user()->todo()->Create($request->all());
			
			return $this->prepareResult(true, $todo, $error['errors'],"Todo created");
			
		}
	}
	
	/**

    * Update the specified resource in storage.

    *

    * @param  \Illuminate\Http\Request  $request

    * @param  \App\Todo  $todo

    * @return \Illuminate\Http\Response

    */

	public function update(Request $request, Todo $todo){
		if($todo->user_id == $request->user()->id){
			
			$error = $this->validations($request,"update todo");
			
			if ($error['error']) {
				
				return $this->prepareResult(false, [], $error['errors'],"error in updating data");
			} else {
				
				$todo = $todo->fill($request->all())->save();
				
				return $this->prepareResult(true, $todo, $error['errors'],"updating data");
				
			}
			
		}else{
			
			return $this->prepareResult(false, [], "unauthorized","You are not authenticated to edit this todo");
			
		}
	}
	
	/**

    * Remove the specified resource from storage.

    *

    * @param  \App\Todo  $todo

    * @return \Illuminate\Http\Response

    */

	public function destroy(Todo $todo){
		
		if($todo->user_id == $request->user()->id){
			
			if ($todo->delete()) {
				
				return $this->prepareResult(true, [], [],"Todo deleted");
				
			}
			
		}else{
			
			return $this->prepareResult(false, [], "unauthorized","You are not authenticated to delete this todo");
			
		}
		
	}
	
	public function InvoiceHeader(Request $request){
		$method = $request->method();
		
		
		if ($request->isMethod('post')) {
			$validator = Validator::make($request->json()->all(), [
            'biller' 			=> 'required|email|max:255',
            'password' 			=> 'required',
            'invoice_id'		=> 'required|max:20',
            'customer_name'		=> 'required|max:10',
            'customer_email'	=> 'required|email|max:255',
            'invoice_date'		=> 'required|date',
            'invoice_duedate'	=> 'required|date',
            'customer_phone'	=> 'required|max:13',
            'customer_address'	=> 'required|max:255',
            'invoice_amount'	=> 'required|numeric'
			]);
			
			if ($validator->fails()) {
				Log::info('app.requests', ['request' => $request->all(),'response' => $validator->errors()]);
				
				//Validator request
				return response()->json([
					'page'		=> "Finbox v.02",
					'status'	=> "Valid Request",
					'message'	=> "Welcome to Finbox",
					'data'		=> [
						'rc' 		=> "11",
						'rc_desc'	=> "Data not Complete !!"]
					
					], 202);
			}else{
				$data = $request->json()->all();
				
				$biller_email 	= $data['biller'];
				$password 		= $data['password'];
				
				if (Auth::attempt(['email' => $biller_email, 'password' => $password])) {
					
					$biller = User::whereEmail($biller_email)->first(['id']);
					
					$invoice_number='INV'.$biller->id.date('ymdHis');
					$insertHead = DB::table('invoices')->insert(
						['created_at' 			=> date('Y-m-d H:i:s'),
						 'updated_at'			=> date('Y-m-d H:i:s'),
						 'biller_id' 			=> $biller->id,
						 'invoice_number'		=> $invoice_number,
						 'invoice_number_ext'	=> $data['invoice_id'],
						 'period'				=> $data['invoice_date'],
						 'due_date'				=> $data['invoice_duedate'],
						 'recipient_name'		=> $data['customer_name'],
						 'recipient_email'		=> $data['customer_email'],
						 'recipient_phone'		=> $data['customer_phone'],
						 'recipient_address'	=> $data['customer_address'],
						 'notes'				=> $data['notes'],
						 'total'				=> $data['invoice_amount'],
						 'terms_conditions'		=> $data['terms'],
						 'recipient_email_cc'	=> $data['email_cc']
						]						 
					);
					
					$invoice = Invoice::whereInvoiceNumber($invoice_number)->first(['id']);
					
					if($insertHead)
						return response()->json([
						'page'		=> "Finbox v.02",
						'status'	=> "Valid Request",
						'message'	=> "Welcome to Finbox",
						'data'		=> [
							'rc' 			=> "00",
							'rc_desc'		=> "Data Sent !",
							'invoice_id'	=> $invoice->id,
							'invoice_number'=> $invoice_number,
							'invoice_name'	=> $data['customer_name'],
							'invoice_email'	=> $data['customer_email'],
							'invoice_amount'=> $data['invoice_amount'],
							'period'		=> $data['invoice_date'],
							'due_date'		=> $data['invoice_duedate']
							]
						], 200);
					else
						return response()->json([
						'page'		=> "Finbox v.02",
						'status'	=> "Valid Request",
						'message'	=> "Welcome to Finbox",
						'data'		=> [
							'rc' 		=> "77",
							'rc_desc'	=> "Invalid data type !!!"]
						], 207);
					
				}else{
				
					// Authentication not passed...
					return response()->json([
					'page'		=> "Finbox v.02",
					'status'	=> "Valid Request",
					'message'	=> "Welcome to Finbox",
					'data'		=> [
						'rc' 		=> "22",
						'rc_desc'	=> "Authentication Failed !!!!"]
					], 201);
				}
			}
			
		}else{
			
			// Invalid method
			return response()->json([
				'page'		=> "Finbox v.02",
				'status'	=> "Invalid Request",
				'data'		=> [
					'rc' 		=> "99",
					'rc_desc'	=> "Invalid method !!!!!!"]
            ], 401);
		}
	
	}
	
	public function InvoiceItem(Request $request){
		$method = $request->method();
		
		if ($request->isMethod('post')) {
			$validator = Validator::make($request->json()->all(), [
            'invoice_id' 		=> 'required|numeric',
			'item_name' 		=> 'required|max:100',
            'item_desc'			=> 'required|max:255',
            'item_amount'		=> 'required|numeric'
			]);
			
			if ($validator->fails()) {
				Log::info('app.requests', ['request' => $request->all(),'response' => $validator->errors()]);
				
				//Validator request
				return response()->json([
					'page'		=> "Finbox v.02",
					'status'	=> "Valid Request",
					'message'	=> "Welcome to Finbox",
					'data'		=> [
						'rc' 		=> "11",
						'rc_desc'	=> "Data not Complete !!"]
					
					], 202);
			}else{
				$data = $request->json()->all();
				
				$invoice_id	= $data['invoice_id'];
				$invoice 	= Invoice::whereId($invoice_id)->first(['id']);
				
				if ($invoice->id !=null) {
					
					$insertItem = DB::table('invoice_items')->insert(
						['created_at' 	=> date('Y-m-d H:i:s'),
						 'updated_at'	=> date('Y-m-d H:i:s'),
						 'invoice_id' 	=> $data['invoice_id'],
						 'name'			=> $data['item_name'],
						 'description'	=> $data['item_desc'],
						 'amount'		=> $data['item_amount']
						]						 
					);
					
					if($insertItem)
						return response()->json([
						'page'		=> "Finbox v.02",
						'status'	=> "Valid Request",
						'message'	=> "Welcome to Finbox",
						'data'		=> [
							'rc' 			=> "00",
							'rc_desc'		=> "Data Sent !",
							'invoice_id'	=> $invoice->id,
							'item_name'		=> $data['item_name'],
							'item_desc'		=> $data['item_desc'],
							'item_amount'	=> $data['item_amount']
							]
						], 200);
					else
						return response()->json([
						'page'		=> "Finbox v.02",
						'status'	=> "Valid Request",
						'message'	=> "Welcome to Finbox",
						'data'		=> [
							'rc' 		=> "77",
							'rc_desc'	=> "Invalid data type !!!"]
						], 207);
					
				}else{
				
					// Authentication not passed...
					return response()->json([
					'page'		=> "Finbox v.02",
					'status'	=> "Valid Request",
					'message'	=> "Welcome to Finbox",
					'data'		=> [
						'rc' 		=> "11",
						'rc_desc'	=> "Invoice id not found !!!!"]
					], 201);
				}
			}
			
		}else{
			
			// Invalid method
			return response()->json([
				'page'		=> "Finbox v.02",
				'status'	=> "Invalid Request",
				'data'		=> [
					'rc' 		=> "99",
					'rc_desc'	=> "Invalid method !!!!!!"]
            ], 401);
		}
	
	}
	
	public function sendEmailApi(Request $request){
		if ($request->isMethod('post')) {
			$validator = Validator::make($request->json()->all(), [
            'invoice_number' 	=> 'required|max:20',
            'is_smssend'		=> 'required'
			]);
			
			if ($validator->fails()) {
				Log::info('app.requests', ['request' => $request->all(),'response' => $validator->errors()]);
				
				//Validator request
				return response()->json([
					'page'		=> "Finbox v.02",
					'status'	=> "Valid Request",
					'message'	=> "Welcome to Finbox",
					'data'		=> [
						'rc' 		=> "11",
						'rc_desc'	=> "Data not meet requirement !!"]
					
					], 202);
			}else{
				$data = $request->json()->all();
				
				$invoice_number	= $data['invoice_number'];
				$invoice 	= Invoice::whereInvoiceNumber($invoice_number)->first(['id','recipient_name','recipient_email','recipient_email_cc','recipient_phone','payment_status','biller_id']);
					
				if (!empty($invoice->id)) {
					
					if (InvoiceItem::whereInvoiceId($invoice->id)->count()<=0)
						return response()->json([
							'page'		=> "Finbox v.02",
							'status'	=> "Not Allowed Request",
							'message'	=> "Welcome to Finbox",
							'data'		=> [
								'rc' 		=> "11",
								'rc_desc'	=> "Invoice item doesn't exists !!"]
							], 500);
					
					
							
					//check if invoice has been paid
					if ($invoice->payment_status=='PAID')
					return response()->json([
							'page'		=> "Finbox v.02",
							'status'	=> "Not Allowed Request",
							'message'	=> "Welcome to Finbox",
							'data'		=> [
								'rc' 		=> "11",
								'rc_desc'	=> "Invoice has been paid"]
							], 500);
							
					$messenger = new MessagingHandler();
					
					$payment_link = $this->getPaymentLink($data['invoice_number']);
					
					if($payment_link=='Error'){
						return response()->json([
							'page'		=> "Finbox v.02",
							'status'	=> "Valid Request",
							'message'	=> "Welcome to Finbox",
							'data'		=> [
								'rc' 		=> "11",
								'rc_desc'	=> "Failed get Payment Link !!"]
							], 500);
					}
					
					$is_sendsms=false;
					if($data['is_smssend']==true){
						$shorter   = new ShortURLHandler();
		
						$shortlink  = $shorter->getShortUrl($payment_link, 'Invoice '.$data['invoice_number'], []);
						
						$smsMessage = 'Anda menerima tagihan dengan nomor '.$data['invoice_number'].' '.$shortlink;

						$sentSMS = $messenger->sendSMSProd($invoice->recipient_phone, base64_encode($smsMessage), []);
						
						$is_sendsms=true;
					}
					
					// create PDF and attach to mail
					$pdfFile = $this->createPdf($data['invoice_number'],$payment_link);
					$fullpath = $this->getTempInvoicePath($pdfFile);
					
					$biller = User::whereId($invoice->biller_id)->first(['name','email']);
					
					$invoice_data['name']			=$invoice->recipient_name;
					$invoice_data['payment_link']	=$payment_link;
					$invoice_data['biller_name']	=$biller->name;
					
					$sendEmail = $messenger->emailApi(
						'invoice/api/body',
						$invoice_data,
						$biller->email,
						$biller->name,
						$invoice->recipient_email,
						$invoice->recipient_name,
						'Invoice '.$data['invoice_number'],
						$invoice->recipient_email_cc,
						$fullpath
					);

					// delete pdf file after sending
					@unlink($fullpath);
					
					if(empty($sendEmail))
						return response()->json([
						'page'		=> "Finbox v.02",
						'status'	=> "Valid Request",
						'message'	=> "Welcome to Finbox",
						'data'		=> [
							'rc' 			=> "00",
							'rc_desc'		=> "Invoice Sent !!",
							'primary_mail'	=> $invoice->recipient_email,
							'secondary_mail'=> $invoice->recipient_email_cc,
							'is_sendsms'	=> $is_sendsms,
							'invoice_id'	=> $invoice->id,
							'invoice_number'=> $data['invoice_number'],
							'cust_name'		=> $invoice->recipient_name
							]
						], 200);
					else
						return response()->json([
						'page'		=> "Finbox v.02",
						'status'	=> "Valid Request",
						'message'	=> "Welcome to Finbox",
						'data'		=> [
							'rc' 		=> "77",
							'rc_desc'	=> "Failed send Invoice !!!"]
						], 207);
					
				}else{
				
					// Authentication not passed...
					return response()->json([
					'page'		=> "Finbox v.02",
					'status'	=> "Valid Request",
					'message'	=> "Welcome to Finbox",
					'data'		=> [
						'rc' 		=> "11",
						'rc_desc'	=> "Invoice number not found !!!!"]
					], 201);
				}
			}
			
		}else{
			
			// Invalid method
			return response()->json([
				'page'		=> "Finbox v.02",
				'status'	=> "Invalid Request",
				'data'		=> [
					'rc' 		=> "99",
					'rc_desc'	=> "Invalid method !!!!!!"]
            ], 401);
		}
	}
	
	private function getPaymentLink($invoice_number){
		if (env('APP_ENV') != 'local') {
			$client    = new Client(['verify' => false]);  //bypass https warning
			$serverUrl = env('PAYMENT_LINK_GENERATOR_URL');

			try {
				$response = $client->request('GET', $serverUrl.'?invoice='.$invoice_number, ['proxy' => env('OUTGOING_PROXY_URL')]);
				$response = $response->getBody()->getContents();

				if(!$response) {
					Log::error('Get Payment Link. Error : Result Null');

					return 'Error';
				}

				Log::info('Response Create link is : '.$response);
				return $response;
			} catch (\Exception $e) {
				Log::error( 'Error creating Payment Link. Error : '.$e->getMessage() );

				return 'Error';
			}

		} else
			return $response = url('invoice/payment/'.$invoice_number);
	}
	
	private function createPdf($invoice_number,$payment_link)
    {
		$invoice = Invoice::whereInvoiceNumber($invoice_number)->first(['biller_id','terms_conditions','notes','recipient_name','recipient_email','invoice_number','created_at','total','due_date']);
		
		$biller = User::whereId($invoice->biller_id)->first(['id','phone','address','logo']);
		
		$data['biller']			= $biller;
		$data['invoice']		= $invoice;
		$data['payment_link']	= $payment_link;
		
		$data['previewInvoice']=DB::select("select a.invoice_number, a.created_at,b.name, b.description, b.amount from invoices a, invoice_items b where a.id=b.invoice_id and a.invoice_number=:b1",["b1" => $invoice_number]);
		
		
        $pdf = PDF::loadView('invoice/api/attachment', $data);
        $filename = $invoice_number. '_'. time().'_'.uniqid().'.pdf';
        @$pdf->save($this->getTempInvoicePath($filename));

        return $filename;
    }
	
	private function getTempInvoicePath($filename)
    {
        return base_path('storage/pdf/'.$filename);
    }
	
}
