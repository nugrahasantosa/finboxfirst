<?php

namespace Finbox\Http\Controllers\Auth;

use DB;
use Mail;

use Finbox\User;
use Finbox\MyClass\MessagingHandler;
use Finbox\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'auth/register';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \Finbox\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
	
	protected function createBillerActivation(array $data)
    {
            $create = DB::table('biller_activations')
            ->insert(['biller_id' => $data['id_biller'],
                'activated' => 0,
                'token' => $data['token'],
                'created_at' => $data['timestamp'],
            ]);
			//print_r($data['timestamp']);die;
    }
	
	protected function register(Request $request)
	{
        return view('auth.register');
    }
	
	protected function postRegister(Request $request)
	{
		$validator = $this->validator($request->all());
        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }
        $user = $this->create($request->all());
        $user -> save();
		$id_biller = User::where('email',$user['email'])->value('id');
        $timestamp = User::where('email',$user['email'])->value('created_at');
        $token = str_random(25);
        $input = array('id_biller' => $id_biller,   
                      'token' => $token,
                      'timestamp' => $timestamp);
        $data = $this->createBillerActivation($input);
		
		$messenger = new MessagingHandler();
		
        $sendEmail = $messenger->sendEmailConfirmation(
            'emails.confirmation',
			$input,
            'cs_finbox@finnet-indonesia.com',
            'FinBox',
            $user['email'],
            $user['name'],
            'Registration Confirmation',
            null
        );
		
		if($sendEmail==true){
			/*return redirect('auth/register')->with('status', 'We sent you an activation code. Check '.$user['email'].' .');*/
			return redirect('verifikasi');
		}else{
			return redirect('auth/register')->with('status', $sendEmail);
		}
    }
	
	protected function confirmation($token)
    {
        $biller_id = DB::table('biller_activations')
            ->where('token',$token)
            ->value('biller_id');
        if (is_null($biller_id)) {
            return redirect('auth/login')->with('danger','Your Token is Not Valid');    
        }else{
            //$del = Biller_Activations::find($id);
            DB::table('biller_activations')
            ->where('token',$token)->delete();
            //$user = Biller::where('id',$id) -> first();
            $user = User::whereId($biller_id)->first(['name']);
			$data['name']=$user->name;
            if(!is_null($user))
            {
                //$user -> activated = 1;
                //$user -> save();
                DB::table('billers')
                ->where('id',$biller_id)
                ->update(['ACTIVATED'=>'1']);
                /*return redirect('confirmation')->with('status', $user['name']);*/
				return view('auth.confirmation',$data);
            }
        }
        return redirect('auth/login')->with('danger','Something went wrong');
    }
	
}
