<?php

namespace Finbox\Http\Controllers\Auth;

use Finbox\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'auth/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
		return redirect('/homepage');
    }
	
	protected function login(Request $request)
	{
        return view('auth.login');
    }
	
	protected function postLogin(Request $request)
	{
		$this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
		
		if (auth()->attempt(array('email' => $request->input('email'), 'password' => $request->input('password'))))
        {
            return redirect()->to('/auth/register');
        }else{
            return back()->with('danger','Your Email or Password are Wrong');
        }
	}
}
