<!DOCTYPE html>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5afa548b227d3d7edc255172/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('pageTitle', 'Finbox')</title>

    <link rel="icon" href="{{ URL::asset('images/favicon.png') }}" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
	
	
	<style>
		.lock{	
			opacity: 0.4;
		}
		.dropdown {
			position: relative;
			display: inline-block;
		}

		.dropdown-content {
			display: none;
			position: relative;
			background-color: #f9f9f9;
			min-width: 160px;
			box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
			padding: 12px 16px;
			z-index: 1;
		}

		.dropdown:hover .dropdown-content {
			display: block;
		}
	</style>
	
	{{-- compiled css --}}
    <!-- Begin: MAIN CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/wizard.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/plugins-min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/menu-overright.css') }}">
    <link href="{{ URL::asset('css/colors/orange-burgundy.css') }}" type="text/css" media="all" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	
	@yield('customPluginCss', '')   {{-- custom per page plugin --}}
	
</head>
<body>
	<div class="left-header left-header-setting">
		<div class="top-header light-bg border-bottom-light" style="background: #ffffff;height: 78px;">
			<div class="parallax">
				<div class="parallax_child"></div>
				<div class="container">
					<div class="row">
						<div class="col-md-6" style="margin-top: 12px;margin-bottom: 0px;">
							<a style="color:#fff" href="{{ url('/dashboard') }}"><img src="{{ URL::asset('images/icon/finbox.png') }}" width="140"></a>
						</div>
						<div class="col-md-4">
							<a class="navbar-brand logo pull-right" href="#" style="color: black"><img src="{{ URL::asset('images/icon/bell.png') }}" height="30"></a>
							<form class="navbar-form navbar-right" role="search" style="padding-top: 10px;">
								<div class="form-group form-search is-empty">
									<input type="text" class="form-control" placeholder="Search">
									<span class="material-input"></span>
								<span class="material-input"></span></div>
								<button type="submit" class="btn btn-white btn-round btn-just-icon" style="border-radius: 5px;">
									<i class="fa fa-search"></i>
									<div class="ripple-container"></div>
								</button>
							</form>
						</div>
						<div class="col-md-1">
							<ul class="navbar-brand logo pull-right" href="#">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<img src="{{ Auth::user()->logo ? asset('img/privacy/'.Auth::user()->id.'/img_account/'.Auth::user()->logo.'') : asset('images/icon/default-avatar.png')}}" height="40" width="40" style="border-radius: 35px;">
									<span class="caret"></span></a>		
								  <ul class="dropdown-menu" role="menu">
									<li @yield('sidebarActiveRingkasanAkun')>
										<a href="{{ url('/akun') }}">Ringkasan Akun</a>
									</li>
									<li>
										<a href="{{ url('auth/logout') }}">Log Out</a>
									</li>
								  </ul>                
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div>
			<div class="row">
				<div class="col-md-3" style="width: 20%;">
					<header class="main-header sticky-header base pading_top header_responsive">
						<nav class="navbar navbar-default" style="padding-left: 24px;">
							<div class="container position-relative">
								<!-- Brand and toggle get grouped for better mobile display -->
								<div class="navbar-header border">
									<!-- Begin: RESPONSIVE MENU TOGGLER -->
									<button type="button" class="navbar-toggle" data-toggle="modal" data-target=".header-search">
										<span class="sr-only">Toggle navigation</span>
										<i class="fa fa-search"></i>
									</button>
									<!-- End: RESPONSIVE MENU TOGGLER -->
									<!-- Begin: RESPONSIVE MENU TOGGLER -->
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<i class="fa fa-bars"></i>
									</button>
									<!-- End: RESPONSIVE MENU TOGGLER -->
									<!-- Begin: LOGO -->
									<!-- End: LOGO -->
								</div>
								<!-- /.navbar-collapse -->
								<div class="collapse navbar-collapse text-weight-400 sidebar3" id="nav-navbar-collapse-1">
									<ul class="nav navbar-nav navbar-right margin-right-0" style="padding-left: 0px;margin-top: 20px;">
										<li>
											<div style="float:left">
												<a href="{{ url('/akun') }}"><img src="{{ Auth::user()->logo ? asset('img/privacy/'.Auth::user()->id.'/img_account/'.Auth::user()->logo.'') : asset('images/icon/default-avatar.png')}}" height="70" width="70" style="border-radius: 35px;"></a>
											</div>
											<div style="float:left;margin-top: 15px;margin-left: 5px;">
												<a href="{{ url('/akun') }}"><p class="text-weight-600" style="color:#cccccc;color:#ff4500;margin: 0 0 0px;">{{ Auth::user()->name }}</p></a>
												<p class="text-weight-600" style="color:#cccccc;">Status - Active</p>
											</div>
										</li>
										<li>
											<h5 class="text-weight-600" style="color:#cccccc;">Deposit</h5>
										</li>
										<li>
											<h4 style="color:#ff4500;"><img src="{{ URL::asset('images/icon/voucher.png') }}" height="30"> Rp. 200.000.000,-</h4>
										</li>
										<li @yield('sidebarActiveProfile') style="padding: 10px 5px;background: #ffb01f;width: 90%;border-radius: 5px;" class="text-center">
											<a href="{{ url('/profile/langkah1')}}" style="color:#fff;text-center;padding: 10px 15px;background: #ffb01f;"><div style="float:left;line-height: 20px;">Aktifasi Biller</div><div style="float:right"><span class="badge" style="background:red;">!</span></div></a>
										</li>
										<li id="Report">
											<h4 class="text-weight-600" style="color:#666666;padding: 10px 15px;">PROFIL SAYA</h4>
										</li>
										<li @yield('sidebarActiveDashboard')>
											<a href="{{ url('/dashboard')}}">Dashboard</a>
										</li>
										<li @yield('sidebarActiveInvoice')>
										  <a href="#Report" data-toggle="collapse" data-target="#toggleDemo0" data-parent="#sidenav01" class="collapsed">Invoice
										  </a>
										  <div class="collapse" id="toggleDemo0" style="height: 0px;">
											@if ($check['lock']=="lock")
												<ul class="nav nav-list">
												  <li><a href="#" class="" data-toggle="{{ $check['modal'] }}" data-target="{{ $check['target'] }}">Upload Invoice</a></li>
												  <li><a href="#" class="" data-toggle="{{ $check['modal'] }}" data-target="{{ $check['target'] }}">Single Invoice</a></li>
												</ul>
											@else
												<ul class="nav nav-list">
												  <li><a href="{{ url('/invoiceBulk') }}">Invoice Bulk</a></li>
												  <li><a href="{{ url('/invoiceSatuan') }}">Single Invoice</a></li>
												</ul>
											@endif
										  </div>
										</li>
										<li @yield('sidebarActiveReport')>
										  <a href="#Report" data-toggle="collapse" data-target="#toggleDemo1" data-parent="#sidenav02" class="collapsed">Report
										  </a>
										  <div class="collapse" id="toggleDemo1" style="height: 0px;">
											@if ($check['lock']=="lock")
												<ul class="nav nav-list">
												  <li><a href="#" class="" data-toggle="{{ $check['modal'] }}" data-target="{{ $check['target'] }}">Invoice List</a></li>
												  <li><a href="#" class="" data-toggle="{{ $check['modal'] }}" data-target="{{ $check['target'] }}">Invoice Draf</a></li>
												</ul>
											@else
												<ul class="nav nav-list">
												  <li><a href="{{ url('/invoiceList') }}">Invoice List</a></li>
												  <li><a href="{{ url('/invoiceDraf') }}">Invoice Draf</a></li>
												</ul>
											@endif
										  </div>
										</li>
										<li>
											<a href="#">Riwayat Transaksi</a>
										</li>
										<li style="display:none;">
											<a href="#" class="" data-toggle="{{ $check['modal'] }}" data-target="{{ $check['target'] }}">Management User</a>
										</li>
										<li style="display:none;">
											<a href="#" class="" data-toggle="{{ $check['modal'] }}" data-target="{{ $check['target'] }}">Rekonsiliasi</a>
										</li>
										<li style="display:none;">
											<a href="#"  class="}" data-toggle="{{ $check['modal'] }}" data-target="{{ $check['target'] }}">Settlement</a>
										</li>
										<li style="display:none;">
											<a href="#" class="" data-toggle="{{ $check['modal'] }}" data-target="{{ $check['target'] }}">Integration</a>
										</li>
										<li @yield('sidebarActiveFaq')>
											<a href="{{ url('/faq')}}">FAQ</a>
										</li>
										<li>
											<hr class="margin-bottom-xsmall" style="border-top: 3px solid #eee;">
										</li>
										<li>
											<a href="#" class="" data-toggle="{{ $check['modal'] }}" data-target="{{ $check['target'] }}">Help Center</a>
										</li>
									</ul>
								</div><!-- /.navbar-collapse -->
							</div>
						</nav>
					</header>
				</div>
				<div class="col-md-9" style="padding-left: 0px;width: 80%;border-left-style: solid !important;
    border-left-width: 2px!important;
    border-left-color: #e7e7e7 !important;">
					<div class="content light-bg min_height_dashboard">
						<div class="container-fluid light-bg">
							@yield('content')
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <!--
    #################################
        - Begin: FOOTER -
    #################################
    -->
    <footer id="footer" class="light-bg" style="border-top-style: solid !important;border-top-width: 2px!important; border-top-color: #e7e7e7 !important;background:#fff;">
        <div class="top-footer" style="padding: 20px 0 20px;">
            <div class="" style="margin-left: 4%;">
                <div class="row">
					<br><div class="col-sm-3">
							<a href="{{ url('/homepage') }}"><img src="{{ URL::asset('images/icon/finbox.png') }}" width="100"></a>
							<p style="font-size: 12px;">© 2018, PT FINNET INDONESIA</p>
					</div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End: FOOTER -
    ################################################################## -->
	
	<!-- Modal Profile Langkah 1 -->
	<div class="modal pullUp-modal animate pullUp" data-animation="pullUp" tabindex="-1" style="display: none;">
		<div class="modal-dialog modal-sm" role="document" style="left: 0px;">
			<div class="modal-content">
				<div class="modal-body" style="max-height:200px;">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					<h4>Lock</h4>
					<p>Jika ingin mengaktifkan menu ini Anda harus mengisi data profile dengan lengkap.</p>
				</div>
				<div class="modal-footer">
					<a href="{{ url('profile/langkah1') }}" class="btn btn-warning pull-right" style="border-radius: 5px;">Oke</a>
				</div>
			</div>
		</div>
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
	
	<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
	<script>
		$(document).ready(function(){
			$(".dropdown").hover(            
				function() {
					$('.dropdown-menu', this).not('.in .dropdown-menu').stop( true, true ).slideDown("fast");
					$(this).toggleClass('open');        
				},
				function() {
					$('.dropdown-menu', this).not('.in .dropdown-menu').stop( true, true ).slideUp("fast");
					$(this).toggleClass('open');       
				}
			);
		});
		$('input[name="dates"]').daterangepicker();
		
		window.onload=function(){
		$('.dropdown').click(function(){
		$(this).siblings(".submenu").toggleClass('hide');


		});
		
		function showModal(id) {
		  $(".modal").modal('hide');
		  $("#" + id).modal();
		}
		}
	</script>
	@yield('customPluginJs')    {{-- custom per page js or plugin --}}
</body>
</html>