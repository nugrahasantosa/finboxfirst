<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Homepage | Finbox</title>

    <link rel="icon" href="{{ URL::asset('images/favicon.png') }}" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <!-- Begin: MAIN CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/plugins-min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/menu-overright.css') }}">
    <link href="{{ URL::asset('css/colors/cherry-green.css') }}" type="text/css" media="all" rel="stylesheet" />
	<style>
		.nav-tabs>li.active>a, .nav-tabs>li.active>a:focus, .nav-tabs>li.active>a:hover {
			border: 0px solid #ddd;
		}
	</style>
    <!-- End: MAIN CSS -->

    <!-- Begin: REQUIRED FOR THIS PAGE ONLY -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('plugins/revolution/css/layers.css') }}"/> 
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('plugins/revolution/css/settings.css') }}"/> 
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('plugins/revolution/css/navigation.css') }}"/> 
    <link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('plugins/flexslider/css/flexslider.css') }}" />	<!-- FlexSlider -->
    <!-- End: REQUIRED FOR THIS PAGE ONLY -->

    <!-- Begin: HTML5SHIV FOR IE8 -->
    <!-- HTML5 shim and Respond.js') }} for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="{{ URL::asset('js/internetexplorer/html5shiv.min.js') }}"></script>
      <script src="{{ URL::asset('js/internetexplorer/respond.min.js') }}"></script>
    <![endif]-->
    <!-- end: HTML5SHIV FOR IE8 -->
</head>
<body>

    <!-- Begin: HEADER SEARCH -->
    <div class="modal fade header-search" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="search">
                <form method="get" action="search.html">
                    <div class="form-group full-width">
                        <div class="input-group">
                            <input type="text" class="form-control input-lg" placeholder="Search Here..." value="" name="q">
                            <div class="input-group-btn "><button type="submit" class="btn btn-base btn-lg"><i class="fa fa-search"></i></button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End: HEADER SEARCH -->

    <!--
    #################################
        - Begin: HEADER -
    #################################
    -->
    <header class="main-header sticky-header smaller">
        <nav class="navbar navbar-default single-page-nav border-top-light border-bottom-light">
            <div class="container position-relative">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header border">
                    <!-- Begin: RESPONSIVE MENU TOGGLER -->
                    <button type="button" class="navbar-toggle" data-toggle="modal" data-target=".header-search">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-search"></i>
                    </button>
                    <!-- End: RESPONSIVE MENU TOGGLER -->
                    <!-- Begin: RESPONSIVE MENU TOGGLER -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-navbar-collapse-2">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-shopping-bag"></i>
                    </button>
                    <!-- End: RESPONSIVE MENU TOGGLER -->
                    <!-- Begin: RESPONSIVE MENU TOGGLER -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars"></i>
                    </button>
                    <!-- End: RESPONSIVE MENU TOGGLER -->
                    <!-- Begin: LOGO -->
                    <a class="navbar-brand logo" href="{{ url('/homepage') }}"><img src="{{ URL::asset('images/icon/finbox.png') }}"></a>
                    <!-- End: LOGO -->
                </div>
                <div class="collapse navbar-collapse" id="nav-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right margin-right-0">
                        <!--<li class="dropdown"> <a href="index.html" class="dropdown-toggle" data-toggle="" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="index-blog-1.html">Home - Blog 1</a></li>
                            <li><a href="index-blog-2.html">Home - Blog 2</a></li>
                            <li><a href="index-blog-3.html">Home - Blog 3</a></li>
                          </ul>
                        </li>-->
						<li class="dropdown mega-menu">
                            <a href="#services" class="dropdown-toggle text-weight-600" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Our Services <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <!-- Start: MEGA MENU CONTENT -->
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-3" style="border-right: 2px solid #f2f2f2;">
												<h3 style="color:#ff4500;">Finbox Services</h3>
												<p>Berbagai informasi yang dibutuhkan untuk terhubung dengan layanan finbox.</p>
                                            </div>
                                            <div class="col-md-3">
												<h3><img src="{{ URL::asset('images/icon/finbox.png') }}" height="40"></h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="#"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                            <div class="col-md-3">
												<h3><img src="{{ URL::asset('images/icon/finboxbiller.png') }}" height="40"></h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="{{ url('/dashboard') }}"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
											<div class="col-md-3">
												<h3><img src="{{ URL::asset('images/icon/finboxca.png') }}" height="40"></h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="{{ url('/dashboard') }}"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End: MEGA MENU CONTENT -->
                                </li>
                            </ul>
                        </li>
						<li class="dropdown mega-menu">
                            <a href="#getStarted" class="dropdown-toggle text-weight-600" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Get Started <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <!-- Start: MEGA MENU CONTENT -->
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-3" style="border-right: 2px solid #f2f2f2;">
												<h3 style="color:#ff4500;">Finbox Services</h3>
												<p>Berbagai informasi yang dibutuhkan untuk terhubung dengan layanan finbox.</p>
                                            </div>
                                            <div class="col-md-3">
												<h3>Technical Document</h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="#"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                            <div class="col-md-3">
												<h3>How To Self Testing</h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="{{ url('/dashboard') }}"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
											<div class="col-md-3">
												<h3>Request Testing</h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="{{ url('/dashboard') }}"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End: MEGA MENU CONTENT -->
                                </li>
                            </ul>
                        </li>
						<li class="dropdown mega-menu">
                            <a href="#support" class="dropdown-toggle text-weight-600" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Support <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <!-- Start: MEGA MENU CONTENT -->
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-3" style="border-right: 2px solid #f2f2f2;">
												<h3 style="color:#ff4500;">Finbox Services</h3>
												<p>Berbagai informasi yang dibutuhkan untuk terhubung dengan layanan finbox.</p>
                                            </div>
                                            <div class="col-md-3">
												<h3>Contact Us</h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="#"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                            <div class="col-md-3">
												<h3>FAQ</h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="{{ url('/dashboard') }}"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End: MEGA MENU CONTENT -->
                                </li>
                            </ul>
                        </li>
						@if (Auth::guest())
							<li><a href="{{ url('auth/login') }}" class=" text-weight-600">Log In</a></li>
						@else
							<li>
								<a href="{{ url('auth/logout') }}" class=" text-weight-600">Log Out</a>
							</li>
						@endif
						<li>
							<a href="{{ url('auth/register') }}"><button class=" text-weight-600 btn btn-primary btn-sm" style="border-radius: 5px;">Sign Up</button></a>
						</li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
    </header>
    <!-- End: HEADER -
    ################################################################## -->
    
    <!--
    #################################
        - Begin: REVOLUTION SLIDER -
    #################################
    -->
    <div id="rev_slider_wrapper" class="rev_slider_wrapper fullwidthbanner-container tp-banner-container">                
        <div id="rev_slider" class="rev_slider fullwidthabanner">
            <ul style="width: 90%;">
                <!-- SLIDE  -->
                <li data-index="rs-23" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="{{ URL::asset('images/slider/slidebg5.jpg') }}" data-rotate="0" data-saveperformance="off" data-title="Slide1" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ URL::asset('images/slider/slidebg5.jpg') }}" alt="" title="slidebg4" width="1920" height="1080" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="15" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption NotGeneric-Icon tp-resizeme z-index-8 text-weight-600"
                         id="slide-23-layer-11"
                         data-x="center" data-hoffset=""
                         data-y="center" data-voffset="-60"
                         data-width="['auto']"
                         data-height="['auto']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":0,"speed":600,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":600,"frame":"999","to":"auto:auto;","ease":"Power2.easeInOut"}]'
                         data-textalign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]">YOU ARE WELCOME</div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption large_bold_white tp-resizeme z-index-8"
                         id="slide-23-layer-15"
                         data-x="center" data-hoffset=""
                         data-y="center" data-voffset=""
                         data-width="['auto']"
                         data-height="['auto']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":10,"speed":1200,"frame":"0","from":"y:-50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":600,"frame":"999","to":"auto:auto;","ease":"Power2.easeInOut"}]'
                         data-textalign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]">FINBOX ULTIMATE</div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption Gym-Subline tp-resizeme z-index-8 text-weight-400"
                         id="slide-23-layer-5"
                         data-x="center" data-hoffset=""
                         data-y="center" data-voffset="60"
                         data-width="['auto']"
                         data-height="['auto']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":10,"speed":1800,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1800,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-textalign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]">Buat tagihan tanpa kekhawatiran dengan layanan pembayaran<br> kapanpun dan <a href="#pay">dimanapun</a>
					</div>
                    <div class="overlay dark-2"></div>
                </li>
                <!-- SLIDE  -->
                <li data-index="rs-26" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="{{ URL::asset('images/slider/slidebg4.jpg') }}" data-rotate="0" data-saveperformance="off" data-title="Medico" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ URL::asset('images/slider/slidebg4.jpg') }}" alt="" title="slidebg3" width="1920" height="1080" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="15" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 6 -->

                    <!-- LAYER NR. 7 -->

                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption large_bold_white tp-resizeme z-index-8"
                         id="slide-26-layer-15"
                         data-x="center" data-hoffset=""
                         data-y="center" data-voffset="-60"
                         data-width="['auto']"
                         data-height="['auto']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":0,"speed":1600,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1600,"frame":"999","to":"auto:auto;","ease":"Power2.easeInOut"}]'
                         data-textalign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]">Semudah Yang Diharapkan</div>

                    <!-- LAYER NR. 9 -->
                    <div class="tp-caption Gym-Subline tp-resizeme z-index-8 text-weight-400"
                         id="slide-26-layer-5"
                         data-x="center" data-hoffset=""
                         data-y="center" data-voffset=""
                         data-width="['auto']"
                         data-height="['auto']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":2000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-textalign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]">klien di seluruh Indonesia menggunakan finbox karena satu alasan,</div>
					<div class="tp-caption Gym-Subline tp-resizeme z-index-8 text-weight-400"
                         id="slide-26-layer-5"
                         data-x="center" data-hoffset=""
                         data-y="center" data-voffset="50"
                         data-width="['auto']"
                         data-height="['auto']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":2000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-textalign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]">finbox mudah digunakan. Create, klik, tagihan pun terkirim.</div>

                    <!-- LAYER NR. 10 -->
                    <div class="overlay dark-2"></div>
                </li>
				<li data-index="rs-29" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="{{ URL::asset('images/slider/slidebg3.jpg') }}" data-rotate="0" data-saveperformance="off" data-title="Medico" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                    <!-- MAIN IMAGE -->
                    <img src="{{ URL::asset('images/slider/slidebg3.jpg') }}" alt="" title="slidebg3" width="1920" height="1080" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="15" class="rev-slidebg" data-no-retina>
                    <!-- LAYERS -->
                    <!-- LAYER NR. 6 -->

                    <!-- LAYER NR. 7 -->

                    <!-- LAYER NR. 8 -->
                    <div class="tp-caption large_bold_white tp-resizeme z-index-8"
                         id="slide-29-layer-15"
                         data-x="right" data-hoffset=""
                         data-y="center" data-voffset="-60"
                         data-width="['auto']"
                         data-height="['auto']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":0,"speed":1600,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":1600,"frame":"999","to":"auto:auto;","ease":"Power2.easeInOut"}]'
                         data-textalign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[250,250,250,250]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]">Repot ?</div>

                    <!-- LAYER NR. 9 -->
                    <div class="tp-caption Gym-Subline tp-resizeme z-index-8 text-weight-400"
                         id="slide-29-layer-5"
                         data-x="right" data-hoffset=""
                         data-y="center" data-voffset=""
                         data-width="['auto']"
                         data-height="['auto']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":2000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-textalign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[30,30,30,30]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]">Punya Banyak Tagihan? Anda Tak Perlu Buat</div>
					<div class="tp-caption Gym-Subline tp-resizeme z-index-8 text-weight-400"
                         id="slide-29-layer-5"
                         data-x="right" data-hoffset=""
                         data-y="center" data-voffset="50"
                         data-width="['auto']"
                         data-height="['auto']"
                         data-type="text"
                         data-responsive_offset="on"
                         data-frames='[{"delay":10,"speed":2000,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":2000,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
                         data-textalign="['inherit','inherit','inherit','inherit']"
                         data-paddingtop="[0,0,0,0]"
                         data-paddingright="[30,30,30,30]"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]">Satu Per Satu Tagihan Anda. Cukup Upload dan Kirim.</div>
                    <!-- LAYER NR. 10 -->
                    <div class="overlay dark-2"></div>
                </li>
            </ul>
        </div>
    </div>
    <!-- End: REVOLUTION SLIDER -
    ################################################################## -->

	<div class="row" style="max-width: 851px;z-index: 1000;margin-top: -80px;margin-left: 19%;margin-right: 5%;font-size: 13px;background: #fff;"> 
		<div class="col-md-12">
			<div class="tabs-theme light">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs text-center" role="tablist" style="background: #fff;">
					<li role="presentation" class="active"><a href="#topUp" aria-controls="services" role="tab" data-toggle="tab" aria-expanded="false"><img src="{{ URL::asset('images/icon/topup.png') }}" height="25px"><br>Top Up</a></li>
					<li role="presentation" class=""><a href="#paketData" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true"><img src="{{ URL::asset('images/icon/paketdata.png') }}" height="25px"><br>Paket Data</a></li>
					<li role="presentation" class=""><a href="#voucherGame" aria-controls="counters" role="tab" data-toggle="tab" aria-expanded="false"><img src="{{ URL::asset('images/icon/voucher.png') }}" height="25px"><br>Voucher Game</a></li>
					<li role="presentation" class=""><a href="#pesawat" aria-controls="counters" role="tab" data-toggle="tab" aria-expanded="false"><img src="{{ URL::asset('images/icon/airline.png') }}" height="25px"><br>Pesawat</a></li>
					<li role="presentation" class=""><a href="#apartemen" aria-controls="services" role="tab" data-toggle="tab" aria-expanded="false"><img src="{{ URL::asset('images/icon/topup.png') }}" height="25px"><br>Apartemen</a></li>
					<li role="presentation" class=""><a href="#keretaApi" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="true"><img src="{{ URL::asset('images/icon/paketdata.png') }}" height="25px"><br>Kereta Api</a></li>
					<li role="presentation" class=""><a href="#pdam" aria-controls="counters" role="tab" data-toggle="tab" aria-expanded="false"><img src="{{ URL::asset('images/icon/voucher.png') }}" height="25px"><br>Air PDAM</a></li>
					<li role="presentation" class=""><a href="#listrilPln" aria-controls="counters" role="tab" data-toggle="tab" aria-expanded="false"><img src="{{ URL::asset('images/icon/airline.png') }}" height="25px"><br>Listrik PLN</a></li>
					<li role="presentation" class="" style="min-width: 58px;"><a href="#bpjs" aria-controls="counters" role="tab" data-toggle="tab" aria-expanded="false"><img src="{{ URL::asset('images/icon/airline.png') }}" height="25px"><br>BPJS</a></li>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="topUp">
						<div class="clearfix">
							<div class="row">
								<div class="col-md-4 form-group">
									<label for="pic_npwp" class="label_form_in_dashboard">Nomor Telepon</label>
									<input type="text" class="form-control home_radius" pattern="[0-9.-]{20}" id="pic_npwp" name="pic_npwp" placeholder="0812345678910">
									<div class="help-block with-errors"></div>
								</div>
								<div class="col-md-4 form-group">
									<label for="pic_npwp" class="label_form_in_dashboard">Nominal</label>
									<select id="province" name="bank_name" id="bank_name" class="form-control input-form radius_form_in_dashboard">
										<option>-Pilih Nominal-</option>
											<option value="10"> 10.000 </option>
										<option value="BANK GANESHA"> 20.000 </option>
										<option value="BANK OF TOKYO"> 25.000 </option>
										<option value="BANK INA PERDANA"> 50.000 </option>
										<option value="BANK VICTORIA"> 100.000 </option>
										<option value="BANK MASPION"> 500.000 </option>
									</select>
								</div>
								<div class="col-md-4">
									<br><button type="submit" value="Submit" name='submit' class="btn btn-base btn-lg daftar text-weight-600 box-shadow-active" style="width: 98%;">Beli</button>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="paketData">
						<div class="clearfix">
							<div class="col-md-12">
								<div class="team text-center">
									<div class="team-image">
										<div class="team-description">
											<p class="text-white">The sit amet orci eget eros fabus tincidunt duis leo sed fringilla mauris sit amet.</p>
											<div class="separator"><i class="short-line"></i><i class="fa fa-circle-o"></i><i class="fa fa-circle"></i><i class="fa fa-circle-o"></i><span class="short-line"></span></div>
										</div>
										<div class="overlay dark-7"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="voucherGame">
						<div class="clearfix">
							<div class="row">
								<div class="col-md-4 form-group">
									<label for="pic_npwp" class="label_form_in_dashboard">Jenis Voucher</label>
									<select id="province" name="bank_name" id="bank_name" class="form-control input-form radius_form_in_dashboard">
										<option>-Pilih Nominal-</option>
										<option value="238"> Mobile Legends </option>
										<option value="224"> Garena - Arena of Valor </option>
										<option value="19"> Steam Wallet IDR </option>
										<option value="170"> Steam Wallet USD </option>
										<option value="28"> Point Blank </option>
									</select>
									<div class="help-block with-errors"></div>
								</div>
								<div class="col-md-4 form-group">
									<label for="pic_npwp" class="label_form_in_dashboard">Nominal</label>
									<select id="province" name="bank_name" id="bank_name" class="form-control input-form radius_form_in_dashboard">
										<option>-Pilih Nominal-</option>
										<option value="10"> 10.000 </option>
										<option value="20"> 20.000 </option>
										<option value="25"> 25.000 </option>
										<option value="50"> 50.000 </option>
										<option value="100"> 100.000 </option>
										<option value="500"> 500.000 </option>
									</select>
								</div>
								<div class="col-md-4">
									<br><button type="submit" value="Submit" name='submit' class="btn btn-base btn-lg daftar text-weight-600 box-shadow-active" style="width: 98%;">Beli</button>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="pesawat">
						<div class="clearfix">
							<div class="col-md-12 text-center">
								<div class="counter-number">
									<i class="fa fa-rocket text-size-40px"></i>
									<div class="counternumber text-weight-300 animate margin-top-small margin-bottom-small slideUp" data-animation="slideUp" data-from="0" data-to="1400" data-speed="1500"></div>
									<div class="separator"><i class="short-line"></i><i class="fa fa-circle-o"></i><i class="fa fa-circle"></i><i class="fa fa-circle-o"></i><span class="short-line"></span></div>
									<p>Projects Completed</p>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="apartemen">
						<div class="clearfix">
							<div class="col-md-12 text-center">
								<div class="counter-number">
									<i class="fa fa-rocket text-size-40px"></i>
									<div class="counternumber text-weight-300 animate margin-top-small margin-bottom-small slideUp" data-animation="slideUp" data-from="0" data-to="1400" data-speed="1500"></div>
									<div class="separator"><i class="short-line"></i><i class="fa fa-circle-o"></i><i class="fa fa-circle"></i><i class="fa fa-circle-o"></i><span class="short-line"></span></div>
									<p>Projects Completed</p>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="keretaApi">
						<div class="clearfix">
							<div class="col-md-12 text-center">
								<div class="counter-number">
									<i class="fa fa-rocket text-size-40px"></i>
									<div class="counternumber text-weight-300 animate margin-top-small margin-bottom-small slideUp" data-animation="slideUp" data-from="0" data-to="1400" data-speed="1500"></div>
									<div class="separator"><i class="short-line"></i><i class="fa fa-circle-o"></i><i class="fa fa-circle"></i><i class="fa fa-circle-o"></i><span class="short-line"></span></div>
									<p>Projects Completed</p>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="pdam">
						<div class="clearfix">
							<div class="col-md-12 text-center">
								<div class="counter-number">
									<i class="fa fa-rocket text-size-40px"></i>
									<div class="counternumber text-weight-300 animate margin-top-small margin-bottom-small slideUp" data-animation="slideUp" data-from="0" data-to="1400" data-speed="1500"></div>
									<div class="separator"><i class="short-line"></i><i class="fa fa-circle-o"></i><i class="fa fa-circle"></i><i class="fa fa-circle-o"></i><span class="short-line"></span></div>
									<p>Projects Completed</p>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="listrikPLN">
						<div class="clearfix">
							<div class="col-md-12 text-center">
								<div class="counter-number">
									<i class="fa fa-rocket text-size-40px"></i>
									<div class="counternumber text-weight-300 animate margin-top-small margin-bottom-small slideUp" data-animation="slideUp" data-from="0" data-to="1400" data-speed="1500"></div>
									<div class="separator"><i class="short-line"></i><i class="fa fa-circle-o"></i><i class="fa fa-circle"></i><i class="fa fa-circle-o"></i><span class="short-line"></span></div>
									<p>Projects Completed</p>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="bpjs">
						<div class="clearfix">
							<div class="col-md-12 text-center">
								<div class="counter-number">
									<i class="fa fa-rocket text-size-40px"></i>
									<div class="counternumber text-weight-300 animate margin-top-small margin-bottom-small slideUp" data-animation="slideUp" data-from="0" data-to="1400" data-speed="1500"></div>
									<div class="separator"><i class="short-line"></i><i class="fa fa-circle-o"></i><i class="fa fa-circle"></i><i class="fa fa-circle-o"></i><span class="short-line"></span></div>
									<p>Projects Completed</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <!--
    #################################
        - Begin: ABOUT US -
    #################################
    -->
    <div id="about" style="padding: 50px 0 0px;">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center">
                        <h1 class="text-weight-600">Apa itu finbox?</h1>
                        <div class="separator"><i class="fa fa-circle-o"></i><i class="fa fa-circle"></i><i class="fa fa-circle-o"></i></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 col-sm-12 service text-center base">
                    <img src="{{ URL::asset('images/icon/finbox.png') }}" height="150">
                </div>
				<div class="col-md-7 col-sm-12 service  base" style="text-align: justify;">
					<br><p>Sandbox adalah testing environment untuk API. Sanbox menyedialan environment yang aman untuk developer mencoba berbagai metode & skenario pembayaran. Semua transaksi di sandbox ini menggunakan data dummy yang hanya digunakan untuk percobaan. Setelah Anda berhasil mengadaptasi API, Anda dapat beralih ke production environment.</p>
				</div>
            </div>
        </div>
    </div>
    <!-- End: ABOUT US -
    ################################################################## -->

    <!--
    #################################
        - Begin: Services -
    #################################
    -->
    <section id="services">
        <div class="container">
            <div class="row light-bg">
                <div class="col-sm-12">
                    <div class="margin-bottom-medium text-center">
                        <h1 class="text-weight-600">Our Clients</h1>
                        <div class="separator"><i class="fa fa-circle-o"></i><i class="fa fa-circle"></i><i class="fa fa-circle-o"></i></div>
						<p>Kami dipercaya oleh berbagai bisnis terkemuka di Indonesia.<br> Anda berada ditangan tangan yang tepat.</p>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="clients owl-carousel">
                        <div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
						<div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
						<div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
						<div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
						<div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div><div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
						<div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
						<div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
                    </div><br>
                </div>
                <div class="col-sm-12"><br>
                    <div class="clients owl-carousel">
                        <div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
						<div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
						<div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
						<div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
						<div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div><div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
						<div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
						<div class="item">
                            <div class="client">
                                <a href="#" class="hover-2"><img src="{{ URL::asset('images/blog/blog-1-thumb.jpg') }}" alt="Client" class="img-responsive radius_home_clients" style="height:150px"></a>
                            </div>
                        </div>
                    </div><br>
                </div>
            </div>
        </div>
    </section>
    <!-- End: WORK -
    ################################################################## -->
	
	<div class="parallax text-white margin-bottom-medium" style="min-height: 500px;background:#f65c26">
        <div class="parallax_child img-section-3" style="transform: translate3d(0px, -228.447px, 0px);"></div>
        <div class="container position-relative z-index-2 padding-top-small">
            <div class="row">
                <div class="col-sm-12">
                    <div class="margin-bottom-medium text-center">
                        <h1 class="text-weight-600">Keuntungan menjadi biller finbox</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--
    #################################
        - Begin: TEAM -
    #################################
    -->
    <div id="pay">
         <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
					<hr class="margin-bottom-xsmall" style="border-top: 2px solid #eee;">
                </div>
				<div class="col-sm-6 text-center">
					<p>Cara cepat mengintegrasikan layanan Anda dengan Finbox.</p>
				</div>
				<div class="col-sm-4 text-center">
					<input type="text" required="" class="form-control input-form radius_form_in_dashboard" name="pic_name" id="pic_name"  placeholder="Masukkan email Anda disini" value="">
				</div>
				<div class="col-sm-2 text-center">
					<button type="submit" class="btn btn-base daftar text-weight-400 text-uppercase box-shadow-active">Submit</button>
				</div>
			</div>
		</div>
    </div>
    <!-- End: TEAM -
    ################################################################## -->
</div>
    <!--
    #################################
        - Begin: FOOTER -
    #################################
    -->
    <footer id="footer" class="light-bg" style="border-top: 1px solid #CCCCCC;">
        <div class="top-footer">
            <div class="container">
                <div class="row">
					<div class="col-md-12">
						<a href="{{ url('/homepage') }}"><img src="{{ URL::asset('images/icon/finbox.png') }}" width="100"></a>
					</div>
					<div class="col-sm-3">
						<div class="widget">
                            <div class="list-link">
                                <ul class="list-ul">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Clients</a></li>
                                    <li><a href="#">Developers</a></li>
                                    <li><a href="#">Security</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>
					</div>
					<div class="col-sm-1"></div>
					<div class="col-sm-3">
						<div class="widget">
                            <div class="list-link">
                                <ul class="list-ul">
                                    <li><a href="#">Frequently Asked Question</a></li>
                                    <li><a href="#">Technical Documentation</a></li>
                                    <li><a href="#">Pricing</a></li>
                                    <li><a href="#">Payment Channel</a></li>
                                    <li><a href="#">Term Condition</a></li>
                                </ul>
                            </div>
                        </div>
					</div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End: FOOTER -
    ################################################################## -->

    <!-- Begin: BACK TO TOP -->
    <a id="back-to-top" href="#" class="back-to-top btn" style="background:#f65c26;color:#fff;">
        <i class="fa fa-chevron-up"></i>
    </a>
    <!-- End: BACK TO TOP -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
    
    <!-- Begin: REQUIRED FOR THIS PAGE ONLY -->
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.actions.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.migration.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.video.min.js') }}"></script>
    <!-- Slider JS -->
    <script type="text/javascript">
    var tpj=jQuery;         
        var revapi4;
        tpj(document).ready(function() {
            if(tpj("#rev_slider").revolution == undefined){
                revslider_showDoubleJqueryError("#rev_slider");
            }else{
                revapi6 = tpj("#rev_slider").show().revolution({
                    sliderType:"standard",
                    jsFileLocation:"{{ URL::asset('plugins/revolution/') }}",
                    sliderLayout:"fullwidth",
                    dottedOverlay:"none",
                    delay:9000,
                    navigation: {
                        keyboardNavigation:"off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation:"off",
                        mouseScrollReverse:"default",
                        onHoverStop:"on",
                        touch:{
                            touchenabled:"on",
                            swipe_threshold: 75,
                            swipe_min_touches: 1,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        }
                        ,
                        arrows: {
                            style:"metis",
                            enable:true,
                            hide_onmobile:true,
                            hide_under:800,
                            hide_onleave:true,
                            hide_delay:200,
                            hide_delay_mobile:1200,
                            tmp:'',
                            left: {
                                h_align:"left",
                                v_align:"center",
                                h_offset:20,
                                v_offset:0
                            },
                            right: {
                                h_align:"right",
                                v_align:"center",
                                h_offset:20,
                                v_offset:0
                            }
                        }
                        ,
                        bullets: {
                            enable:true,
                            hide_onmobile:false,
                            style:"uranus",
                            hide_onleave:false,
                            direction:"horizontal",
                            h_align:"center",
                            v_align:"bottom",
                            h_offset:0,
                            v_offset:20,
                            space:5,
                            tmp:'<span class="tp-bullet-inner"></span>'
                        }
                    },
                    visibilityLevels:[1240,1024,778,480],
                    gridwidth:1240,
                    gridheight:500,
                    lazyType:"none",
                    parallax: {
                        type:"mouse",
                        origo:"enterpoint",
                        speed:400,
                        levels:[5,10,15,20,25,30,35,40,45,46,47,48,49,50,51,55],
                        type:"mouse",
                    },
                    shadow:0,
                    spinner:"spinner2",
                    stopLoop:"off",
                    stopAfterLoops:-1,
                    stopAtSlide:-1,
                    shuffle:"off",
                    autoHeight:"off",
                    fullScreenAutoWidth:"off",
                    fullScreenAlignForce:"off",
                    fullScreenOffsetContainer: "",
                    fullScreenOffset: "",
                    disableProgressBar:"on",
                    hideThumbsOnMobile:"off",
                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    debugMode:false,
                    fallbacks: {
                        simplifyAll:"off",
                        nextSlideOnWindowFocus:"off",
                        disableFocusListener:false,
                    }
                });
            }
        }); /*ready*/
    </script>
    <script src="{{ URL::asset('plugins/flexslider/js/jquery.flexslider.js') }}"></script><!-- FlexSlider -->
	<script>
    $(document).ready(function () {
        $(".carousel-5").owlCarousel({
            items: 5, //10 items above 1000px browser width
            itemsDesktop: [1000, 5], //5 items between 1000px and 901px
            itemsDesktopSmall: [900, 4], // betweem 900px and 601px
            itemsTablet: [600, 3], //2 items between 600 and 0
            autoPlay: 3000,
            stopOnHover: true,
            navigation: true,
            pagination: false,
            paginationSpeed: 1000,
            goToFirstSpeed: 2000,
            singleItem: false,
            autoHeight: false,
            transitionStyle: "fade"
        });
    });

    $(document).ready(function () {
        $(".clients").owlCarousel({
            items: 6, //10 items above 1000px browser width
            itemsDesktop: [1000, 5], //5 items between 1000px and 901px
            itemsDesktopSmall: [900, 3], // betweem 900px and 601px
            itemsTablet: [600, 3], //2 items between 600 and 0
            autoPlay: 3000,
            stopOnHover: true,
            navigation: true,
            pagination: false,
            paginationSpeed: 1000,
            goToFirstSpeed: 2000,
            singleItem: false,
            autoHeight: true,
            transitionStyle: "fade"
        });
    });
    </script>
    <script>
    $(document).ready(function ($) {
            $(".flexslider").flexslider({
            animation: "slide",
            start: function (slider) {
                $('.flexslider').removeClass('loading');
            }
        });
        $(".flexslider-thumbnails").flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            start: function (slider) {
                $('.flexslider-thumbnails').removeClass('loading');
            }
        });
    });
    </script>
    <!-- End: REQUIRED FOR THIS PAGE ONLY -->

</body>
</html>