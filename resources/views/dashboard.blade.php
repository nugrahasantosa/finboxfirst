@extends('layout')

@section('pageTitle')
	Dashboard - Finbox
@stop

@section('sidebarActiveDashboard')
    class="active"
@stop

@section('customPluginCss')
	<style>
		#container {
		min-width: 310px;
		width: 100%;
		height: 400px;
		margin: 0 auto
	}
	</style>
@endsection

@section('customPluginJs')
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/series-label.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script>
		Highcharts.chart('container', {

			title: {
				text: 'Grafik Invoice dan Pembayaran'
			},

			subtitle: {
				text: 'Bulan Ini'
			},

			yAxis: {
				title: {
					text: 'Number of Employees'
				}
			},
			legend: {
				layout: 'vertical',
				align: 'right',
				verticalAlign: 'middle'
			},

			plotOptions: {
				series: {
					label: {
						connectorAllowed: false
					},
					pointStart: 2010
				}
			},

			series: [{
				name: 'Invoice',
				data: [43934, 52503, 57177, 69658, 97031, 119931, 137133, 154175]
			}, {
				name: 'Payment',
				data: [11744, 17722, 16005, 19771, 20185, 24377, 32147, 39387]
			}],

			responsive: {
				rules: [{
					condition: {
						maxWidth: 500
					},
					chartOptions: {
						legend: {
							layout: 'horizontal',
							align: 'center',
							verticalAlign: 'bottom'
						}
					}
				}]
			}

		});
		
		// Make monochrome colors
		var pieColors = (function () {
			var colors = ['#ffb01f', '#8085e9'],
				base = Highcharts.getOptions().colors[0],
				i;

			for (i = 0; i < 10; i += 1) {
				// Start out with a darkened base color (negative brighten), and end
				// up with a much brighter color
				colors.push(Highcharts.Color(base).brighten((i - 3) / 7).get());
			}
			return colors;
		}());

		// Build the chart
		Highcharts.chart('container2', {
			chart: {
				plotBackgroundColor: null,
				plotBorderWidth: null,
				plotShadow: false,
				type: 'pie'
			},
			title: {
				text: ''
			},
			tooltip: {
				pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions: {
				pie: {
					allowPointSelect: true,
					cursor: 'pointer',
					colors: pieColors,
					dataLabels: {
						enabled: true,
						format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
						distance: -50,
						filter: {
							property: 'percentage',
							operator: '>',
							value: 4
						}
					}
				}
			},
			series: [{
				name: 'Share',
				data: [
					{ name: 'Total Invoice Terbayar', y: 61.41 },
					{ name: 'Total Invoice Belum Terbayar', y: 11.84 }
				]
			}]
		});
    </script>
@endsection

@section('content')
    <!--
    #################################
        - Begin: PORTFOLIO -
    #################################
    -->
	<h4 class="text-weight-600" style="color:#666666;;margin: 20px 0px 20px 0px;">Dashboard</h4>
	<div class="row">
		<div class="col-md-3">
			<div class="border-dark form_in_dashboard">
				<div class="col-sm-12">
					<h2 style="padding-right: 0px;color:#ffb01f;font-weight: 700;">Rp. 950.000</h2>
					<h4 style="padding-right: 0px;color:#ffb01f;font-weight: 400;margin: 0px;">5 Invoice</h4>
				</div>
				
				<div class="row">
					<div class="col-sm-12">
						<h4 class="text-center" style="padding-right:10px;">Pembayaran Bulan Ini</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="border-dark form_in_dashboard">
				<div class="col-sm-12">
					<h4 style="padding-right: 0px;color:#ffb01f;font-weight: 700;margin-top: 25px;margin-bottom: 0px;">Terbayar : 5</h4>
					<h4 style="padding-right: 0px;color:#ffb01f;font-weight: 700;">Belum Terbayar : 2</h4>
				</div>
				
				<div class="row">
					<div class="col-sm-12">
						<h4 class="text-center" style="padding-right:10px;">Tagihan Bulan Ini</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="border-dark form_in_dashboard">
				<div class="col-sm-12">
					<h4 style="padding-right: 0px;color:#ffb01f;font-weight: 700;margin-top: 25px;margin-bottom: 0px;">Terbayar : 5</h4>
					<h4 style="padding-right: 0px;color:#ffb01f;font-weight: 700;">Belum Terbayar : 2</h4>
				</div>
				
				<div class="row">
					<div class="col-sm-12">
						<h4 class="text-center" style="padding-right:10px;">Tagihan Bulan Lalu</h4>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="border-dark form_in_dashboard">
				<div class="col-sm-12">
					<h2 style="padding-right: 0px;color:#ffb01f;font-weight: 700;">Rp. 250.000</h2>
					<h4 style="padding-right: 0px;color:#ffb01f;font-weight: 400;margin: 0px;">2 Invoice</h4>
				</div>
				
				<div class="row">
					<div class="col-sm-12">
						<h4 class="text-center" style="padding-right:10px;">Total Outstanding Invoice</h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<div class="border-dark form_in_dashboard" style="min-height:470px">
				<h4 class="header_form_in_dashboard">Total Revenue</h4>
				<hr class="margin-bottom-xsmall hr_form_in_dashboard">
				<div class="row">
					<div class="col-sm-12">
						<div id="container2" style="min-width: 50px; height: 300px; max-width: 300px; margin: 0 auto"></div>
					</div>
					<div class="col-sm-12">
						<h2 class="text-center" style="padding-right: 0px;color:#ffb01f;font-weight: 700;">Rp. 950.000</h2>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="border-dark form_in_dashboard">
				<h4 class="header_form_in_dashboard">Chart</h4>
				<hr class="margin-bottom-xsmall hr_form_in_dashboard">
				<div class="row">
					<div class="col-sm-12">
						<div id="container"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- End: PORTFOLIO -
    ################################################################## -->
@endsection