@extends('layout')

@section('pageTitle')
	Faq - Finbox
@stop

@section('sidebarActiveFaq')
    class="active"
@stop

@section('content')
<h4 class="text-weight-600" style="color:#666666;;margin: 20px 0px 20px 0px;">FAQ</h4>
<div class="border-dark form_in_dashboard">
	<div class="row">
		<div class="col-md-12">
			<div class="panel-group accordion" id="accordion1" role="tablist" aria-multiselectable="true" style="margin-bottom:0px">
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne1" aria-expanded="true">
								<span class="icon-collapse-sign text-weight-700">1</span>
								Mauris cubilia molestie?
							</a>
						</h4>
					</div>
					<div id="collapseOne1" class="panel-collapse collapse in">
						<div class="panel-body">
							<p class="text-justify">Eu tellus est lobortis ullamcorper maecenas turpis parturient litora, facilisi posuere dolor Hac, tincidunt eget rutrum commodo quam nisl aptent. Dolor. Dapibus orci ligula amet risus mi euismod at Mollis natoque. Integer turpis gravida enim ultrices, nisi. Fusce semper lectus a id neque nulla habitant, sagittis leo. Primis torquent duis ridiculus donec aliquet luctus nostra Quisque hendrerit urna, cum Sociis senectus Magnis elit dictumst fringilla penatibus tempus feugiat urna.</p>
							<p class="text-justify">Placerat feugiat. Leo tempor non quam. Rhoncus malesuada aenean curae; iaculis erat sociis nibh eleifend sagittis suspendisse et euismod netus praesent vel aenean. Praesent ipsum nisi iaculis elit pretium litora quam elit dis sem bibendum litora purus nec, vel aliquam nisi sapien Vel pellentesque platea leo.</p>
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo1" aria-expanded="false">
								<span class="icon-collapse-sign text-weight-700">2</span>
								Consequat nonummy lacus?
							</a>
						</h4>
					</div>
					<div id="collapseTwo1" class="panel-collapse collapse" role="tabpanel">
						<div class="panel-body">
							<p class="text-justify">Eu tellus est lobortis ullamcorper maecenas turpis parturient litora, facilisi posuere dolor Hac, tincidunt eget rutrum commodo quam nisl aptent. Dolor. Dapibus orci ligula amet risus mi euismod at Mollis natoque. Integer turpis gravida enim ultrices, nisi. Fusce semper lectus a id neque nulla habitant, sagittis leo. Primis torquent duis ridiculus donec aliquet luctus nostra Quisque hendrerit urna, cum Sociis senectus Magnis elit dictumst fringilla penatibus tempus feugiat urna.</p>
							<p class="text-justify">Placerat feugiat. Leo tempor non quam. Rhoncus malesuada aenean curae; iaculis erat sociis nibh eleifend sagittis suspendisse et euismod netus praesent vel aenean. Praesent ipsum nisi iaculis elit pretium litora quam elit dis sem bibendum litora purus nec, vel aliquam nisi sapien Vel pellentesque platea leo.</p>
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseThree1" aria-expanded="false">
								<span class="icon-collapse-sign text-weight-700">3</span>
								Semper placerat lobortis nunc erat?
							</a>
						</h4>
					</div>
					<div id="collapseThree1" class="panel-collapse collapse" role="tabpanel">
						<div class="panel-body">
							<p class="text-justify">Eu tellus est lobortis ullamcorper maecenas turpis parturient litora, facilisi posuere dolor Hac, tincidunt eget rutrum commodo quam nisl aptent. Dolor. Dapibus orci ligula amet risus mi euismod at Mollis natoque. Integer turpis gravida enim ultrices, nisi. Fusce semper lectus a id neque nulla habitant, sagittis leo. Primis torquent duis ridiculus donec aliquet luctus nostra Quisque hendrerit urna, cum Sociis senectus Magnis elit dictumst fringilla penatibus tempus feugiat urna.</p>
							<p class="text-justify">Placerat feugiat. Leo tempor non quam. Rhoncus malesuada aenean curae; iaculis erat sociis nibh eleifend sagittis suspendisse et euismod netus praesent vel aenean. Praesent ipsum nisi iaculis elit pretium litora quam elit dis sem bibendum litora purus nec, vel aliquam nisi sapien Vel pellentesque platea leo.</p>
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseFour1" aria-expanded="false">
								<span class="icon-collapse-sign text-weight-700">4</span>
								Posuere ridiculus ante blandit hendrerit?
							</a>
						</h4>
					</div>
					<div id="collapseFour1" class="panel-collapse collapse" role="tabpanel">
						<div class="panel-body">
							<p class="text-justify">Eu tellus est lobortis ullamcorper maecenas turpis parturient litora, facilisi posuere dolor Hac, tincidunt eget rutrum commodo quam nisl aptent. Dolor. Dapibus orci ligula amet risus mi euismod at Mollis natoque. Integer turpis gravida enim ultrices, nisi. Fusce semper lectus a id neque nulla habitant, sagittis leo. Primis torquent duis ridiculus donec aliquet luctus nostra Quisque hendrerit urna, cum Sociis senectus Magnis elit dictumst fringilla penatibus tempus feugiat urna.</p>
							<p class="text-justify">Placerat feugiat. Leo tempor non quam. Rhoncus malesuada aenean curae; iaculis erat sociis nibh eleifend sagittis suspendisse et euismod netus praesent vel aenean. Praesent ipsum nisi iaculis elit pretium litora quam elit dis sem bibendum litora purus nec, vel aliquam nisi sapien Vel pellentesque platea leo.</p>
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseFive1" aria-expanded="false">
								<span class="icon-collapse-sign text-weight-700">5</span>
								Sodales proin aenean tempor?
							</a>
						</h4>
					</div>
					<div id="collapseFive1" class="panel-collapse collapse" role="tabpanel">
						<div class="panel-body">
							<p class="text-justify">Eu tellus est lobortis ullamcorper maecenas turpis parturient litora, facilisi posuere dolor Hac, tincidunt eget rutrum commodo quam nisl aptent. Dolor. Dapibus orci ligula amet risus mi euismod at Mollis natoque. Integer turpis gravida enim ultrices, nisi. Fusce semper lectus a id neque nulla habitant, sagittis leo. Primis torquent duis ridiculus donec aliquet luctus nostra Quisque hendrerit urna, cum Sociis senectus Magnis elit dictumst fringilla penatibus tempus feugiat urna.</p>
							<p class="text-justify">Placerat feugiat. Leo tempor non quam. Rhoncus malesuada aenean curae; iaculis erat sociis nibh eleifend sagittis suspendisse et euismod netus praesent vel aenean. Praesent ipsum nisi iaculis elit pretium litora quam elit dis sem bibendum litora purus nec, vel aliquam nisi sapien Vel pellentesque platea leo.</p>
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseSix1" aria-expanded="false">
								<span class="icon-collapse-sign text-weight-700">6</span>
								Nonummy augue penatibus?
							</a>
						</h4>
					</div>
					<div id="collapseSix1" class="panel-collapse collapse" role="tabpanel">
						<div class="panel-body">
							<p class="text-justify">Eu tellus est lobortis ullamcorper maecenas turpis parturient litora, facilisi posuere dolor Hac, tincidunt eget rutrum commodo quam nisl aptent. Dolor. Dapibus orci ligula amet risus mi euismod at Mollis natoque. Integer turpis gravida enim ultrices, nisi. Fusce semper lectus a id neque nulla habitant, sagittis leo. Primis torquent duis ridiculus donec aliquet luctus nostra Quisque hendrerit urna, cum Sociis senectus Magnis elit dictumst fringilla penatibus tempus feugiat urna.</p>
							<p class="text-justify">Placerat feugiat. Leo tempor non quam. Rhoncus malesuada aenean curae; iaculis erat sociis nibh eleifend sagittis suspendisse et euismod netus praesent vel aenean. Praesent ipsum nisi iaculis elit pretium litora quam elit dis sem bibendum litora purus nec, vel aliquam nisi sapien Vel pellentesque platea leo.</p>
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseSeven1" aria-expanded="false">
								<span class="icon-collapse-sign text-weight-700">7</span>
								Imperdiet sem eleifend tincidunt volutpat viverra?
							</a>
						</h4>
					</div>
					<div id="collapseSeven1" class="panel-collapse collapse" role="tabpanel">
						<div class="panel-body">
							<p class="text-justify">Eu tellus est lobortis ullamcorper maecenas turpis parturient litora, facilisi posuere dolor Hac, tincidunt eget rutrum commodo quam nisl aptent. Dolor. Dapibus orci ligula amet risus mi euismod at Mollis natoque. Integer turpis gravida enim ultrices, nisi. Fusce semper lectus a id neque nulla habitant, sagittis leo. Primis torquent duis ridiculus donec aliquet luctus nostra Quisque hendrerit urna, cum Sociis senectus Magnis elit dictumst fringilla penatibus tempus feugiat urna.</p>
							<p class="text-justify">Placerat feugiat. Leo tempor non quam. Rhoncus malesuada aenean curae; iaculis erat sociis nibh eleifend sagittis suspendisse et euismod netus praesent vel aenean. Praesent ipsum nisi iaculis elit pretium litora quam elit dis sem bibendum litora purus nec, vel aliquam nisi sapien Vel pellentesque platea leo.</p>
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseEight1" aria-expanded="false">
								<span class="icon-collapse-sign text-weight-700">8</span>
								Venenatis enim ultricies praesent?
							</a>
						</h4>
					</div>
					<div id="collapseEight1" class="panel-collapse collapse" role="tabpanel">
						<div class="panel-body">
							<p class="text-justify">Eu tellus est lobortis ullamcorper maecenas turpis parturient litora, facilisi posuere dolor Hac, tincidunt eget rutrum commodo quam nisl aptent. Dolor. Dapibus orci ligula amet risus mi euismod at Mollis natoque. Integer turpis gravida enim ultrices, nisi. Fusce semper lectus a id neque nulla habitant, sagittis leo. Primis torquent duis ridiculus donec aliquet luctus nostra Quisque hendrerit urna, cum Sociis senectus Magnis elit dictumst fringilla penatibus tempus feugiat urna.</p>
							<p class="text-justify">Placerat feugiat. Leo tempor non quam. Rhoncus malesuada aenean curae; iaculis erat sociis nibh eleifend sagittis suspendisse et euismod netus praesent vel aenean. Praesent ipsum nisi iaculis elit pretium litora quam elit dis sem bibendum litora purus nec, vel aliquam nisi sapien Vel pellentesque platea leo.</p>
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseNine1" aria-expanded="false">
								<span class="icon-collapse-sign text-weight-700">9</span>
								Gravida potenti ad dignissim class urna tempor?
							</a>
						</h4>
					</div>
					<div id="collapseNine1" class="panel-collapse collapse" role="tabpanel">
						<div class="panel-body">
							<p class="text-justify">Eu tellus est lobortis ullamcorper maecenas turpis parturient litora, facilisi posuere dolor Hac, tincidunt eget rutrum commodo quam nisl aptent. Dolor. Dapibus orci ligula amet risus mi euismod at Mollis natoque. Integer turpis gravida enim ultrices, nisi. Fusce semper lectus a id neque nulla habitant, sagittis leo. Primis torquent duis ridiculus donec aliquet luctus nostra Quisque hendrerit urna, cum Sociis senectus Magnis elit dictumst fringilla penatibus tempus feugiat urna.</p>
							<p class="text-justify">Placerat feugiat. Leo tempor non quam. Rhoncus malesuada aenean curae; iaculis erat sociis nibh eleifend sagittis suspendisse et euismod netus praesent vel aenean. Praesent ipsum nisi iaculis elit pretium litora quam elit dis sem bibendum litora purus nec, vel aliquam nisi sapien Vel pellentesque platea leo.</p>
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseTen1" aria-expanded="false">
								<span class="icon-collapse-sign text-weight-700">10</span>
								Quis a habitant sociosqu taciti litora sagittis?
							</a>
						</h4>
					</div>
					<div id="collapseTen1" class="panel-collapse collapse" role="tabpanel">
						<div class="panel-body">
							<p class="text-justify">Eu tellus est lobortis ullamcorper maecenas turpis parturient litora, facilisi posuere dolor Hac, tincidunt eget rutrum commodo quam nisl aptent. Dolor. Dapibus orci ligula amet risus mi euismod at Mollis natoque. Integer turpis gravida enim ultrices, nisi. Fusce semper lectus a id neque nulla habitant, sagittis leo. Primis torquent duis ridiculus donec aliquet luctus nostra Quisque hendrerit urna, cum Sociis senectus Magnis elit dictumst fringilla penatibus tempus feugiat urna.</p>
							<p class="text-justify">Placerat feugiat. Leo tempor non quam. Rhoncus malesuada aenean curae; iaculis erat sociis nibh eleifend sagittis suspendisse et euismod netus praesent vel aenean. Praesent ipsum nisi iaculis elit pretium litora quam elit dis sem bibendum litora purus nec, vel aliquam nisi sapien Vel pellentesque platea leo.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- End: FAQs -
################################################################## -->

@endsection