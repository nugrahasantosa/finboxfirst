@extends('layout')

@section('pageTitle')
	Contact Us - Finbox
@stop

@section('sidebarActiveContactUs')
    class="active"
@stop

@section('content')


<!--
#################################
	- Begin: CONTACT US -
#################################
-->
<section class="padding-top-large padding-bottom-small">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-sm-8 padding-bottom-medium">
				<h3 class="page-header text-weight-300">Get in Touch with Us</h3>
				<p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime. nobis est eligendi optio cumque nihil impedit quo minus.</p>

				<form method="POST" name="assets/contact_form" action="">

					<div class="row">
						<div class="col-md-4 col-sm-4">
							<div class="form-group">
								<label class="control-label">Name</label>
								<input type="text" placeholder="Name" class="form-control input-lg" name="name" value="">
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="form-group">
								<label class="control-label">Email</label>
								<input type="email" placeholder="Email" class="form-control input-lg" name="email" value="">
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="form-group">
								<label class="control-label">Phone Number</label>
								<input type="text" placeholder="Phone Number" class="form-control input-lg" name="phonenumber" value="">
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label class="control-label">Address</label>
								<input type="text" placeholder="Address" class="form-control input-lg" name="address" value="">
							</div>
						</div>
						<div class="col-md-6 col-sm-6">
							<div class="form-group">
								<label class="control-label">Company</label>
								<input type="text" placeholder="Company" class="form-control input-lg" name="company" value="">
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="form-group">
								<label class="control-label">Subject</label>
								<input type="text" placeholder="Subject" class="form-control input-lg" name="subject" value="">
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="form-group">
								<label class="control-label">Your Message!</label>
								<textarea rows="4" cols=30 placeholder="Your Message!" class="form-control input-lg" name="message"></textarea>
							</div>
						</div>
					</div>
					<button type="submit" value="Submit" name='submit' class="btn btn-base btn-lg text-weight-600 text-uppercase box-shadow-active">Submit</button>
				</form>
				
				<script language='JavaScript' type='text/javascript'>
					function refreshCaptcha() {
						var img = document.images['captchaimg'];
						img.src = img.src.substring(0, img.src.lastIndexOf("?")) + "?rand=" + Math.random() * 1000;
					}
				</script>
				<!-- End: FORM -->

			</div>
			<div class="col-md-4 col-sm-4 padding-bottom-medium">
				<h3 class="page-header text-weight-300">Contact Us</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
				<address> <abbr title="Address"><strong>Address:</strong></abbr><br> 1355 Market Street, Suite 900<br> San Francisco, CA 94103 </address>
				<address> <abbr title="Phone"><strong>Phone:</strong></abbr><br> (123) 456-7890 </address>
				<address> <abbr title="Email"><strong>Email:</strong></abbr><br> <a href="mailto:#">info@imax.com</a> </address>
				<ul class="social-icon base margin-top-small margin-bottom-small">
					<li><a title="" data-placement="top" data-toggle="tooltip" href="#" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
					<li><a title="" data-placement="top" data-toggle="tooltip" href="#" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
					<li><a title="" data-placement="top" data-toggle="tooltip" href="#" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
					<li><a title="" data-placement="top" data-toggle="tooltip" href="#" data-original-title="Youtube"><i class="fa fa-youtube"></i></a></li>
					<li><a title="" data-placement="top" data-toggle="tooltip" href="#" data-original-title="Google"><i class="fa fa-google"></i></a></li>
				</ul>

				<!-- Begin: MAP -->
				<div class="is-map width-100 height-320px" data-latlng="48.868974, 2.330663" data-zoom="13">
					<p data-marker-image="assets/images/map-marker.png" data-latlng="48.860617, 2.337650" data-title="Location 1">
						<strong>Location 1:</strong>
						<br /> Main Office Here
					</p>
					<p data-marker-image="assets/images/map-marker.png" data-latlng="48.865491, 2.321137" data-title="Location 2">
						<strong>Location 2:</strong>
						<br /> 2nd Branch Office Here
					</p>
					<p data-marker-image="assets/images/map-marker.png" data-latlng="48.871977, 2.331612" data-title="Location 3">
						<strong>Location 3:</strong>
						<br /> 3rd Branch Office Here
					</p>
				</div>
				<!-- End: MAP -->

			</div>
		</div>
	</div>
</section>
<!-- End: CONTACT US -
################################################################## -->
@endsection