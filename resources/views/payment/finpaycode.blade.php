<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Payment Page |Finbox</title>
	
    <link rel="icon" href="{{ URL::asset('images/favicon.png') }}" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <!-- Begin: MAIN CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/plugins-min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/menu-overright.css') }}">
    <link href="{{ URL::asset('css/colors/orange-burgundy.css') }}" type="text/css" media="all" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/vendor.css') }}">
	<style>
		.header_nav{
			height: 61px;
			position: fixed;
			right: 0;
			left: 0;
			z-index: 1030;
			margin-bottom: 0;
			top: 0;
		}
		.accordion .panel-heading > .panel-title > a {
			display: block;
			font-size: 20px;
			font-weight: 500;
		}
		.panel .panel-heading a:hover, .panel .panel-heading a:active, .panel .panel-heading a[aria-expanded="true"] {
			color: #fd9912;
		}
		.main-panel > .content {
			margin-top: 0px;
			padding: 0px 15px;
			min-height: calc(100vh - 123px);
		}
	</style>
    <!-- End: MAIN CSS -->

    <!-- Begin: HTML5SHIV FOR IE8 -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/internetexplorer/html5shiv.min.js"></script>
      <script src="assets/js/internetexplorer/respond.min.js"></script>
    <![endif]-->
    <!-- end: HTML5SHIV FOR IE8 -->
</head>
<body class="switcher_boxed boxed">
    <!-- Begin: HEADER SEARCH -->
    <div class="modal fade header-search" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="search">
                <form method="get" action="search.html">
                    <div class="form-group full-width">
                        <div class="input-group">
                            <input type="text" class="form-control input-lg" placeholder="Search Here..." value="" name="q">
                            <div class="input-group-btn "><button type="submit" class="btn btn-base btn-lg"><i class="fa fa-search"></i></button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End: HEADER SEARCH -->

    <!--
    #################################
        - Begin: HEADER -
    #################################
    -->
    <header class="main-header sticky-header header_nav">
        <nav class="navbar navbar-default">
            <div class="container position-relative">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header border">
                    <!-- Begin: RESPONSIVE MENU TOGGLER -->
                    <button type="button" class="navbar-toggle" data-toggle="modal" data-target=".header-search">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-search"></i>
                    </button>
                    <!-- End: RESPONSIVE MENU TOGGLER -->
                    <!-- Begin: RESPONSIVE MENU TOGGLER -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-navbar-collapse-2">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-shopping-bag"></i>
                    </button>
                    <!-- End: RESPONSIVE MENU TOGGLER -->
                    <!-- Begin: RESPONSIVE MENU TOGGLER -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars"></i>
                    </button>
                    <!-- End: RESPONSIVE MENU TOGGLER -->
                    <!-- Begin: LOGO -->
                     <img width="140" height="40"src="{{ asset('images/icon/finbox.png') }}" style="margin-top: 10px;margin-bottom: 10px;">
                    <!-- End: LOGO -->
                </div>
            </div>
        </nav>
    </header>
    <!-- End: HEADER -
    ################################################################## -->


    <!--
    #################################
        - Begin: SHOP -
    #################################
    -->
    <div style="background: #f5f5f5;padding: 80px 0 0;">
		<div class="wrapper">
			<div class="main-panel" style="float: left;width: 100%;">
				<div class="content">
					<div class="container-fluid"><br><br><br><br>
						<div class="row">
							<div class="col-lg-1 col-md-12"></div>
							<div class="col-lg-10 col-md-12">
								<div class="card" style="border-radius: 0px;">
									<div style="padding: 10px 25px 10px;background: #FAFAFA;">
										<h5>Pembayaran via Finpaycode</h5>
									</div>
									<div style="padding: 20px 25px 20px;" class="text-center">
										<div><h4>Batas Pembayaran : <strong id="remainingTime">24 Jam 00 Menit 00 Detik</strong></h4>
										<p>Jumlah tagihan</p>
										<h3><strong>Rp {{number_format($amount,'2',',','.')}}</strong></h3>
										<h4>Finpaycode Anda : </h4>
										<h2 style="color:#fd9912;"><strong>{{$payment_code}}</strong></h2>
									</div>
									<div class="card-header" style="background: #FAFAFA;">
										<div class="nav-tabs-navigation">
											<div class="nav-tabs-wrapper">
												<div class="clearfix">
													<h4>Petunjuk Pembayaran Finpaycode</h4>
												</div>
											</div>
										</div>
									</div>
									<p style="margin-left: 2%;margin-right: 2%;margin-bottom: 5%;">Pembayaranmu dicatat dengan nomor invoice pembayaran <strong style="color: #fd9912">#INV123456789</strong>. Finbox akan melakukan verifikasi otomatis paling lama 30 menit setelah Anda melakukan pembayaran. Jika Anda menghadapi kendala mengenai pembayaran, silahkan langsung hubungi <a href="#" style="color: #fd9912">Customer Service Finbox</a>.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- End: SHOP -
    ################################################################## -->
	<footer id="footer" class="light-bg" style="border-top-style: solid !important;border-top-width: 2px!important; border-top-color: #e7e7e7 !important;background:#fff;">
        <div class="top-footer" style="padding: 20px 0 20px;">
            <div class="" style="margin-left: 4%;">
                <div class="row">
					<br><div class="col-sm-3">
							<a href="{{ url('/homepage') }}"><img src="{{ URL::asset('images/icon/finbox.png') }}" width="100"></a>
							<p style="font-size: 12px;">© 2018, PT FINNET INDONESIA</p>
					</div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End: BACK TO TOP -->
    <button type="button" class="btn" data-toggle="modal" data-target="#myModal" data-remote="https://www.tutorialrepublic.com/">Launch modal</button>

<div id="myModal" class="modal hide fade">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> × </button>
        <h3 id="myModalLabel">Modal header</h3>
    </div>
    <div class="modal-body">
       <!-- remote content will be inserted here via jQuery load() -->
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
	<script>	
		function Timer(duration, display) 
		{
			var timer = duration, hours, minutes, seconds;
			setInterval(function () {
				hours = parseInt((timer /3600)%24, 10)
				minutes = parseInt((timer / 60)%60, 10)
				seconds = parseInt(timer % 60, 10);

						hours = hours < 10 ? "0" + hours : hours;
				minutes = minutes < 10 ? "0" + minutes : minutes;
				seconds = seconds < 10 ? "0" + seconds : seconds;

				display.text(hours +" Jam "+minutes + " Menit " + seconds + " Detik");

						--timer;
			}, 1000);
		}

		jQuery(function ($) 
		{
			var twentyFourHours = 24 * 60 * 60;
			var display = $('#remainingTime');
			Timer(twentyFourHours, display);
		});
	</script>

</body>
</html>