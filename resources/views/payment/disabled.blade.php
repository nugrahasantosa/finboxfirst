<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Payment Page |Finbox</title>
	
    <link rel="icon" href="{{ URL::asset('images/favicon.png') }}" type="image/png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <!-- Begin: MAIN CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/plugins-min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/menu-overright.css') }}">
    <link href="{{ URL::asset('css/colors/orange-burgundy.css') }}" type="text/css" media="all" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/vendor.css') }}">
	<style>
		.header_nav{
			height: 61px;
			position: fixed;
			right: 0;
			left: 0;
			z-index: 1030;
			margin-bottom: 0;
			top: 0;
		}
		.accordion .panel-heading > .panel-title > a {
			display: block;
			font-size: 20px;
			font-weight: 500;
		}
		.panel .panel-heading a:hover, .panel .panel-heading a:active, .panel .panel-heading a[aria-expanded="true"] {
			color: #fd9912;
		}
		.main-panel > .content {
			margin-top: 0px;
			padding: 0px 15px;
			min-height: calc(100vh - 123px);
		}
		h6, .h6 {
			font-size: 1em;
			text-transform: capitalize;
			font-weight: 500;
		}
		.accordion .panel-heading > .panel-title > a {
			display: block;
			font-size: 18px;
			font-weight: 500;
		}
	</style>
    <!-- End: MAIN CSS -->

    <!-- Begin: HTML5SHIV FOR IE8 -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/internetexplorer/html5shiv.min.js"></script>
      <script src="assets/js/internetexplorer/respond.min.js"></script>
    <![endif]-->
    <!-- end: HTML5SHIV FOR IE8 -->
</head>
<body class="switcher_boxed boxed">
    <!-- Begin: HEADER SEARCH -->
    <div class="modal fade header-search" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="search">
                <form method="get" action="search.html">
                    <div class="form-group full-width">
                        <div class="input-group">
                            <input type="text" class="form-control input-lg" placeholder="Search Here..." value="" name="q">
                            <div class="input-group-btn "><button type="submit" class="btn btn-base btn-lg"><i class="fa fa-search"></i></button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End: HEADER SEARCH -->

    <!--
    #################################
        - Begin: HEADER -
    #################################
    -->
    <header class="main-header sticky-header header_nav">
        <nav class="navbar navbar-default">
            <div class="container position-relative">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header border">
                    <!-- Begin: RESPONSIVE MENU TOGGLER -->
                    <button type="button" class="navbar-toggle" data-toggle="modal" data-target=".header-search">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-search"></i>
                    </button>
                    <!-- End: RESPONSIVE MENU TOGGLER -->
                    <!-- Begin: RESPONSIVE MENU TOGGLER -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-navbar-collapse-2">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-shopping-bag"></i>
                    </button>
                    <!-- End: RESPONSIVE MENU TOGGLER -->
                    <!-- Begin: RESPONSIVE MENU TOGGLER -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars"></i>
                    </button>
                    <!-- End: RESPONSIVE MENU TOGGLER -->
                    <!-- Begin: LOGO -->
                     <img width="140" height="40"src="{{ asset('images/icon/finbox.png') }}" style="margin-top: 10px;margin-bottom: 10px;">
                    <!-- End: LOGO -->
                </div>
            </div>
        </nav>
    </header>
    <!-- End: HEADER -
    ################################################################## -->


    <!--
    #################################
        - Begin: SHOP -
    #################################
    -->
    <div class="padding-bottom-medium" style="background: #f5f5f5;padding: 80px 0 70px;">
		<div class="wrapper">
			<div class="main-panel" style="float: left;width: 100%;">
				<div class="content">
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-8 col-md-12">
								<h2 style="margin-bottom: -15px;">Transaksi Dibatalkan.</h2>
								<h4 style="margin-bottom: -15px;">Transaksi Anda telah dibatalkan atau diubah oleh sistem Finbox, silahkan tunggu invoice baru Anda.</h4>
								<div class="card">
									<div class="card-header">
										<div class="nav-tabs-navigation">
											<div class="nav-tabs-wrapper">
												<div class="clearfix">
													<table width="100%" style="margin: 10px;">
														<tr>
															<td><p class="category">Invoice Id</p></td>
															<td><h4 class="card-title">#{{$invoiceHead->invoice_number}}</h4></td>
														</tr>
														<tr>
															<td><p class="category">Payment Date</p></td>
															<td><h4 class="card-title">-
															</h4></td>
														</tr>
														<tr>
															<td><p class="category">Payment Method</p></td>
															<td><h4 class="card-title">-</h4></td>
														</tr>
														<tr>
															<td><p class="category">Payment Status</p></td>
															<td><h4 class="card-title">Disabled</h4></td>
														</tr>
													</table>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4 col-md-12">
								<h3 style="margin-bottom: -15px;">Detail Pembayaran</h3>
								<div class="card" style="margin-bottom: 0px;max-height: 450px;overflow: auto;">
									<div class="card-content table-responsive" style="margin: 10px;">
										<table width="100%">
											<tr>
												<td><p class="category">Nomor Invoice</p></td>
												<td><h4 class="card-title">: {{$invoiceHead->invoice_number}}</h4></td>
											</tr>
											<tr>
												<td><p class="category">Penerima</p></td>
												<td><h4 class="card-title">: {{$invoiceHead->recipient_name}}</h4></td>
											</tr>
										</table>
										<hr style="border-bottom: 2px solid #d4d4d4;"></hr>
										<table class="col-md-12" style="font-size: 12px;line-height: 2;margin-bottom: 20px;">
											<tr style="font-size: 15px;">
												<td style="width:50%;"><h4 class="card-title">Name</h4></td>
												<td class="pull-right"><h4 class="card-title">Price</h4></td>
											</tr>
											@foreach($invoiceItem as $data)
												<tr style="font-size: 15px;">
													<td style="width:50%;">{{$data->name}}
													</td>
													<td class="pull-right">Rp. {{$data->amount_total}}
													</td>
												</tr>
											@endforeach
											<tr style="font-size: 18px;">
												<td>Cart Subtotal</td>
												<td class="pull-right">Rp. {{$invoiceHead->total}}</td>
											</tr>
											<tr style="font-size: 18px;">
												<td>Diskon</td>
												<td class="pull-right">{{$invoiceHead->discount==0?$invoiceHead->discount:ceil($invoiceHead->total/$invoiceHead->discount)}}%</td>
											</tr>
											<tr style="color:#f65c26;font-size: 15px;">
												<td><h4>Total Tagihan</h4></td>
												<td class="pull-right"><h4>Rp.  {{$invoiceHead->total-$invoiceHead->discount}}</h4></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="card" style="margin-top: -10px;background: #e6e6e6;margin-bottom: 5px;">
									<div class="card-header">
										<div class="nav-tabs-navigation">
											<div class="nav-tabs-wrapper">
												<div class="clearfix" style="text-align: center;font-weight: 700;">
													<p style="margin-top: 5px;font-weight: 700;">Status Pembayaran</p>
													<h3 style="color:#f65c26;font-weight: 700;margin-top: 0px;"><?php echo 'DISABLED'; ?></h3>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    <!-- End: SHOP -
    ################################################################## -->
	<footer id="footer" class="light-bg" style="border-top-style: solid !important;border-top-width: 2px!important; border-top-color: #e7e7e7 !important;background:#fff;">
        <div class="top-footer" style="padding: 20px 0 20px;">
            <div class="" style="margin-left: 4%;">
                <div class="row">
					<br><div class="col-sm-3">
							<a href="{{ url('/homepage') }}"><img src="{{ URL::asset('images/icon/finbox.png') }}" width="100"></a>
							<p style="font-size: 12px;">© 2018, PT FINNET INDONESIA</p>
					</div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End: BACK TO TOP -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>

</body>
</html>