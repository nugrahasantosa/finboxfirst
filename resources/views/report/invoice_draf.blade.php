@extends('layout')

@section('pageTitle')
	Invoice List - Finbox
@stop

@section('sidebarActiveReport')
    class="active"
@stop

@section('customPluginCss')
	<style>
		input[type=search] {
			-webkit-appearance: none;
			border-bottom: 3px solid #ffb01f;
			border-top: none;
			border-left: none;
			border-right: none;
		}
		select{
			border-bottom: 2px solid #ffb01f;
			border-left: none;
			border-right: none;
			border-top: none;
		}
		#invoice_previous, #invoice_next{
			margin: 15px;
			border: 2px solid #ffb01f;
			padding: 5px;
			background: #fff7e1;
		}
		#invoice_info{
			margin-bottom: 20px;
		}
		a {
			color: #a5a5a5;
			text-decoration: none;
		}
		a:hover, a:focus {
			color: #ffb01f;
			background: #eee;
			text-decoration: underline;
		}
	</style>
@endsection

@section('customPluginJs')
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
	<script src="https://sandbox.finpay.co.id/bob/public/js/jquery.dataTables.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://sandbox.finpay.co.id/bob/public/js/jQuery-dataTables-Material.js" />
<script type="text/javascript">
	$(function() {

		var start = moment('<?php echo $start; ?>','DDMMYYYY');
		var end = moment('<?php echo $end; ?>','DDMMYYYY');

		function cb(start, end) {
			$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			
			$('body').append($('<form/>')
			  .attr({'action': './invoiceList', 'method': 'post', 'id': 'replacer'})
			  .append($('<input/>')
				.attr({'type': 'hidden', 'name': 'start', 'value': start.format('DDMMYYYY')})
			  )
			  .append($('<input/>')
				.attr({'type': 'hidden', 'name': 'end', 'value': end.format('DDMMYYYY')})
			  )
			).find('#replacer').submit();
		}

		$('#reportrange').daterangepicker({
			startDate: start,
			endDate: end,
			ranges: {
				'7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
				'30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
				'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
				'Bulan Lalu': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, cb);

		//cb(start, end);
	});

	$('#reportrange').on('apply.daterangepicker', function (ev, picker) {
		console.log('start: '+picker.startDate.format('YYYY-MM-DD'));
		console.log('end  : '+picker.endDate.format('YYYY-MM-DD'));
		days = (moment(picker.endDate)-moment(picker.startDate))/ 1000/60/60/24;
		console.log('Range : '+Math.round(days)+' days' );
	});
	
	$(document).ready(function() {
		$('#invoice').DataTable();
	} );
</script>
@stop

@section('content')
<h4 class="text-weight-600" style="color:#666666;;margin: 20px 0px 20px 0px;">Report > Invoice Draf</h4>
<div class="border-dark form_in_dashboard" style="min-height: 530px;">
	<h4 class="header_form_in_dashboard">Invoice Draf</h4>
	<hr class="margin-bottom-xsmall hr_form_in_dashboard">
	<div class="row">
		<div class="col-md-12" style="padding-left: 4%;padding-right: 4%;">
			<div class="row">
				<div class="col-md-4 col-md-offset-8">
					<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
						<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
						<span id="tanggal"></span><?php 
						$st = DateTime::createFromFormat('dmY', $start);
						$ed = DateTime::createFromFormat('dmY', $end);
						echo $st->format('F d, Y').' - '.$ed->format('F d, Y');?> <b class="caret"></b>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="">
					<h4 class="category" style="color: #AEAEAE">Tagihan yang dibuat hingga {{date("d F Y")}}</h4>
					<table class="table table-hover table-nonhanson" id="invoice">
						<thead class="text-warning">
							<tr style="background: #ffb01f;">
								<th>Invoice Number</th>
								<th>Nama Penerima</th>
								<th>e-Mail Penerima</th>
								<th>Due Date</th>
								<th>Invoice Status</th>
								<!--<th>Tipe Pembayaran</th>!-->
								<th>Payment Status</th>
								<th class="text-right">Amount</th>
								<th>Action</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach($drafInvoice as $list){?>
							<tr>
								<td>{{$list->invoice_number}}</td>
								<td>{{$list->recipient_name}}</td>
								<td>{{$list->recipient_email}}</td>
								<td>{{$list->due_date}}</td>
								<td>
								<?php
								  if($list->invoice_status=="")
									 $status='unpaid';
								  else if(strtolower($list->invoice_status)=="5-invoice-unpaid")
									  $status='unpaid';
								  else
									 $status=$list->invoice_status;
								 
								  if(strtolower($status)=='unpaid'||strtolower($list->invoice_status)=="5-invoice-unpaid")
									$button="btn-primary";
								  else if(strtolower($status)=='paid')
									$button="btn-success";
								  else
									$button="btn-danger";
								?>
									<!-- Single button -->
									<div class="btn-group">
										<button type="button" class="btn <?php echo $button; ?>" aria-haspopup="true" aria-expanded="false">
										{{ $status }} </span>
										</button>
									</div>
								</td>
								<td>
								<?php
								  if($list->payment_status=="")
									 $status='issued';
								  else if(strtolower($list->payment_status)=="5-invoice-issued")
									  $status='issued';
								  else
									 $status=$list->payment_status;
								 
								  if(strtolower($status)=='issued'||strtolower($list->payment_status)=="5-invoice-issued")
									$button="btn-primary";
								  else if(strtolower($status)=='paid')
									$button="btn-success";
								  else
									$button="btn-danger";
								?>
									<!-- Single button -->
									<div class="btn-group">
										<button type="button" class="btn <?php echo $button; ?>" aria-haspopup="true" aria-expanded="false">
										@if( $status == 'issued')
											{{ 'UNPAID' }}
										@endif </span>
										</button>
									</div>
								</td>
								<td class="text-right">Rp. {{number_format($list->total - ($list->total * $list->discount/100) ,0, ',', '.')}}</td>
								<td>
									<a class="btn btn-success" aria-haspopup="true" aria-expanded="false" id="preview-invoice-classic-form-btn" href="{{ url('/previewDraft',$list->invoice_number)}}" target="_blank">
								Preview
									</a>
								</td>
								<td>
									<?php 
										if(strtolower($list->invoice_status)=='4-invoice-canceled'){
											$but="disabled";
											$class="btn-default";
										}else if(strtolower($list->invoice_status)=='disabled'){
											$but="disabled";
											$class="btn-default";
										}else{
											
											$but="";
											$class ="btn-info";
										}
									?>
									
									<a class="btn {{$class}}" aria-haspopup="true" aria-expanded="false" href="#" id="sendDraftInvoice" <?php echo $but ?>>
									Send
									</a>
								</td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection