<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Home - One Page | iMax</title>

    <link rel="icon" href="{{ URL::asset('images/favicon.png') }}" type="image/png">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <!-- Begin: MAIN CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/plugins-min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/menu-overright.css') }}">
    <link href="{{ URL::asset('css/colors/aqua-yellow.css') }}" type="text/css" media="all" rel="stylesheet" />
    <!-- End: MAIN CSS -->

    <!-- Begin: REQUIRED FOR THIS PAGE ONLY -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('plugins/revolution/css/layers.css') }}"/> 
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('plugins/revolution/css/settings.css') }}"/> 
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('plugins/revolution/css/navigation.css') }}"/> 
    <link rel="stylesheet" type="text/css" media="screen" href="{{ URL::asset('plugins/flexslider/css/flexslider.css') }}" />	<!-- FlexSlider -->
    <!-- End: REQUIRED FOR THIS PAGE ONLY -->

    <!-- Begin: HTML5SHIV FOR IE8 -->
    <!-- HTML5 shim and Respond.js') }} for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="{{ URL::asset('js/internetexplorer/html5shiv.min.js') }}"></script>
      <script src="{{ URL::asset('js/internetexplorer/respond.min.js') }}"></script>
    <![endif]-->
    <!-- end: HTML5SHIV FOR IE8 -->
</head>
<body>
    <!-- Begin: HEADER SEARCH -->
    <div class="modal fade header-search" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="search">
                <form method="get" action="search.html">
                    <div class="form-group full-width">
                        <div class="input-group">
                            <input type="text" class="form-control input-lg" placeholder="Search Here..." value="" name="q">
                            <div class="input-group-btn "><button type="submit" class="btn btn-base btn-lg"><i class="fa fa-search"></i></button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End: HEADER SEARCH -->

    <!--
    #################################
        - Begin: HEADER -
    #################################
    -->
    <header class="main-header sticky-header">
        <nav class="navbar navbar-default single-page-nav border-top-light border-bottom-light">
            <div class="container position-relative">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <!-- Begin: RESPONSIVE MENU TOGGLER -->
                    <button type="button" class="navbar-toggle" data-toggle="modal" data-target=".header-search">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-search"></i>
                    </button>
                    <!-- End: RESPONSIVE MENU TOGGLER -->
                    <!-- Begin: RESPONSIVE MENU TOGGLER -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-navbar-collapse-2">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-shopping-bag"></i>
                    </button>
                    <!-- End: RESPONSIVE MENU TOGGLER -->
                    <!-- Begin: RESPONSIVE MENU TOGGLER -->
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars"></i>
                    </button>
                    <!-- End: RESPONSIVE MENU TOGGLER -->
                    <!-- Begin: LOGO -->
                    <a class="navbar-brand logo" href="{{ url('/homepage') }}"><img src="{{ URL::asset('images/icon/finbox.png') }}" height="40"></a>
                    <!-- End: LOGO -->
                </div>
                <div class="collapse navbar-collapse" id="nav-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right margin-right-0">
                        <li class="dropdown mega-menu">
                            <a href="#services" class="dropdown-toggle text-weight-600" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Our Services <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <!-- Start: MEGA MENU CONTENT -->
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-3" style="border-right: 2px solid #f2f2f2;">
												<h3 style="color:#ff4500;">Finbox Services</h3>
												<p>Berbagai informasi yang dibutuhkan untuk terhubung dengan layanan finbox.</p>
                                            </div>
                                            <div class="col-md-3">
												<h3 ><img src="{{ URL::asset('images/icon/finbox.png') }}" height="40"></h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="#"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                            <div class="col-md-3">
												<h3><img src="{{ URL::asset('images/icon/finboxbiller.png') }}" height="40"></h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="{{ url('/dashboard') }}"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
											<div class="col-md-3">
												<h3><img src="{{ URL::asset('images/icon/finboxca.png') }}" height="40"></h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="{{ url('/dashboard') }}"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End: MEGA MENU CONTENT -->
                                </li>
                            </ul>
                        </li>
						<li class="dropdown mega-menu">
                            <a href="#services" class="dropdown-toggle text-weight-600" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Get Started <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <!-- Start: MEGA MENU CONTENT -->
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-3" style="border-right: 2px solid #f2f2f2;">
												<h3 style="color:#ff4500;">Finbox Services</h3>
												<p>Berbagai informasi yang dibutuhkan untuk terhubung dengan layanan finbox.</p>
                                            </div>
                                            <div class="col-md-3">
												<h3>Technical Document</h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="#"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                            <div class="col-md-3">
												<h3>How To Self Testing</h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="{{ url('/dashboard') }}"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
											<div class="col-md-3">
												<h3>Request Testing</h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="{{ url('/dashboard') }}"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End: MEGA MENU CONTENT -->
                                </li>
                            </ul>
                        </li>
						<li class="dropdown mega-menu">
                            <a href="#services" class="dropdown-toggle text-weight-600" data-toggle="dropdown" data-hover="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Support <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <!-- Start: MEGA MENU CONTENT -->
                                    <div class="mega-menu-content">
                                        <div class="row">
                                            <div class="col-md-3" style="border-right: 2px solid #f2f2f2;">
												<h3 style="color:#ff4500;">Finbox Services</h3>
												<p>Berbagai informasi yang dibutuhkan untuk terhubung dengan layanan finbox.</p>
                                            </div>
                                            <div class="col-md-3">
												<h3>Contact Us</h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="#"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                            <div class="col-md-3">
												<h3>FAQ</h3>
												<p style="text-align: justify;">Sistem pembayaran online yang terhubung dengan berbagai metode pembayaran di Indonesia.</p>
												<a href="{{ url('/dashboard') }}"  class="pull-right">Baca Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- End: MEGA MENU CONTENT -->
                                </li>
                            </ul>
                        </li>
                        <li><a href="{{ url('auth/login') }}" class="text-weight-600" style="color: #F65C26;" text-weight-600">Log In</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
        </nav>
    </header>
    <!-- End: HEADER -
    ################################################################## -->

	<div  class="switcher_boxed boxed">
		<!--
		#################################
			- Begin: Services -
		#################################
		-->
		<div class="container light-bg">
			<div class="row">
				<div class="col-sm-12">
					<div class="text-center">
						<a class="logo" href="{{ url('/homepage') }}"><img src="{{ URL::asset('images/icon/finbox.png') }}" width="140"></a>
						<h4 class="color_text_confirmation_p">Silahkan melakukan verifikasi akun dengan melalui<br> tautan yang sudah kami kirimkan ke email Anda.</h4>
					</div>
				</div>
				<div class="col-sm-4"></div>
				<div class="col-sm-4">
					<img src="{{ URL::asset('images/icon/cekmail.png') }}" height="150" data-bgposition="center center" class="img-responsive">
				</div>
				<div class="col-sm-4"></div>
				<div class="col-sm-12"><br></div>
				<div class="col-sm-4"></div>
				<div class="col-sm-3" style="margin-left: 5%;">
					<a href="{{ url('auth/login') }}" class="btn btn-base btn-lg daftar text-weight-600 box-shadow-active" style="width: 98%;">Mulai Sekarang!</a>
				</div>
			</div>
		</div>
		<!-- End: WORK -
		################################################################## -->
	</div>
    <!--
    #################################
        - Begin: FOOTER -
    #################################
    -->
    <footer id="footer" class="light-bg">
        <div class="top-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
						<h1 class="text-weight-600" style="color:black">LOGO</h1>
					</div>
					<div class="col-md-4 col-sm-12">
                        <!-- Begin: WIDGET -->
                        <div class="widget">
                            <div class="list-link">
                                <ul class="list-ul" style="margin-left: 0px;">
                                    <li><h4 class="widget-title text-weight-700 color_text_footer"><a href="#">About Us</a></h4></li>
                                    <li><h4 class="widget-title text-weight-700 color_text_footer"><a href="#">Blog</a></h4></li>
                                    <li><h4 class="widget-title text-weight-700 color_text_footer"><a href="#">Clients</a></h4></li>
                                    <li><h4 class="widget-title text-weight-700 color_text_footer"><a href="#">Developer</a></h4></li>
                                    <li><h4 class="widget-title text-weight-700 color_text_footer"><a href="#">Security</a></h4></li>
                                    <li><h4 class="widget-title text-weight-700 color_text_footer"><a href="#">Contact</a></h4></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End: WIDGET -->
                    </div>
					<div class="col-md-4 col-sm-12">
                        <!-- Begin: WIDGET -->
                        <div class="widget">
                            <div class="list-link">
                                <ul class="list-ul"  style="margin-left: 0px;">
                                    <li><h4 class="widget-title text-weight-700 color_text_footer"><a href="#">Frequently Asked Questions</a></h4></li>
                                    <li><h4 class="widget-title text-weight-700 color_text_footer"><a href="#">Technical Documentation</a></h4></li>
                                    <li><h4 class="widget-title text-weight-700 color_text_footer"><a href="#">Pricing</a></h4></li>
                                    <li><h4 class="widget-title text-weight-700 color_text_footer"><a href="#">Payment Channel</a></h4></li>
                                    <li><h4 class="widget-title text-weight-700 color_text_footer"><a href="#">Term and Condition</a></h4></li>
                                </ul>
                            </div>
                        </div>
                        <!-- End: WIDGET -->
                    </div>
                    <div class="col-md-4 col-sm-12" id="contact_us">
                        <!-- Begin: WIDGET -->
						<div class="col-md-3">
							<img src="{{ URL::asset('images/icon/cust.png') }}" height="75" data-bgposition="center center">
						</div>
						<div class="col-md-9  col-sm-12" style="padding-left: 25px;">
							<h5>Call our customer support</h5>
							<h5>021-1500770</h5>
							<h6>helpdesk@finnet-indonesia.com</h6>
						</div>
                        <!-- End: WIDGET -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End: FOOTER -
    ################################################################## -->

    <!-- Begin: BACK TO TOP -->
    <a id="back-to-top" href="#" class="back-to-top btn btn-base">
        <i class="fa fa-chevron-up"></i>
    </a>
    <!-- End: BACK TO TOP -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
    
    <!-- Begin: REQUIRED FOR THIS PAGE ONLY -->
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.actions.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.kenburn.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.layeranimation.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.migration.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.navigation.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.parallax.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.slideanims.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('plugins/revolution/extensions/revolution.extension.video.min.js') }}"></script>
    <!-- End: REQUIRED FOR THIS PAGE ONLY -->

</body>
</html>