<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Page - Lost Password | iMax</title>

    <link rel="icon" href="{{ URL::asset('images/favicon.png') }}" type="image/png">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    <!-- Begin: MAIN CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/plugins-min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/menu-overright.css') }}">
    <link href="{{ URL::asset('css/colors/aqua-yellow.css') }}" type="text/css" media="all" rel="stylesheet" />
    <!-- End: MAIN CSS -->

    <!-- Begin: HTML5SHIV FOR IE8 -->
    <!-- HTML5 shim and Respond.js') }} for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="{{ URL::asset('js/internetexplorer/html5shiv.min.js') }}"></script>
      <script src="{{ URL::asset('js/internetexplorer/respond.min.js') }}"></script>
    <![endif]-->
    <!-- end: HTML5SHIV FOR IE8 -->
</head>
<body class="switcher_boxed boxed">

    <!-- Begin: HEADER SEARCH -->
    <div class="modal fade header-search" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="search">
                <form method="get" action="search.html">
                    <div class="form-group full-width">
                        <div class="input-group">
                            <input type="text" class="form-control input-lg" placeholder="Search Here..." value="" name="q">
                            <div class="input-group-btn "><button type="submit" class="btn btn-base btn-lg"><i class="fa fa-search"></i></button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End: HEADER SEARCH -->

    <!--
    #################################
        - Begin: LOST PASSWORD -
    #################################
    -->
	<div class="container">
		<div class="row masnory-container">
			<div class="col-md-12">
				<p class="text-center header">
					<a class="logo" href="{{ url('/home') }}"><img src="{{ URL::asset('images/icon/finbox.png') }}" width="160"></a>
				</p>
				<hr class="margin-bottom-xsmall" style="border-top: 3px solid #eee;">
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<form method="POST" name="assets/contact_form" action="{{ url('/password/forgot') }}" class="border-dark padding-small margin-top-small margin-bottom-small">
					<h3 class="margin-auto text-center color_text_forgot_h">RESET PASSWORD</h3>
					<p class="margin-bottom-0 text-center color_text_forgot">Lupa kata sandi? Masukkan email lohin Anda dibawah ini. Kami akan mengirimkan pesan email beserta tautan untuk reset kata sandi Anda.</p>
					<div class="card-content spacer-50">
						{!! csrf_field() !!}
					
						@if (count($errors) > 0)
							
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger" style="margin-left:20px;">
										{{ $error }}
									</div>
								@endforeach
						
						@endif
						
						@if (session('status'))
								<div class="alert alert-success" style="margin-left:20px;">
										{{ session('status') }}
									</div>
						@endif
						
					</div>
					<div class="row margin-top-xsmall">
						<div class="col-sm-3"></div>
						<div class="col-sm-6" style="text-align: center;">
							<div class="form-group">
								<label class="control-label color_text_forgot_h" style="float:left;">Email</label><br>
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="email" placeholder="alexandria@finbox.com" class="form-control input-lg radius_input" name="email" value="">
							</div>
						</div>
						<div class="col-md-12">
							<p class="margin-bottom-0 text-center color_text_forgot">Pastikan Anda sudah memasukkan email Anda yang sudah terdaftar di Finbox.</p>
						</div>
					</div>
					<div class="row margin-top-xsmall">
						<div class="col-sm-3"></div>
						<div class="col-sm-6" style="text-align: center;">
							<button type="submit" value="Submit" name='submit' class="btn btn-base btn-lg daftar box-shadow-active">Kirim Link Reset Password</button>
							<p class="margin-bottom-small text-center color_text_forgot">kembali ke <a href="{{ url('/auth/login') }}">Log In</a></p>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
    <!-- End: LOST PASSWORD -
    ################################################################## -->
	
	<!--
    #################################
        - Begin: FOOTER -
    #################################
    -->
    <footer id="footer" class="light-bg" style="border-top: 1px solid #CCCCCC;">
        <div class="top-footer">
            <div class="container">
                <div class="row">
					<div class="col-md-12">
						<a href="{{ url('/homepage') }}"><img src="{{ URL::asset('images/icon/finbox.png') }}" width="100"></a>
					</div>
					<div class="col-sm-3">
						<div class="widget">
                            <div class="list-link">
                                <ul class="list-ul">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Blog</a></li>
                                    <li><a href="#">Clients</a></li>
                                    <li><a href="#">Developers</a></li>
                                    <li><a href="#">Security</a></li>
                                    <li><a href="#">Contact</a></li>
                                </ul>
                            </div>
                        </div>
					</div>
					<div class="col-sm-1"></div>
					<div class="col-sm-3">
						<div class="widget">
                            <div class="list-link">
                                <ul class="list-ul">
                                    <li><a href="#">Frequently Asked Question</a></li>
                                    <li><a href="#">Technical Documentation</a></li>
                                    <li><a href="#">Pricing</a></li>
                                    <li><a href="#">Payment Channel</a></li>
                                    <li><a href="#">Term Condition</a></li>
                                </ul>
                            </div>
                        </div>
					</div>
					<div class="col-sm-2">
						<img src="{{ URL::asset('images/icon/cust.png') }}" width="80" data-bgposition="center center" class="pull-right">
					</div>
					<div class="col-sm-3">
						<h5>Call our customer support</h5>
						<h5>021-1500770</h5>
						<h6>helpdesk@finnet-indonesia.com</h6>
					</div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End: FOOTER -
    ################################################################## -->

    <!-- Begin: BACK TO TOP -->
    <a id="back-to-top" href="#" class="back-to-top btn btn-base">
        <i class="fa fa-chevron-up"></i>
    </a>
    <!-- End: BACK TO TOP -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>

</body>
</html>