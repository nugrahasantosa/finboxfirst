<!DOCTYPE html>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5afa548b227d3d7edc255172/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Page - Register | Finbox</title>

    <link rel="icon" href="{{ URL::asset('images/favicon.png') }}" type="image/png">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">


    <!-- Begin: MAIN CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/plugins-min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/menu-overright.css') }}">
    <link href="{{ URL::asset('css/colors/aqua-yellow.css') }}" type="text/css" media="all" rel="stylesheet" />
    <!-- End: MAIN CSS -->

    <!-- Begin: HTML5SHIV FOR IE8 -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="assets/js/internetexplorer/html5shiv.min.js"></script>
      <script src="assets/js/internetexplorer/respond.min.js"></script>
    <![endif]-->
    <!-- end: HTML5SHIV FOR IE8 -->
</head>
<body class="switcher_boxed boxed">

    <!-- Begin: HEADER SEARCH -->
    <div class="modal fade header-search" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-md" role="document">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="search">
                <form method="get" action="search.html">
                    <div class="form-group full-width">
                        <div class="input-group">
                            <input type="text" class="form-control input-lg" placeholder="Search Here..." value="" name="q">
                            <div class="input-group-btn "><button type="submit" class="btn btn-base btn-lg"><i class="fa fa-search"></i></button></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End: HEADER SEARCH -->

    <!--
    #################################
        - Begin: REGISTER -
    #################################
    -->
    <div class="parallax">
		<div class="parallax_child img-section-2"></div>
			<div class="vertical-content-setting">
				<div class="container">
					<div class="row masnory-container">
						<div class="col-md-12">
							<p class="text-center header">
								<a class="logo" href="{{ url('/homepage') }}"><img src="{{ URL::asset('images/icon/finbox.png') }}" width="160"></a>
							</p><br><br>
						</div>
						<div class="col-md-1"></div>
						<div class="col-md-4">
							<div class="card-content spacer-50">
								@if (session('status'))
									<div class="alert alert-success alert-dismissable" style="margin-left:20px;">
									{{ session('status') }}
									</div>
								@endif
								@if (session('warning'))
									<div class="alert alert-warning alert-dismissable" style="margin-left:20px;">
									{{ session('warning') }}
									</div>
								@endif
								@if (session('danger'))
									<div class="alert alert-danger alert-dismissable" style="margin-left:20px;">
									{{ session('danger') }}
									</div>
								@endif
							<!-- END ADD RAEDI -->
								@if (count($errors) > 0)
										<div class="alert alert-danger" style="margin-left:20px;">
										@foreach ($errors->all() as $error)
											
												{{ $error }}
											
										@endforeach
										</div>
								@endif
							</div>
							<form method="POST" action="{{ url('/auth/postRegister')}}" name="assets/contact_form">
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="control-label">Nama</label>
											<div class="inputWithIcon">
												<input type="text" id="inputName" placeholder="John Alex Andria" class="form-control input-lg" name="name" value="" style="border-radius: 0;">
												<i><img src="{{ URL::asset('images/icon/peopleicon.png') }}" height="25" data-bgposition="center center"></i>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label class="control-label">Email</label>
											<div class="inputWithIcon">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="email" id="inputEmail" placeholder="alexandria@finbox.com" class="form-control input-lg" name="email" value="" style="border-radius: 0;">
												<i><img src="{{ URL::asset('images/icon/mailicon.png') }}" height="25" data-bgposition="center center"></i>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="inputPassword" class="control-label">Password</label>
											<div class="inputWithIcon">
												<input type="password" id="inputPassword" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" class="form-control input-lg" name="password" value="" style="border-radius: 0;">
												<i><img src="{{ URL::asset('images/icon/gembokicon.png') }}" height="25" data-bgposition="center center"></i>
												<label class="control-label">Minimal 6 karakter</label>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="inputPassword" class="control-label">Password Confirmation</label>
											<div class="inputWithIcon">
												<input type="password" id="inputPasswordConfirmation" placeholder="&#9679;&#9679;&#9679;&#9679;&#9679;&#9679;" class="form-control input-lg" name="password_confirmation" value="" style="border-radius: 0;">
												<i><img src="{{ URL::asset('images/icon/gembokicon.png') }}" height="25" data-bgposition="center center"></i>
												<label class="control-label">Minimal 6 karakter</label>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12 col-sm-12">
										<button type="submit" value="Submit" name='submit' class="btn btn-base btn-lg daftar text-weight-600 text-uppercase box-shadow-active" style="width: 98%;">DAFTAR</button>
										<h5 class="text-center">Sudah Memiliki Akun Finbox? Masuk <a href="{{ url('/auth/login') }}">di sini!</a></h5>
									</div>
								</div>
							</form>
						</div>
						<div id="or" class="col-md-2">
							<br><div class="line3">
							</div>
							<p class="text-center mt-20 mb-20 small-gray">
								atau
							</p>
							<div class="line4">
							</div>
						</div>
						<div class="col-md-4 text-center hr_vertical">
							<div class="row" style="margin-top: 16.5%;display: grid;">
								<div class="col-md-12">
									<div class="btn btn-lg text-weight-600 text-uppercase icon_facebook margin_facebook" style="width: 10%;border-right:0;"><img src="{{ URL::asset('images/icon/ficon.png') }}" height="15" data-bgposition="center center"></div>
									<button type="submit" value="Submit" name='submit' class="btn btn-lg facebook margin_facebook width_button992" style="border-left: 0;">Masuk dengan facebook</button>
								</div>
								<div class="col-md-12">
									<div class="btn btn-lg text-weight-600 text-uppercase icon_google padding_facebook" style="width: 10%;border-right:0;"><img src="{{ URL::asset('images/icon/gicon.png') }}" height="15" data-bgposition="center center"></div>
										<button type="submit" value="Submit" name='submit' class="btn btn-lg google padding_facebook width_button992" style="border-left: 0;">Masuk dengan google</button>
								</div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <!-- End: REGISTER -
    ################################################################## -->

    <!-- Begin: BACK TO TOP -->
    <a id="back-to-top" href="#" class="back-to-top btn btn-base">
        <i class="fa fa-chevron-up"></i>
    </a>
    <!-- End: BACK TO TOP -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	
    <script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/plugins.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
	


</body>
</html>