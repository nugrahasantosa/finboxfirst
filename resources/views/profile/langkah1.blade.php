<?php 	
if($active=="active"){
	$finish="";
	$start="active";
  }else{ 
	 $finish="active";
	 $start="";
  }
	
?>
@extends('layout')

@section('pageTitle')
	Profile - Finbox
@stop

@section('sidebarActiveProfile')
    class="active"
@stop

@section('customPluginCss')
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<style>
	.btn:hover, .btn:focus {
		color: #fff;
		text-decoration: none;
	}
</style>
@stop

@section('customPluginJs')
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="{{ URL::asset('js/camera/jpeg_camera_with_dependencies.min.js') }}"></script>
	<script>
	$(document).ready(function() {
    
		var navListItems = $('ul.setup-panel li a'),
			allWells = $('.setup-content');

		allWells.hide();

		navListItems.click(function(e)
		{
			e.preventDefault();
			var $target = $($(this).attr('href')),
				$item = $(this).closest('li');
			
			if (!$item.hasClass('disabled')) {
				navListItems.closest('li').removeClass('active');
				$item.addClass('active');
				allWells.hide();
				$target.show();
			}
		});
		
		$('ul.setup-panel li.active a').trigger('click');
		
		// DEMO ONLY //
		$('#activate-step-2').on('click', function(e) {
			$('ul.setup-panel li:eq(1)').removeClass('disabled');
			$('ul.setup-panel li a[href="#step-2"]').trigger('click');
		});
		
		$('#activate-step-1').on('click', function(e) {
			$('ul.setup-panel li:eq(1)').removeClass('disabled');
			$('ul.setup-panel li a[href="#step-1"]').trigger('click');
		})
	});
	$(document).ready(function () {
		var countryStateInfo = {
			"DKI Jakarta": {
				"Jakarta Selatan": [], "Jakarta Timur": [], "Jakarta Utara": [], "Jakarta Barat": []
			},
			"Jawa Barat": {
				"Bandung": [], "Purwakarta": [], "Subang": []
			},
			"Jawa Timur": {
				"Malang": [], "Surabaya" : [], "Pasuruan": [], "Lamongan": [], "Pacitan": []
			}
		}
		
		window.onload = function () {
	
	//Get html elements
			var countySel = document.getElementById("countySel");
			var stateSel = document.getElementById("stateSel");	
			var citySel = document.getElementById("citySel");
			var zipSel = document.getElementById("zipSel");
			
			//Load countries
			for (var country in countryStateInfo) {
				countySel.options[countySel.options.length] = new Option(country, country);
			}
			
			//County Changed
			countySel.onchange = function () {
				 
				 stateSel.length = 1; // remove all options bar first
				 citySel.length = 1; // remove all options bar first
				 zipSel.length = 1; // remove all options bar first
				 
				 if (this.selectedIndex < 1)
					 return; // done
				 
				 for (var state in countryStateInfo[this.value]) {
					 stateSel.options[stateSel.options.length] = new Option(state, state);
				 }
			}
			
			//State Changed
			stateSel.onchange = function () {		 
				 
				 citySel.length = 1; // remove all options bar first
				 zipSel.length = 1; // remove all options bar first
				 
				 if (this.selectedIndex < 1)
					 return; // done
				 
				 for (var city in countryStateInfo[countySel.value][this.value]) {
					 citySel.options[citySel.options.length] = new Option(city, city);
				 }
			}
			
			//City Changed
			citySel.onchange = function () {
				zipSel.length = 1; // remove all options bar first
				
				if (this.selectedIndex < 1)
					return; // done
				
				var zips = countryStateInfo[countySel.value][stateSel.value][this.value];
				for (var i = 0; i < zips.length; i++) {
					zipSel.options[zipSel.options.length] = new Option(zips[i], zips[i]);
				}
			}	
		}
		//Initialize tooltips
		$('.nav-tabs > li a[title]').tooltip();
		
		//Wizard
		$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

			var $target = $(e.target);
		
			if ($target.parent().hasClass('disabled')) {
				return false;
			}
		});

		$(".next-step").click(function (e) {

			var $active = $('.wizard .nav-tabs li.active');
			$active.next().removeClass('disabled');
			nextTab($active);

		});
		$(".prev-step").click(function (e) {

			var $active = $('.wizard .nav-tabs li.active');
			prevTab($active);

		});
	});

	function nextTab(elem) {
		$(elem).next().find('a[data-toggle="tab"]').click();
	}
	function prevTab(elem) {
		$(elem).prev().find('a[data-toggle="tab"]').click();
	}
    </script>
@endsection
<!--start content!-->
@section('content')
<h4 style="color:#666666;">Aktifasi Biller</h4>
<div class="wizard">
	<div class="wizard-inner" style="padding-left: 20%;">
		<div class="connecting-line"></div>
		<ul class="nav nav-tabs setup-panel" style="border-bottom: 0px solid #ddd;">
			<li class="<?php echo $active=="active" ? 'active' : 'disabled'?>" style="float: left;"><a href="#step-1">
				<span class="round-tab">
					<i class="glyphicon glyphicon-folder-open" style="top: 15px;"></i>
				</span>
			</a></li>
			<li class="<?php echo $active=="active" ? 'disabled' : 'disabled'?>" style="float: left;"><a href="#step-2">
				<span class="round-tab">
					<i class="glyphicon glyphicon-pencil" style="top: 15px;"></i>
				</span>
			</a></li>
			<li class="<?php echo $active=="active" ? 'disabled' : 'active'?>" style="float: left;"><a href="#step-3">
				<span class="round-tab">
					<i class="glyphicon glyphicon-ok" style="top: 15px;"></i>
				</span>
			</a></li>
		</ul>
	</div>
	<div class="col-md-12">
			@if (Session::has('msg'))
				<div class="alert alert-info">{{    Session::get('msg') }}
				
				 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				</div>
			@elseif (Session::has('incorrect'))
				<div class="alert alert-danger">{{  Session::get('incorrect') }}
				
				 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				</div>
			@endif
		</div>
	{!! Form::open(['url' => 'register', 'files' => true,'id'=>'registerForm']) !!}
		<div class="row setup-content {{$start}}" id="step-1" style="padding: 0px 20px 50px 20px;">
			<div class="border-dark form_in_dashboard">
				<h4 class="header_form_in_dashboard">Informasi bisnis dan usaha</h4>
				<hr class="margin-bottom-xsmall hr_form_in_dashboard">
				<div class="row" style="padding: 2%;">
					<div class="col-md-12 label_form_in_dashboard">
						<div class="form-group">
							<label for="nama_perusahaan">Nama Badan Usaha  <sup style="color: red">*</sup></label>
							<input type="text" required="" class="form-control input-form radius_form_in_dashboard" name="company_name" id="company_name" placeholder="Nama Badan Usaha">
						</div>
						<div class="form-group">
						<label for="nama_pemilik_rekening">Nama Pemilik Usaha  <sup style="color: red">*</sup></label>
							<input type="text" required="" class="form-control input-form radius_form_in_dashboard" name="acc_name" id="acc_name" placeholder="Nama Pemilik Usaha" value="">
						</div>
						<div class="form-group">
							<label for="alamat_perusahaan">Alamat Badan Usaha / Alamat Perseorangan <sup style="color: red">*</sup></label>
							<textarea class="form-control input-form radius_form_in_dashboard" id="company_addrress" name="company_addrress" required="" placeholder="Alamat Badan Usaha"></textarea>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="alamat_perusahaan" >Provinsi <sup style="color: red">*</sup></label>
									<select id="countySel" name="company_province" class="form-control input-form radius_form_in_dashboard" size="1">
										<option value="" selected="selected">- Pilih Provinsi -</option>
									</select>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="alamat_perusahaan">Kota <sup style="color: red">*</sup></label>
									<select name="company_city" id="stateSel" class="form-control input-form radius_form_in_dashboard" size="1">
										<option value="" selected="selected">- Pilih Kota -</option>
									</select>
								</div>
							</div>
						</div>
						<div class="form-group" style="display:none">
							<select id="citySel" size="1" style="display:none">
								<option value="" selected="selected">-- Select City--</option>
							</select>
							<br>
							<br>
							<select id="zipSel" size="1" style="display:none">
								<option value="" selected="selected">-- Select Zip--</option>
							</select>
						</div>
						<div class="form-group">
							<label for="no_tlp_perusahaan">Nomer Telepon Badan Usaha</label>
							<input type="number" class="form-control input-form radius_form_in_dashboard" id="company_phone" name="company_phone" placeholder="Nomer Telepon Badan usaha">
						</div>
						<div class="form-group">
							<label for="npwp_perusahaan">NPWP Perusahaan <sup style="color: red">*</sup></label>
							<input type="text" class="form-control input-form radius_form_in_dashboard" id="company_npwp" name="company_npwp" placeholder="NPWP Perusahaan" required="">
							<div class="help-block with-errors"></div>
							<div class="help-block">Contoh: 99.999.999.9-999.999</div>
						</div>
						<div class="form-group">
							<div class="col-md-3 pull-right" style="padding-right:0px">
									<button id="activate-step-2" class="btn btn-base btn-lg btn-block">Lanjut <i class="fa fa-arrow-circle-right"></i></button><br>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<div class="row setup-content" id="step-2" style="padding: 0px 20px 50px 20px;">
			<div class="border-dark form_in_dashboard">
				<h4 class="header_form_in_dashboard">Informasi Rekening</h4>
				<hr class="margin-bottom-xsmall hr_form_in_dashboard">
				<div class="row" style="padding: 2%;">
					<div class="col-md-12 label_form_in_dashboard">
						<div class="form-group">
							<label for="bank_name" >Nama Bank  <sup style="color: red">*</sup></label>
								<select id="bankName" name="bank_name" id="bank_name" class="form-control input-form radius_form_in_dashboard" required="">
									<option>Nama Bank</option>
									<option value="BANK BCA"> BANK BCA </option>
									<option value="BANK BNI"> BANK BNI </option>
									<option value="BANK Mandiri"> BANK Mandiri </option>
									<option value="BANK BRI"> BANK BRI </option>
									<option value="BANK NIAGA"> BANK NIAGA </option>
									<option value="BANK BTN"> BANK BTN </option>
								</select>
						</div>
						<div class="form-group">
							<label for="nomor_rekening">Nomor Rekening  <sup style="color: red">*</sup></label>
								<input type="number" required="" class="form-control input-form radius_form_in_dashboard" name="bank_acc_no" id="bank_acc_no" placeholder="Nomor Rekening" value="">
							
						</div>
						<div class="form-group">
							<label for="nama_pemilik_rekening">Nama Pemilik Rekening  <sup style="color: red">*</sup></label>
								<input type="text" required="" class="form-control input-form radius_form_in_dashboard" name="bank_acc_name" id="bank_acc_name" placeholder="Nama Pemilik Rekenig" value="">
						</div>
						<div class="form-group">
							<label for="cabang">Cabang  <sup style="color: red">*</sup></label>
								<input type="text" required="" class="form-control input-form radius_form_in_dashboard" name="bank_cabang" id="bank_cabang" placeholder="Kantor Cabang Bank" value="">
						</div>
					</div>
				</div>
			</div>
			<div class="border-dark form_in_dashboard">
				<h4 class="header_form_in_dashboard">Upload document</h4>
				<hr class="margin-bottom-xsmall hr_form_in_dashboard">
				<div class="row" style="padding: 2%;">
					<div class="col-md-12">
						<div class="form-group has-feedback">
							<label for="ktp" class="col-sm-3 label_form_in_dashboard">KTP  <sup style="color: red">*</sup><br> Scan KTP dalam format JPG(*) max ukuran file 1024KB</label>
							<div class="col-sm-3">
								<div class="col-xs-12">
									<img id="pic_ktp_img" class="img-responsive" style="max-height: 150px;height: 100px;width:81px">
								</div>
								<div class="col-xs-12">
									<span class="btn-sm btn-primary" onclick="$(this).parent().find('input[type=file]').click();">Pilih image</span>
									</span>
									<span class="btn-sm btn-primary" onclick="$(this).parent().find('input[type=file]').click();">Camera image</span>
								</div>
							</div>
							<label for="produk" class="col-sm-3 label_form_in_dashboard">NPWP Perusahaan  <sup style="color: red">*</sup><br> Scan NPWP dalam format JPG (*) max.size :1024 KB</label>
							<div class="col-sm-3">
								<div class="col-xs-12">
									<img id="comp_npwp_img" class="img-responsive" style="max-height: 150px;height: 100px;width:81px">
								</div>
								<div class="col-xs-12">
									<span class="btn-sm btn-primary" onclick="$(this).parent().find('input[type=file]').click();">Pilih image</span>
										<input onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());
												document.getElementById('comp_npwp_img').src = window.URL.createObjectURL(this.files[0]);" style="display: none;" accept="image/jpeg" name="comp_npwp_img" type="file">
									</span>
									<span class="btn-sm btn-primary" onclick="$(this).parent().find('input[type=file]').click();">Camera image</span>
								</div>
							</div>
						</div>
						<div class="form-group has-feedback">
							<label style="margin-top: 10px;" for="npwp" class="col-sm-3 label_form_in_dashboard">NPWP Pemilik Perusahaan <sup style="color: red">*</sup><br> Scan NPWP dalam format JPG(*) max ukuran file 1024KB</label>
							<div class="col-sm-3" style="margin-top: 10px;">
								<div class="col-xs-6">
									<img id="pic_npwp_img" class="img-responsive" style="max-height: 150px;height: 100px;width:81px">
									
								</div><div class="col-xs-6">
									<div id="pic_npwp_cam" style="max-height: 150px;height: 100px;width:81px"></div>
								</div>
								<div class="col-xs-12">
									<span class="btn-sm btn-primary" onclick="$(this).parent().find('input[type=file]').click();">Pilih image</span>
										<input onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());
												document.getElementById('pic_npwp_img').src = window.URL.createObjectURL(this.files[0]);" style="display: none;" accept="image/jpeg" name="pic_npwp_img" type="file">
									</span>
									<span class="btn-sm btn-primary" id="btn_pic_npwp">Camera image</span>
								</div>
							</div>
							<label style="margin-top: 10px;" for="website" class="col-sm-3 label_form_in_dashboard">Logo Perusahaan  <sup style="color: red">*</sup><br> Upload Logo perusahaan dalam JPG(*) max ukuran file 1024KB</label>
							<div class="col-sm-3" style="margin-top: 10px;">
								<div class="col-xs-12">
									<img id="comp_logo_img" class="img-responsive" style="max-height: 150px;height: 100px;width:81px">
								</div>
								<div class="col-xs-12">
									<span class="btn-sm btn-primary" onclick="$(this).parent().find('input[type=file]').click();">Pilih image</span>
										<input onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());
												document.getElementById('comp_logo_img').src = window.URL.createObjectURL(this.files[0]);" style="display: none;" accept="image/jpeg" name="comp_logo_img" type="file">
									</span>
									<span class="btn-sm btn-primary" onclick="$(this).parent().find('input[type=file]').click();">Camera image</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br><hr class="margin-bottom-xsmall hr_form_in_dashboard">
				<div class="row">
					<div class="col-md-8 pull-right" style="padding-right:30px;">
						<h5 class="pull-right" style="color:#F7931E;margin:0;">Baca syarat dan ketentuan finbox</h5><br>
						<label class="pull-right" style="margin:0;font-size:10px;margin-bottom: 10px;">
							<input type="checkbox" id="agree" value="1">
							Saya telah membaca dan setuju dengan syarat dan ketentuan finbox
						</label><br>
					</div>
					<div class="col-md-8 pull-right" style="padding-right:30px;">
						<div class="col-md-4 pull-right" style="padding-right:0px">
							<button type="submit" class="btn btn-base btn-lg btn-block">Submit <i class="fa fa-arrow-circle-right"></i></button><br>
						</div>
						<div class="col-md-4 pull-right" style="padding-right:0px">
							<button id="activate-step-1" class="btn btn-base btn-lg btn-block"><i class="fa fa-arrow-circle-left"></i> Kembali</button><br>
						</div>
					</div><br>
				</div>
			</div>
		</div>
		<div class="row setup-content {{$finish}}" id="step-3" style="padding: 0px 20px 50px 20px;">
			<div class="border-dark form_in_dashboard">
				<div class="row">
					<div class="col-sm-12">
						<div class="text-center">
							<h3 class="color_text_confirmation_p">Hai <?php echo empty($name) ? 'Nugra' : $name->comp_name ?>,</h3>
							<h4 class="color_text_confirmation_p">Terimaksih sudah melengkapi<br> registrasi akun bisnis Anda!</h4>
						</div>
					</div>
					<div class="col-sm-5"></div>
					<div class="col-sm-2">
						<img src="{{ URL::asset('images/icon/regsuc.png') }}" data-bgposition="center center" class="img-responsive">
					</div>
					<div class="col-sm-5"></div>
					<div class="col-sm-12">
						<div class="margin-bottxom-xsmallom-xsmall text-center">
							<br><h4 class="color_text_confirmation_p">Sekarang Anda sudah bisa menggunakan<br>layanan finbox secara keseluruhan untuk bisnis Anda!</h4>
						</div>
					</div>
					<div class="col-sm-4"></div>
					<div class="col-sm-4">
						<a href="{{ url('/akun') }}" class="btn btn-base btn-lg daftar text-weight-600 box-shadow-active radius_input" style="width: 98%;margin-bottom: 20%;border-radius: 50px;">Lihat Profil</a><br>
					</div>
				</div>
			</div>
		</div>
	{!! Form::close() !!}
</div>
<!--end content!-->
@endsection

<script>
    var options = {
      shutter_ogg_url: "jpeg_camera/shutter.ogg",
      shutter_mp3_url: "jpeg_camera/shutter.mp3",
      swf_url: "jpeg_camera/jpeg_camera.swf",
    };
    var camera = new JpegCamera("#pic_npwp_cam", options);
  
  $('#btn_pic_npwp').click(function(){
    var snapshot = camera.capture();
    snapshot.show();
    
    snapshot.upload({api_url: "action.php"}).done(function(response) {
$('#imagelist').prepend("<tr><td><img src='"+response+"' width='100px' height='100px'></td><td>"+response+"</td></tr>");
}).fail(function(response) {
  alert("Upload failed with status " + response);
});
})

function done(){
    $('#snapshots').html("uploaded");
}
</script>