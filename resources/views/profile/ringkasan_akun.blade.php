@extends('layout')

@section('pageTitle')
	Ringkasan Akun - Finbox
@stop

@section('sidebarActiveRingkasanAkun')
    class="active"
@stop

@section('customPluginJs')
	<style>
		input[type="file"] {
			width: 98%;
			border-radius: 5px;
			color: #fff;
		}
		.btn-default {
			color: #333;
			background-color: #fbfbfb;
			border-color: #ccc;
		}
	</style>
@stop

@section('customPluginJs')
	<script>
	$(document).ready(function () {
		//Initialize tooltips
		$('.nav-tabs > li a[title]').tooltip();
		
		//Wizard
		$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

			var $target = $(e.target);
		
			if ($target.parent().hasClass('disabled')) {
				return false;
			}
		});

		$(".next-step").click(function (e) {

			var $active = $('.wizard .nav-tabs li.active');
			$active.next().removeClass('disabled');
			nextTab($active);

		});
		$(".prev-step").click(function (e) {

			var $active = $('.wizard .nav-tabs li.active');
			prevTab($active);

		});
	});
	</script>
@endsection

@section('content')
<div class="col-md-12">
	@if (Session::has('msg'))
		<div class="alert alert-info">{{    Session::get('msg') }}
		
		 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		</div>
	@elseif (Session::has('incorrect'))
		<div class="alert alert-danger">{{  Session::get('incorrect') }}
		
		 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		</div>
	@endif
</div>
<h4 class="text-weight-600" style="color:#666666;;margin: 20px 0px 20px 0px;">Profil Saya > Ringkasan Akun</h4>
<div class="border-dark form_in_dashboard">
	<div class="row">
		<div class="col-md-12">
			<div class="tabs-theme dark">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs text-weight-600" role="tablist" style="font-size: 18px;">
					<li role="presentation" class="active"><a href="#biodataDiri" aria-controls="services" role="tab" data-toggle="tab" aria-expanded="true">Biodata Diri</a></li>
					<li role="presentation" class=""><a href="#informasiBisnis" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">Informasi Biller</a></li>
					<li role="presentation" class=""><a href="#rekening" aria-controls="counters" role="tab" data-toggle="tab" aria-expanded="false">Rekening</a></li>
					<li role="presentation" class=""><a href="#dokumen" aria-controls="counters" role="tab" data-toggle="tab" aria-expanded="false">Dokumen</a></li>
					<li role="presentation" class=""><a href="#editPassword" aria-controls="counters" role="tab" data-toggle="tab" aria-expanded="false">Edit Password</a></li>
				</ul>
				<!-- Tab panes -->
				{!! Form::open(['url'=>'profile/edit', 'files' => true]) !!}
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="biodataDiri">
							<div class="clearfix">
								<div class="col-md-3 light">
									<div class="col-md-12 text-center">
										<img src="{{ $billers[0]->logo ? asset('img/privacy/'.$billers[0]->id.'/img_account/'.$billers[0]->logo) : asset('images/icon/default-avatar.png') }}" id="img_account" class="img-responsive" style="max-height: 200px; max-width:180px;">
									</div>
									<div class="col-md-12 text-center">
										<span class="btn-lg btn-default" style="border-radius: 5px;" onclick="$(this).parent().find('input[type=file]').click();">Pilih image</span>
											<input onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());
													document.getElementById('img_account').src = window.URL.createObjectURL(this.files[0]);" style="display: none;" accept="image/jpeg" name="img_account" type="file" value="{{ $billers[0]->logo ? $billers[0]->logo : '' }}">
										</span>
									</div>
									<p><br>Besar file maksimum 10.000.000 bytes (10 Megambytes)<br>Ekstensi file yang diperbolehkan JPG, JPEG, PNG</p>
								</div>
								<div class="col-md-9 light">
									<h3>Data Diri</h3>
									<div class="form-group">
										<label for="nama_pemilik_rekening">Nama</label>
										{!! Form::text('name', $billers[0]->name, ['class'=> 'form-control input-form', 'id'=>'name']) !!}
									</div>
									<div class="form-group">
										<label for="nama_pemilik_rekening">Email</label>
										{!! Form::text('email', $billers[0]->email, ['class'=> 'form-control input-form', 'id'=>'email']) !!}
									</div>
									<div class="form-group">
										<label for="nama_pemilik_rekening">Address</label>
										{!! Form::text('address_acc', $billers[0]->address, ['class'=> 'form-control input-form', 'id'=>'address_acc']) !!}
									</div>
									<div class="form-group">
										<label for="nama_pemilik_rekening">Status</label>
										<input type="text" disabled required="" class="form-control input-form" name="aktifasi_user" id="aktifasi_user" placeholder="Active" value="">
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 form-group pull-right">
										<br><button type="submit" class="btn btn-base btn-lg btn-block next-step" style="border-radius: 5px;">Simpan</button>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="informasiBisnis">
							<div class="clearfix">
								<div class="col-md-3 light">
									<div class="col-md-12 text-center">
										<img src="{{ $billersInfo[0]->logo ? asset('img/privacy/'.$billersInfo[0]->biller_id.'/comp_logo_img/'.$billersInfo[0]->logo) : asset('images/icon/default-avatar.png') }}" id="comp_npwp_img" class="img-responsive" style="max-height: 200px; max-width:180px;">
									</div>
									<div class="col-md-12 text-center">
										<span class="btn-lg btn-default" style="border-radius: 5px;" onclick="$(this).parent().find('input[type=file]').click();">Pilih image</span>
											<input onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());
													document.getElementById('comp_npwp_img').src = window.URL.createObjectURL(this.files[0]);" style="display: none;" accept="image/jpeg" name="logo" type="file" value="{{ $billersInfo[0]->logo ? $billersInfo[0]->logo : ''}}">
										</span>
									</div>
									<p><br>Besar file maksimum 10.000.000 bytes (10 Megambytes)<br>Ekstensi file yang diperbolehkan JPG, JPEG, PNG</p>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<label for="nama_perusahaan">Nama Badan Usaha</label>
										{!! Form::text('comp_name', $billersInfo[0]->comp_name, ['class'=> 'form-control input-form', 'id'=>'comp_name']) !!}
									</div>
									<div class="form-group">
										<label for="nama_pemilik_rekening">Nama Owner Badan Usaha</label>
										{!! Form::text('owner_name', $billersInfo[0]->owner_name ? $billersInfo[0]->owner_name : null , ['class'=> 'form-control input-form', 'id'=>'owner_name']) !!}
									</div>
									<div class="form-group">
										<label for="alamat_perusahaan">Alamat Badan Usaha / Alamat Perseorangan</label>
										{!! Form::text('address', $billersInfo[0]->address, ['class'=> 'form-control input-form', 'id'=>'address']) !!}
									</div>
									<div class="row">
										<div class="col-sm-6">
											<div class="form-group">
												<label for="alamat_perusahaan" >Provinsi</label>
												{!!Form::select('province',$provinces,$billersInfo[0]->province, ['id'=>'province','class' => 'form-control input-form']) !!}
											</div>
										</div>
										<div class="col-sm-6">
											<div class="form-group">
												<label for="alamat_perusahaan">Kota</label>
												{!!Form::select('city',$cities,$billersInfo[0]->city, ['id'=>'city','class' => 'form-control input-form']) !!}
											</div>
										</div>
									</div>
									<div class="form-group">
										<label for="no_tlp_perusahaan">Nomer Telepon Badan Usaha</label>
										{!! Form::number('comp_phone', $billersInfo[0]->comp_phone, ['class'=> 'form-control input-form', 'id'=>'comp_phone']) !!}
									</div>
									<div class="form-group">
										<label for="npwp_perusahaan">NPWP Perusahaan</label>
										{!! Form::text('comp_npwp', $billersInfo[0]->comp_npwp, ['class'=> 'form-control input-form', 'id'=>'comp_npwp']) !!}
										<div class="help-block with-errors"></div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 form-group pull-right">
										<br><button type="submit" class="btn btn-base btn-lg btn-block next-step" style="border-radius: 5px;">Simpan</button>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="rekening">
							<div class="clearfix">
								<div class="col-md-12">
									<div class="form-group">
										<label class="col-sm-2" for="nama_bank">Nama Bank</label>
										<div class="col-md-4">
											<select id="bankName" name="bank_name" id="bank_name" class="form-control input-form">
												<option>Nama Bank</option>
												<option value="BANK BCA" {{ $bank_name == 'BANK BCA' ? 'selected' : '' }}> BANK BCA </option>
												<option value="BANK BNI" {{ $bank_name == 'BANK BNI' ? 'selected' : '' }}> BANK BNI </option>
												<option value="BANK Mandiri" {{ $bank_name == 'BANK Mandiri' ? 'selected' : '' }}> BANK Mandiri </option>
												<option value="BANK BRI" {{ $bank_name == 'BANK BRI' ? 'selected' : '' }}> BANK BRI </option>
												<option value="BANK NIAGA" {{ $bank_name == 'BANK NIAGA' ? 'selected' : '' }}> BANK NIAGA </option>
												<option value="BANK BTN" {{ $bank_name == 'BANK BTN' ? 'selected' : '' }}> BANK BTN </option>
											</select>
										</div>
									</div><br><br>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="col-sm-2" for="nomor_rek">Nomor Rekening</label>
										<div class="col-md-4">
											{!! Form::text('bank_acc_no', $billersInfo[0]->bank_acc_no, ['class'=> 'form-control input-form', 'id'=>'bank_acc_no']) !!}
										</div>
									</div><br><br>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="col-sm-2" for="nama_pemilik_rek">Nama Pemilik Rekening</label>
										<div class="col-md-4">
											{!! Form::text('bank_acc_name', $billersInfo[0]->bank_acc_name, ['class'=> 'form-control input-form', 'id'=>'bank_acc_name']) !!}
										</div>
									</div><br><br>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label class="col-sm-2" for="cabang">Cabang</label>
										<div class="col-md-4">
											{!! Form::text('bank_cabang', $billersInfo[0]->bank_cabang, ['class'=> 'form-control input-form', 'id'=>'bank_cabang']) !!}
										</div>
									</div><br><br>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 form-group pull-right">
										<br><button type="submit" class="btn btn-base btn-lg btn-block next-step" style="border-radius: 5px;">Simpan</button>
									</div>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="dokumen">
							<div class="clearfix">
			
							</div>
						</div>
						
						{!! Form::close() !!}
						<div role="tabpanel" class="tab-pane" id="editPassword">
							<div class="clearfix">
								{!! Form::open(['url'=>'akun/edit/password', 'files' => true]) !!}
								<div class="col-md-12">
									<div class="form-group">
										<label for="nama_perusahaan">Password Lama</label>
										{!! Form::password('oldPassword', ['class'=>'form-control input-form', 'required', 'id'=>'oldPassword']) !!}
									</div>
									<div class="form-group">
										<label for="alamat_perusahaan">Password Baru</label>
										{!! Form::password('newPassword', ['class'=>'form-control input-form', 'required', 'id'=>'password']) !!}
									</div>
									<div class="form-group">
										<label for="alamat_perusahaan" >Konfirmasi Password Baru</label>
										{!! Form::password('confirmNewPassword', ['class'=>'form-control input-form', 'required', 'id'=>'confirmNewPassword']) !!}
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 form-group pull-right">
										<br><button type="submit" class="btn btn-base btn-lg btn-block next-step" style="border-radius: 5px;">Update Password</button>
									</div>
								</div>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
</div>
@endsection