@extends('layout')

@section('pageTitle')
	Profile - Finbox
@stop

@section('sidebarActiveProfile')
    class="active"
@stop

@section('content')
<br><br>
<div class="col-md-12">
	<div class="col-md-6">
		<div class="form-group">
			<label for="nama_bank">Nama Bank</label>
			<select required="" class="form-control input-lg" name="nama_bank" id="nama_bank">
				<option value="BANK BCA"> BANK BCA </option>
				<option value="BANK GANESHA"> BANK GANESHA </option>
				<option value="BANK OF TOKYO"> BANK OF TOKYO </option>
				<option value="BANK INA PERDANA"> BANK INA PERDANA </option>
				<option value="BANK VICTORIA"> BANK VICTORIA </option>
				<option value="BANK MASPION"> BANK MASPION </option>
				<option value="BANK MAYORA"> BANK MAYORA </option>
				<option value="BANK SINAR HARAPAN BALI"> BANK SINAR HARAPAN BALI </option>
				<option value="BANK NOBU"> BANK NOBU </option>
				<option value="BANK ANTARDAERAH"> BANK ANTARDAERAH </option>
				<option value="BANK DKI"> BANK DKI </option>
				<option value="BANK BJB"> BANK BJB </option>
				<option value="BANK DIY"> BANK DIY </option>
				<option value="BANK JATENG"> BANK JATENG </option>
				<option value="BANK JATIM"> BANK JATIM </option>
				<option value="BANK BPD BALI"> BANK BPD BALI </option>
				<option value="BANK BRI"> BANK BRI </option>
				<option value="BANK BRI SYARIAH"> BANK BRI SYARIAH </option>
				<option value="CITIBANK"> CITIBANK </option>
				<option value="BANK MEGA SYARIAH"> BANK MEGA SYARIAH </option>
				<option value="POSINDO"> POSINDO </option>
				<option value="BANK BPRKS"> BANK BPRKS </option>
				<option value="BANK PERMATA"> BANK PERMATA </option>
				<option value="BANK SYARIAH MANDIRI"> BANK SYARIAH MANDIRI </option>
				<option value="BANK MEGA"> BANK MEGA </option>
				<option value="BANK BTN"> BANK BTN </option>
				<option value="BANK BJB SYARIAH"> BANK BJB SYARIAH </option>
				<option value="BANK SULUT"> BANK SULUT </option>
				<option value="BANK NTB"> BANK NTB </option>
				<option value="BANK NTT"> BANK NTT </option>
				<option value="BANK MANDIRI"> BANK MANDIRI </option>
				<option value="BANK CIMB NIAGA"> BANK CIMB NIAGA </option>
				<option value="BANK OCBC NISP"> BANK OCBC NISP </option>
				<option value="BANK MAYAPADA"> BANK MAYAPADA </option>
				<option value="BANK MESTIKA"> BANK MESTIKA </option>
				<option value="BANK UOB    INDONESIA"> BANK UOB    INDONESIA </option>
				<option value="BANK PANIN"> BANK PANIN </option>
				<option value="BANK HIMPUNAN SAUDARA"> BANK HIMPUNAN SAUDARA </option>
				<option value="BANK PRIMAMASTER"> BANK PRIMAMASTER </option>
				<option value="BANK BPD ACEH"> BANK BPD ACEH </option>
				<option value="BANK NAGARI"> BANK NAGARI </option>
				<option value="BANK SUMSEL BABEL"> BANK SUMSEL BABEL </option>
				<option value="BANK SUMUT"> BANK SUMUT </option>
				<option value="BANK BNI"> BANK BNI </option>
				<option value="BANK BII"> BANK BII </option>
				<option value="STANDARD CHARTERED BANK"> STANDARD CHARTERED BANK </option>
				<option value="BANK MUAMALAT"> BANK MUAMALAT </option>
				<option value="BANK ARTHA GRAHA"> BANK ARTHA GRAHA </option>
				<option value="BANK HSBC"> BANK HSBC </option>
				<option value="BANK HSBC"> BANK HSBC </option>
				<option value="BANK EKONOMI RAHARJA"> BANK EKONOMI RAHARJA </option>
				<option value="BANK DANAMON"> BANK DANAMON </option>
				<option value="BANK BUKOPIN"> BANK BUKOPIN </option>
				<option value="RABOBANK"> RABOBANK </option>
				<option value="BANK NUSANTARA PARAHYANGAN"> BANK NUSANTARA PARAHYANGAN </option>
				<option value="BANK BRI AGRO"> BANK BRI AGRO </option>
				<option value="MNC BANK"> MNC BANK </option>
				<option value="BANK BNI SYARIAH"> BANK BNI SYARIAH </option>
				<option value="BANK BTPN"> BANK BTPN </option>
				<option value="BANK KALSEL"> BANK KALSEL </option>
				<option value="BANK KALTIM"> BANK KALTIM </option>
				<option value="BANK KESEJAHTERAAN EKONOMI"> BANK KESEJAHTERAAN EKONOMI </option>
				<option value="BANK ARTOS INDONESIA"> BANK ARTOS INDONESIA </option>
				<option value="BANK DINAR INDONESIA"> BANK DINAR INDONESIA </option>
				<option value="BANK SAHABAT SAMPOERNA"> BANK SAHABAT SAMPOERNA </option>
				<option value="BANK SBI INDONESIA"> BANK SBI INDONESIA </option>
				<option value="BANK ROYAL INDONESIA"> BANK ROYAL INDONESIA </option>
				<option value="BANK YUDHA BHAKTI"> BANK YUDHA BHAKTI </option>
				<option value="BANK MITRANIAGA"> BANK MITRANIAGA </option>
				<option value="BANK JASA JAKARTA"> BANK JASA JAKARTA </option>
				<option value="BANK ANDARA"> BANK ANDARA </option>
				<option value="BANK BISNIS INTERNASIONAL"> BANK BISNIS INTERNASIONAL </option>
				<option value="BANK JABAR BANTEN SYARIAH"> BANK JABAR BANTEN SYARIAH </option>
				<option value="BANK QNB KESAWAN"> BANK QNB KESAWAN </option>
				<option value="BANK ANGLOMAS INTERNASIONAL"> BANK ANGLOMAS INTERNASIONAL </option>
				<option value="BANK SAHABAT PURBA DANARTA"> BANK SAHABAT PURBA DANARTA </option>
				<option value="BANK MULTI ARTA SENTOSA"> BANK MULTI ARTA SENTOSA </option>
				<option value="BANK INDEX SELINDO"> BANK INDEX SELINDO </option>
				<option value="BANK PUNDI INDONESIA"> BANK PUNDI INDONESIA </option>
				<option value="CENTRATAMA NASIONAL BANK"> CENTRATAMA NASIONAL BANK </option>
				<option value="BANK FAMA INTERNASIONAL"> BANK FAMA INTERNASIONAL </option>
				<option value="BANK CHINATRUST INDONESIA"> BANK CHINATRUST INDONESIA </option>
				<option value="BANK MAYBANK INDOCORP"> BANK MAYBANK INDOCORP </option>
				<option value="BANK HARDA INTERNASIONAL"> BANK HARDA INTERNASIONAL </option>
			</select>
			<div class="help-block with-errors"></div>
		</div>
		<div class="form-group">
			<label for="no_rekening">Nomor Rekening</label>
			<input type="text" required="" class="form-control input-lg" name="no_rekening" id="no_rekening" placeholder="Nomor Rekening">
			<div class="help-block with-errors"></div>
		</div>
		<div class="form-group">
			<label for="nama_pemilik_rek">Nama Pemilik Rekening</label>
			<input type="text" required="" class="form-control input-lg" name="nama_pemilik_rek" id="nama_pemilik_rek" placeholder="Nama Pemilik Rekening">
			<div class="help-block with-errors"></div>
		</div>
		<div class="form-group">
			<label for="cabang">Cabang</label>
			<input required="" type="text" class="form-control input-lg" name="cabang" id="cabang" placeholder="Cabang">
			<div class="help-block with-errors"></div>
		</div>
	</div>
	<div class="col-md-2"></div>
	<div class="col-md-6">
		<h2>Upload dokumen</h2>
		<hr class="colorgraph-red">
		<div class="row">
		<div class="col-md-12">
			<div class="col-md-3">
				<div class="row" style="display: block;clear: both;margin-bottom: 5px;">
					<div class="col-xs-12">
					<img id="imgktp" class="img-responsive" style="max-height: 150px;height: 100px;width:70px">
					</div>
				</div>
				<div class="input-group">
					<label for="ktp">KTP</label>
					<div>
						<span class="input-group-btn">
						<span class="btn-sm btn-primary" onclick="$(this).parent().find('input[type=file]').click();">Pilih image</span>
							<input onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());
									document.getElementById('imgktp').src = window.URL.createObjectURL(this.files[0]);" style="display: none;" accept="image/jpeg" name="ktp" type="file">
						</span>
						<span class="form-control"></span>
						<br><h6>Scan KTP dalam format JPG (*) max.size :1024 KB</h6>
					</div>
				</div>
			</div>
			<div class="col-md-3">			
				<div class="row" style="display: block;clear: both;margin-bottom: 5px;">
					<div class="col-xs-12">
					<img id="imgnpwp" class="img-responsive" style="max-height: 150px;height: 100px;width:70px">
					</div>
				</div>
				<div class="input-group">
					<label for="npwp">NPWP</label>
					<div>
						<span class="input-group-btn">
						<span class="btn-sm btn-primary" onclick="$(this).parent().find('input[type=file]').click();">Pilih image</span>
							<input onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());
										document.getElementById('imgnpwp').src = window.URL.createObjectURL(this.files[0]);" style="display: none;" accept="image/jpeg" name="npwp" type="file">
						</span>
						<span class="form-control"></span>
						<br><h6>Scan NPWP dalam format JPG (*) max.size :1024 KB</h6>
					</div>
				</div>
			</div>
			<div class="col-md-3">			
				<div class="row" style="display: block;clear: both;margin-bottom: 5px;">
					<div class="col-xs-12">
					<img id="imgproduct" class="img-responsive" style="max-height: 150px;height: 100px;width:70px">
					</div>
				</div>
				<div class="input-group">
					<label for="product">Produk</label>
					<div>
						<span class="input-group-btn">
						<span class="btn-sm btn-primary" onclick="$(this).parent().find('input[type=file]').click();">Pilih image</span>
							<input onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());
									document.getElementById('imgproduct').src = window.URL.createObjectURL(this.files[0]);" style="display: none;" accept="image/jpeg" name="product" type="file">
						</span>
						<span class="form-control"></span>
						<br><h6>Upload foto produk dalam format JPG (*) max.size :1024 KB</h6>
					</div>				
				</div>
			</div>
			<div class="col-md-3">			
				<div class="row" style="display: block;clear: both;margin-bottom: 5px;">
					<div class="col-xs-12">
					<img id="imgoutlet" class="img-responsive" style="max-height: 150px;height: 100px;width:70px">
					</div>
				</div>					
				<div class="input-group">
					<label for="outlet">Foto</label>
					<div>
						<span class="input-group-btn">
						<span class="btn-sm btn-primary" onclick="$(this).parent().find('input[type=file]').click();">Pilih image</span>
							<input onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());
										document.getElementById('imgoutlet').src = window.URL.createObjectURL(this.files[0]);" style="display: none;" accept="image/jpeg" name="outlet" type="file">
						</span>
						<span class="form-control"></span>
						<h6>foto website / foto sosmed / foto toko dalam format JPG (*) max.size :1024 KB</h6>
					</div>					
				</div>
			</div>
		</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="row" style="margin-top:20px">
		<div class="col-md-4 col-md-3 col-md-3">
			<div class="checkbox">
			<label>
				<input type="checkbox" name="setuju" checked="" required=""> Aggre terms and condition
			</label>
			</div>
		</div>
		</div>
	</div>  
	<div  class="row">
		<div class="col-md-4">
			<a href="{{ url('profile/langkah1') }}">
				<button type="submit" class="btn btn-theme btn-lg btn-block"><i class="fa fa-arrow-circle-left"></i> Back</button><br><br>
			</a>
		</div>
		<div class="col-md-4 pull-right">
			<button type="submit" class="btn btn-theme btn-lg btn-block">Finish <i class="fa fa-arrow-circle-right"></i></button><br><br>
		</div>
	</div>
</div>
@endsection