@extends('layout')

@section('pageTitle')
	Ringkasan Akun - Finbox
@stop

@section('sidebarActiveRingkasanAkun')
    class="active"
@stop

@section('customPluginJs')
	<style>
		input[type="file"] {
			width: 98%;
			border-radius: 5px;
			color: #fff;
		}
		.btn-default {
			color: #333;
			background-color: #fbfbfb;
			border-color: #ccc;
		}
	</style>
@stop

@section('customPluginJs')
	<script>
	$(document).ready(function () {
		//Initialize tooltips
		$('.nav-tabs > li a[title]').tooltip();
		
		//Wizard
		$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

			var $target = $(e.target);
		
			if ($target.parent().hasClass('disabled')) {
				return false;
			}
		});

		$(".next-step").click(function (e) {

			var $active = $('.wizard .nav-tabs li.active');
			$active.next().removeClass('disabled');
			nextTab($active);

		});
		$(".prev-step").click(function (e) {

			var $active = $('.wizard .nav-tabs li.active');
			prevTab($active);

		});
	});
	</script>
@endsection

@section('content')
<div class="col-md-12">
	@if (Session::has('msg'))
		<div class="alert alert-info">{{    Session::get('msg') }}
		
		 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		</div>
	@elseif (Session::has('incorrect'))
		<div class="alert alert-danger">{{  Session::get('incorrect') }}
		
		 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		</div>
	@endif
</div>
<h4 class="text-weight-600" style="color:#666666;;margin: 20px 0px 20px 0px;">Profil Saya > Ringkasan Akun</h4>
<div class="border-dark form_in_dashboard">
	<div class="row">
		<div class="col-md-12">
			<div class="tabs-theme dark">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs text-weight-600" role="tablist" style="font-size: 18px;">
					<li role="presentation" class="active"><a href="#biodataDiri" aria-controls="services" role="tab" data-toggle="tab" aria-expanded="true">Biodata Diri</a></li>
				</ul>
				<!-- Tab panes -->
				{!! Form::open(['url'=>'profile/edit/user', 'files' => true]) !!}
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="biodataDiri">
							<div class="clearfix">
								<div class="col-md-3 light">
									<div class="col-md-12 text-center">
										<img src="{{ Auth::user()->logo ? asset('img/privacy/'.Auth::user()->id.'/img_account/'.Auth::user()->logo) : asset('images/icon/default-avatar.png') }}" id="img_account" class="img-responsive" style="max-height: 200px; max-width:180px;">
									</div>
									<div class="col-md-12 text-center">
										<span class="btn-lg btn-default" style="border-radius: 5px;" onclick="$(this).parent().find('input[type=file]').click();">Pilih image</span>
											<input onchange="$(this).parent().parent().find('.form-control').html($(this).val().split(/[\\|/]/).pop());
													document.getElementById('img_account').src = window.URL.createObjectURL(this.files[0]);" style="display: none;" accept="image/jpeg" name="img_account" type="file" value="{{ Auth::user()->logo ? Auth::user()->logo : '' }}">
										</span>
									</div>
									<p><br>Besar file maksimum 10.000.000 bytes (10 Megambytes)<br>Ekstensi file yang diperbolehkan JPG, JPEG, PNG</p>
								</div>
								<div class="col-md-9 light">
									<h3>Data Diri</h3>
									<div class="form-group">
										<label for="nama_pemilik_rekening">Nama</label>
										{!! Form::text('name', Auth::user()->name ? Auth::user()->name : '' , ['class'=> 'form-control input-form', 'id'=>'name']) !!}
									</div>
									<div class="form-group">
										<label for="nama_pemilik_rekening">Email</label>
										{!! Form::text('email', Auth::user()->email ? Auth::user()->email : '' , ['class'=> 'form-control input-form', 'id'=>'email']) !!}
									</div>
									<div class="form-group">
										<label for="nama_pemilik_rekening">Alamat</label>
										{!! Form::text('address', Auth::user()->address ? Auth::user()->address : '' , ['class'=> 'form-control input-form', 'id'=>'address']) !!}
									</div>
									<div class="form-group">
										<label for="nama_pemilik_rekening">Status</label>
										<input type="text" disabled required="" class="form-control input-form" name="aktifasi_user" id="aktifasi_user" placeholder="Active" value="">
									</div>
								</div>
								<div class="col-md-12">
									<div class="col-md-3 form-group pull-right">
										<br><button type="submit" class="btn btn-base btn-lg btn-block next-step" style="border-radius: 5px;">Simpan</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection