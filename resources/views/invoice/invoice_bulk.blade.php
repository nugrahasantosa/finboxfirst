@extends('layout')

@section('pageTitle')
	Invoice Bulk - Finbox
@stop

@section('sidebarActiveInvoice')
    class="active"
@stop

@section('content')
<h4 class="text-weight-600" style="color:#666666;;margin: 20px 0px 20px 0px;">Invoice > Bulk Invoice</h4>

<div class="border-dark form_in_dashboard">
	@if (Session::has('message'))
	<div id="sessionMessage" class="alert alert-info">
		{{ Session::get('message') }} 
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	</div>
	@elseif (Session::has('incorrect'))
		<div id="sessionMessage" class="alert alert-danger">
		{{ 	Session::get('incorrect') }}
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		</div>
	@elseif (Session::has('info'))
		<div id="sessionMessage" class="alert alert-warning">
		{{ 	Session::get('info') }}
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		</div>
	@endif
	<h4 class="header_form_in_dashboard">Upload Invoice Bulk</h4>
	<hr class="margin-bottom-xsmall hr_form_in_dashboard">
	<div class="row">
		<form action="{{ URL::to('uploadInvoice') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
		<div class="col-sm-12">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard" style="padding-left: 4%;"><h3>Invoice File</h3></label>
				
				<div class="col-sm-6">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input name="file" type="file">
					</span>
					<br><label for="file excel" class="label_form_in_dashboard">xlsx file (Excel 2007 or later format)</label>
				</div>
				
			</div><br>
		</div>
		<div class="col-sm-12">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard"><h3></h3></label>
				<div class="col-sm-3">
					<button type="submit" class="btn btn-base btn-lg btn-block">Submit</button><br>
				</div>
			</div>
		</div>
		</form>
		<div class="col-sm-12">
			<hr class="hr_form_in_dashboard" style="margin-bottom: 0px;">
			<label style="margin-top: 10px;padding-left: 2%;" for="npwp" class="col-sm-6 label_form_in_dashboard">you can download the template <a href="{{ asset('import_template/import_template_clean_v4.1.xlsx') }}" style="color: #337ab7;font-size: inherit;">here</a><br>and the example <a href="{{ asset('import_template/import_example_v4.1.xlsx') }}" style="color: #337ab7;font-size: inherit;">here</a></label>
		</div>
	</div>
</div>
@endsection