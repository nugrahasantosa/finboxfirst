<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Preview Invoice</title>
</head>
<link rel="stylesheet" href="https://sandbox.finpay.co.id/bob/public/css/vendor.css">
<link rel="stylesheet" href="https://sandbox.finpay.co.id/bob/public/css/app.css">

<script src="{{ asset('js/app-dashboard.js') }}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <!-- Include Date Range Picker --> 
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
    <div class="row">
		<div class="col-md-12">
		<div class="col-md-2">
		</div>
        <div class="col-md-8">
			<div class="col-md-12">
			@foreach($billerInfo as $biller)
            <div id="navbar">
				
	            <div class="navigation-example">
				<nav class="navbar navbar-dark stylish-color" style="margin-bottom:0px; margin-top:30px; background-color:#eee;">
					<div class="col-md-12">
						<div class="col-md-6">
							<img src="{{URL::asset('img/logo/'.$biller->id.'/'.$biller->logo)}}" alt="powered by" style="width:130px; height=70px;">
						</div>
						<div class="col-md-6">
							<div class="col-sm-6">
							<i class="fa fa-mobile fa-2x" style="padding-top:25px; margin-left:0px; color:#008bfe;">
							</i>
							<br>
							{{strtoupper($biller->phone ? $biller->phone : '+62 857 4384 4484')}}
							</div>
							<div class="col-sm-6">
								<i class="fa fa-map-marker fa-2x" aria-hidden="true"style="padding-top:25px; color:#008bfe;">
								</i>
								<br>
								{{$biller->address ? $biller->address : 'Pules Kidul, RT 01/03, Donokerto, Turi Sleman, Yogyakarta'}}
								
							
							</div>
						</div>
					</div>
				</nav>
				@endforeach
				@foreach($invoiceInfo as $info)
	                <nav class="navbar navbar-inverse" style="border-radius:3px; border-bottom:0px; margin-bottom
					:0px;">
	                    <!-- Brand and toggle get grouped for better mobile display --> 
						 <div class="col-md-12">
							<div class="col-sm-8">
							  <p style="margin-top:15px; margin-bottom:0px; margin-right:0px;">DITAGIHKAN KEPADA :
							  <br>
							  <b><h4 style="margin-top:0px; margin-bottom:0px;">{{strtoupper($info->recipient_name ? $info->recipient_name : '** Recipient Name **')}}</h4></b>
							 
							  <br>
							  {{ $info->recipient_email ? $info->recipient_email : '** Recipient Email **' }}</p>
						    </div>
							<div class="col-sm-4" style="align:right;">
								<textarea style="background-color:#212121; color:#00bcd4; font-weight: bold; text-align:left; font-size:25px; border: 0; overflow: hidden; width:140px; resize:none; margin-top:15px;">INVOICE</textarea>
								<br>
								<textarea style="background-color:#212121; text-align:left;border: 0; overflow: hidden; resize: none;">No Invoice : {{ $info->invoice_number ? $info->invoice_number : 'NU GR AHA' }}</textarea>
							</div>
						  
						  </div>
	                </nav>
	    <!--        end rose navbar -->

	    <!--        info navbar -->
					<nav class="navbar navbar-dark stylish-color" style="background-color:#424346; border-radius:0px; border-bottom:0px; margin-bottom
					:0px; min-height:60px;">
							<div class="col-md-12">
								<div class="col-sm-4">
								  <p style="color:#fff; margin-top:10px;">TANGGAL INVOICE : <?php 
								  $format = 'Y-m-d H:i:s';
								  $date = DateTime::createFromFormat($format, $info->created_at);
								  echo $date->format('d F Y')? $date->format('d F Y') : 'NU GR AHA';;
								  ?></p>
								</div>
								<div class="col-sm-4">
								   <b><p style="color:#fff; margin-top:10px;">TOTAL : Rp. {{ number_format($info->total,2,",",".") }}
								  </b>
								</div>
								<div class="col-sm-4" style="align:right;">
								   <p style="color:#fff; margin-top:10px;">TANGGAL JATUH TEMPO: <?php 
								  $format = 'Y-m-d H:i:s';
								  $date = DateTime::createFromFormat($format, $info->due_date);
								  echo $date->format('d F Y') ? $date->format('d F Y') : 'NU GR AHA';
								  ?></p>
								</div>
							  
							  </div>
						</div>
					</nav>
					@endforeach
	    <!--        end info navbar -->
					<div class="table-responsive no-margin">
					<table class="table table-striped" style="padding-left:20px;">
					
					  <thead style="background-color:#00bcd4; color:#fff;">
						<tr>
						  <th style="padding-left:45px; font-family:calibri;"><b>testNO. TRANSAKSI</b></th>
						  <th style="font-family:calibri;"><b>TANGGAL</b></th>
						  <th style="font-family:calibri;"><b>DESKRIPSI</b></th>
						  <th style="padding-right:45px; font-family:calibri;"><b>JUMLAH</b></th>
						  </b>
						</tr>
					  </thead>
					
					 <tbody>
					 <?php $total=0 ?> 
					 @foreach($previewInvoice as $previewDraft)
						<tr>
						  <th scope="row" style="padding-left:45px;">{{$previewDraft->invoice_number}}</th>
						  <td><?php 
								  $format = 'Y-m-d H:i:s';
								  $date = DateTime::createFromFormat($format, $previewDraft->created_at);
								  echo $date->format('d-m-Y') ? $date->format('d-m-Y') : 'NU GR AHA';
								  ?></td>
						  <td>{{$previewDraft->name}}</td>
						  
						  <td>Rp. {{number_format($previewDraft->amount,2,",",".")}}</td>
						</tr>
						<?php $total +=$previewDraft->amount ?>
						@endforeach
						<tr>
						  <th scope="row" style="padding-left:45px;"></th>
						  <td></td>
						  <td><b>Total</b></td>
						  <td><b>Rp. {{ number_format($total,2,",",".")}}</b></td>
						</tr>
						
					  </tbody>
					 
					</table>
					</div>
					 <!--        info navbar -->
					 @foreach($billerInfo as $biller)
					<nav class="navbar navbar-dark stylish-color" style="border-radius:0px; border-bottom:0px; margin-bottom
					:0px; min-height:60px;">
							<div class="col-md-12">
								<div class="col-sm-4">
								  
								   
								  <p><h3 style="color:#00bcd4; margin-top:10px;">TERIMA KASIH</h3></p>
								  <p>
								  <b>SYARAT & KETENTUAN</b></p>
								  <textarea style="text-align:justify; border: 0; overflow: hidden; width:330px; height:150px; resize:none;">{{$biller->terms ? $biller->terms : '-'}}</textarea>
								  <p style="text-align:justify;">
								  

								  </p>
								</div>
								<div class="col-sm-4">
								<p><h5 style="color:#00bcd4; margin-top:10px;"><b>Notes</b></h5></p>
								   <textarea style="text-align:justify; border: 0; overflow: hidden; width:330px; height:auto; resize:none;">{{ $biller->notes ? $biller->notes : '-' }} </textarea>
								   <a class="btn btn-info" aria-haspopup="true" aria-expanded="false" href="{{$payment_link}}" id="cancelDraftInvoice">
								Pay
								</a>
								</div>
								<div class="col-sm-4">
								 <p style="color:#00bcd4;margin-right:55px;margin-top:100px;">Powered by :</p>
								 <img src="{{URL::asset('img/logo/logo.png')}}" alt="powered by" style="width:130px; height=70px;">
								</div>
							  
							</div>
					</nav>
	    <!--        end info navbar -->
				@endforeach
				<nav class="navbar" style="border-radius:0px; border-bottom:0px; margin-bottom:0px; background-color:#00bcd4; min-height:20px;">
	                    <!-- Brand and toggle get grouped for better mobile display -->
	                    
						<div class="col-md-12">
	                    </div>
	
	             </nav>
				 <nav class="navbar navbar-inverse" style="border-radius:0px; border-bottom:0px; margin-bottom:0px; min-height:30px;">
	                    <!-- Brand and toggle get grouped for better mobile display -->
	                    
						<div class="col-md-12">
	                    </div>
							
	             </nav>
				 
	            </div>
	        </div>
		
		</div>
		<div class="col-md-2" style="margin-top:50px;">
		</div>
	</div>
    </div>

