@extends('layout')

@section('pageTitle')
	Invoice - Finbox
@stop

@section('sidebarActiveInvoice')
    class="active"
@stop

@section('content')
<h4 style="color:#666666;;margin: 20px 0px 20px 0px;">Biller > Buat Invoice > Invoice Satuan</h4>
<div class="border-dark form_in_dashboard">
	<div class="row">
		<div class="col-sm-12">
			<div class="margin-top-small text-center">
				<h1 class="color_text_confirmation_p">Selamat!</h1>
			</div>
		</div>
		<div class="col-sm-4"></div>
		<div class="col-sm-4 margin-bottom-0">
			<img src="{{ URL::asset('images/icon/successcreate.png') }}" width="300" data-bgposition="center center" class="img-responsive">
		</div>
		<div class="col-sm-4"></div>
		<div class="col-sm-12">
			<div class="text-center">
				<br><h4 class="color_text_confirmation_p">Invoice sudah berhasil dibuat dan akan<br>dikirimkan ke pelanggan Anda.</h4>
			</div>
		</div>
		<div class="col-sm-4"></div>
		<div class="col-sm-4 text-center margin-bottom-medium">
			<a href="{{ url('auth/login') }}" class="btn btn-base btn-lg daftar text-weight-600 box-shadow-active radius_input" style="width: 80%;border-radius: 50px;">Oke</a>
		</div>
	</div>
</div>
@endsection