@extends('layout')

@section('pageTitle')
	Invoice - Finbox
@stop

@section('sidebarActiveInvoice')
    class="active"
@stop

@section('customPluginJs')
	<script type="text/javascript" src="{{ URL::asset('js/app-invoice-simple.js') }}"></script>

	<script>
		$(function() {		
		  $('input[name="invoiceDate"]').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			minYear: 1901,
			maxYear: parseInt(moment().format('MM/DD/YYYY'),10)
		  });
		});
		$(function() {		
		  $('input[name="invoiceDueDate"]').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			minYear: 1901,
			maxYear: parseInt(moment().format('MM/DD/YYYY'),10)
		  });
		});
		setTimeout(function() {
		    $('#sessionMessage').fadeOut('fast');
		}, 15000); 
	</script>
	<script src="{{ URL::asset('js/sweetalert2.all.js') }}"></script>
  <!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
  <script src="https://unpkg.com/promise-polyfill"></script>
@endsection
	
@section('content')
@if (Session::has('success'))
	<div id="sessionMessage" class="alert alert-info">
		{{ Session::get('success') }} 
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	</div>
	@elseif (Session::has('incorrect'))
		<div id="sessionMessage" class="alert alert-danger">
		{{ 	Session::get('incorrect') }}
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		</div>
	@elseif (Session::has('info'))
		<div id="sessionMessage" class="alert alert-warning">
		{{ 	Session::get('info') }}
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		</div>
	@endif
<h4 class="text-weight-600" style="color:#666666;;margin: 20px 0px 20px 0px;">Invoice > Invoice Satuan</h4>
{!! Form::open(['url'=>'createSimpleInvoice', 'class'=>'form-horizontal', 'id'=>'simpleInvoiceForm']) !!}
<div id="simple-invoice-form" class="simple-invoice-form">
<div class="border-dark form_in_dashboard">
	<h4 class="header_form_in_dashboard">Informasi Penerima Invoice</h4>
	<hr class="margin-bottom-xsmall hr_form_in_dashboard">
	<div class="row main-form-row" id="main-form-row">
		<div class="col-sm-12" style="padding-right: 4%;">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard">Invoice To  <sup style="color: red">*</sup></label>
				<div class="col-sm-3">
					{!!Form::select('clients[]',$clients,'Pilih Client', ['class' => 'form-control input-form radius_form_in_dashboard', 'required']) !!}
				</div>
				<div class="col-sm-3">
					<button type="button" class="btn btn-base" data-toggle="modal" data-target="#modalPenerima" style="border-radius: 20px;"><i class="fa fa-plus"></i> Tambah Penerima</button>
				</div>
			</div>
			<div class="row hide clientInfo">
			<label class="col-md-3 label-on-right"></label>
			<div class="col-md-3" style="padding-left:25px;">
				<div class="form-group label-floating is-empty">
					<span id="recipientName" class="recipientName" style="visibility:hidden"></span>
					<label class="control-label"></label>
					<div><i class="fa fa-envelope-o" style="font-size:16px;"></i> &nbsp;&nbsp;<span class="clientEmail" id="clientEmail" style="font-size:16px;"></span></div>
					<div><i class="fa fa-phone"></i> &nbsp;&nbsp;<span class="clientPhone" id="clientPhone" style="font-size:16px;"></span></div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group label-floating is-empty">
					<label class="control-label"></label>
					<div><i class="fa fa-location-arrow" style="font-size:16px;"></i> &nbsp;&nbsp; <span class="clientAddress" id="clientAddress" style="font-size:16px;"></span></div>
					<div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="clientCity"></span></div>
					<div>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="clientProvince" id="clientProvince" style="font-size:16px;"></span> <span class="clientZipcode" id="clientZipcode"style="font-size:16px;"></span></div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
<div class="border-dark form_in_dashboard">
	<h4 class="header_form_in_dashboard">Format Invoice</h4>
	<hr class="margin-bottom-xsmall hr_form_in_dashboard">
	
	<div class="row">
		{{ Form::hidden('_token', 'csrf_token()', array('id' => '_token')) }}
		{{ Form::hidden('invoiceId', $invoiceId, array('id' => 'invoiceId')) }}
		{{ Form::hidden('siteUrl', URL::to('/'), array('id' => 'siteUrl')) }}
		
		<div class="col-sm-12" style="padding-right: 4%;">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard">Nomor Invoice  <sup style="color: red">*</sup><br><p style="color:#c0b8b1">nomor invoice ini di generate secara otomatis oleh sistem.</p></label><br>
				<div class="col-sm-6" id="invoiceDiv">
					<b><span class="invoiceNumber" id="invoiceNumber">{!! $invoiceNumber !!}</span></b>
				</div>
			</div>
		</div>
		<div class="col-sm-12" style="padding-right: 4%;">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard">Tanggal Invoice  <sup style="color: red">*</sup></label>
				<div class="col-sm-3">
					{!! Form::text('invoiceDate', $period, ['id' => 'invoiceDate','class' => 'form-control input-form radius_form_in_dashboard', 'required']) !!}
				</div>
				<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard">Tanggal Kadaluarsa  <sup style="color: red">*</sup></label>
				<div class="col-sm-3">
					{!! Form::text('invoiceDueDate', $dueDate, ['id' => 'invoiceDueDate','class' => 'form-control input-form radius_form_in_dashboard', 'required']) !!}
				</div>
			</div>
		</div>
	</div>
</div>
<div class="border-dark form_in_dashboard">
	<h4 class="header_form_in_dashboard">Informasi Produk</h4>
	<hr class="margin-bottom-xsmall hr_form_in_dashboard">
	<div class="row">
		<div class="col-sm-12" style="padding-left: 4%;padding-right: 4%;">
			<table class="table invoice-item-table table-responsive" border="0">
				<thead>
					<tr>
						<th class="item-name">Nama Produk</th>
						<th class="item-name">Deskripsi Produk</th>
						<th>Jumlah</th>
						<th class="item-total text-left">Harga Item <small>(Rp)</small></th>
						<th class="item-total text-left">Ppn</th>
						<th class="item-total text-left">Total <small>(Rp)</small></th>
						<th class="item-action text-center"><!-- action --></th>
					</tr>
				</thead>
				<tbody>
					<tr class="invoice-item-row">
						<td>{!! Form::text('name[]', null, ['class'=>'form-control', 'required']) !!}</td>
						<td>{!! Form::text('description[]', null, ['class'=>'form-control', 'required']) !!}</td>
						<td>{!! Form::text('countItem[]', null, ['class'=>'form-control', 'required']) !!}</td>
						<td class="text-left">{!! Form::text('subTotal[]', null, ['class'=>'form-control text-left thousand-separator', 'required']) !!}</td>
						<td class="text-left">{!! Form::text('ppn[]', null, ['class'=>'form-control text-left thousand-separator', 'required']) !!}</td>
						<td class="text-left">{!! Form::text('total[]', null, ['class'=>'form-control text-left thousand-separator', 'required']) !!}</td>
						<td class="td-actions text-center">
							<button type="button" class="btn btn-danger btn-just-icon btn-round removeItemBtn" title="hapus"><i class="material-icons">close</i></button>
						</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td><button type="button" class="btn btn-base btn-sm btn-round addItemBtn"><i class="fa fa-plus"></i></button></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="text-right"style="font-size:18px;"><strong>Sub Total <b style="font-size:14px;">(Rp)</b></strong></td>
						<td class="subtotal text-right" id="subtotal"></td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="text-right"style="font-size:18px;"><strong>Discount <b style="font-size:14px;">(Rp)</b></strong></td>
						<td class="discount text-right">{!! Form::text('discount[]', 0, ['class'=>'form-control text-right thousand-separator', 'required', 'placeholder'=>'0']) !!}</td>
						<td></td>
					</tr><tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td class="text-right"style="font-size:18px;"><strong>Grand Total <b style="font-size:14px;">(Rp)</b></strong></td>
						<td class="grandTotal text-right" id="grandTotal"></td>
						<td></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
<div class="border-dark form_in_dashboard">
	<h4 class="header_form_in_dashboard">Informasi Pengiriman Invoice</h4>
	<hr class="margin-bottom-xsmall hr_form_in_dashboard">
	<div class="row">
		<div class="col-sm-12">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard">Tipe Invoice  <sup style="color: red">*</sup><br><p style="color:#c0b8b1">memungkinkan untuk Anda memilih tipe invoice simple atau invoice lengkap.</p></label><br>
				<div class="col-sm-6">
					<label class="radio-inline">
					{!! Form::radio('template[]', '1',['id' => 'template','class' => 'form-control input-form radius_form_in_dashboard','required']) !!} Template 1
					</label>
					<label class="radio-inline">
						{!! Form::radio('template[]', '2',['id' => 'template','class' => 'form-control input-form radius_form_in_dashboard','required']) !!} Template 2
					</label>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard">Dikirim Dari  <sup style="color: red">*</sup></label>
				<div class="col-sm-6">
					<label class="checkbox checkbox-inline">
						{!! Form::checkbox('isSendEmail[]', true, true, ['id' => 'isSendEmail','disabled']) !!} E-mail
					</label>
					<label class="checkbox checkbox-inline">
						{!! Form::checkbox('isSendSms[]', true, false, ['id' => 'isSendSms']) !!} SMS
					</label>
				</div>
			</div>
		</div>
		<div class="col-sm-12">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard">Recurring  <sup style="color: red">*</sup><br></label>
				<div class="col-sm-6">
					<label class="radio-inline" class="col-sm-2">
						{!! Form::radio('isRecurring[]', '4',['id' => 'isRecurring','class' => 'form-control input-form radius_form_in_dashboard','required']) !!} Ya
						
					</label>
					<label class="radio-inline" class="col-sm-2">
						{!! Form::radio('isRecurring[]', '0',['id' => 'isRecurring','class' => 'form-control input-form radius_form_in_dashboard','required']) !!} Tidak
					</label>
				</div>
			</div>
		</div>
		<div class="col-sm-12 margin-bottom-xsmall">
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard">Notes Invoice</label>
				<div class="col-sm-6">
					{!! Form::textarea('notes[]', 'test', ['id'=>'notes','class' => 'form-control input-form radius_form_in_dashboard', 'required', 'placeholder'=>'Notes Invoice', 'rows'=>'0']) !!}
				</div>
			</div>
		</div>
		<div class="col-sm-12 margin-bottom-xsmall" >
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard">Email CC </label>
				<div class="col-sm-9">
					{!! Form::text('recipientEmailCC[]', null, ['class'=>'form-control input-form radius_form_in_dashboard']) !!}
					<p style="color:#c0b8b1;font-size: 13px;">dipisahkan dengan tanda , untuk penerima invoice lebih dari 1 penerima</p>
				</div>
			</div><br>
		</div>
		<div class="col-sm-12">
			<div class="form-group">
				<p style="color:#c0b8b1;padding-left:2%;font-size:13px">Note : Invoice akan dikirimkan dengan detail yang sama setiap bulannya, sesuai dengan jadwal pengirimannya</p>
			</div>
		</div>
	</div>
</div><br>
<div class="col-md-12" style="padding-right:0px">
	<div class="col-md-2 pull-right" style="padding-right:0px">
		<button type="submit" class="btn btn-base btn-lg btn-block">Kirim</button><br>
	</div>
	<div class="col-md-2 pull-right" style="padding-right:0px">
		<button type="submit" data-draft="1" class="btn btn-lg btn-dark btn-block">Simpan</button><br>
	</div>
</div>
{!! Form::close() !!}
</div>
{!! Form::open(['url'=>'addClient', 'class'=>'form-horizontal', 'id'=>'addClient']) !!}
<div class="modal fade" id="modalPenerima" tabindex="-1" role="dialog" style="display: none;">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
				<h4 class="modal-title">Informasi Penerima Invoice</h4>
			</div>
			<div class="modal-body" style="height:auto;">
				<div class="row">
					<div class="col-sm-12" style="padding-right: 4%;margin-bottom: 17px;">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard">Nama Penerima  <sup style="color: red">*</sup></label>
							<div class="col-sm-2">
								{!! Form::select('salutation',$salutation,'Tn', ['class' => 'form-control input-form radius_form_in_dashboard', 'required']) !!}
							</div>
							<div class="col-sm-7">
								{!! Form::text('recipientName', null, ['class' => 'form-control input-form radius_form_in_dashboard', 'required', 'placeholder'=>'Masukkan Nama Penerima']) !!}
							</div>
						</div>
					</div>
					<div class="col-sm-12" style="padding-right: 4%;margin-bottom: 17px;">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard">Email Penerima  <sup style="color: red">*</sup></label>
							<div class="col-sm-9">
								{!! Form::email('recipientEmail', null, ['class' => 'form-control input-form radius_form_in_dashboard', 'required', 'placeholder'=>'Masukkan email penerima']) !!}
							</div>
						</div>
					</div>
					<div class="col-sm-12" style="padding-right: 4%;margin-bottom: 17px;">
						<div class="form-group">
							<label for="inputAlamat" class="col-sm-3 label_form_in_dashboard">Alamat Penerima  <sup style="color: red">*</sup></label>
							<div class="col-sm-9">
								{!! Form::text('billing_address', null, ['class' => 'form-control input-form radius_form_in_dashboard', 'required', 'placeholder'=>'Masukkan Alamat penerima']) !!}
							</div>
						</div>
					</div>
					<div class="col-sm-12" style="padding-right: 4%;margin-bottom: 17px;">
						<div class="form-group">
							<label for="inputProvince" class="col-sm-3 label_form_in_dashboard">Provinsi  <sup style="color: red">*</sup></label>
							<div class="col-sm-3">
								{!!Form::select('province',$provinces,null, ['id'=>'province','class' => 'form-control input-form radius_form_in_dashboard', 'required']) !!}
							</div>
							<label for="inputKota" class="col-sm-3 label_form_in_dashboard">Kota  <sup style="color: red">*</sup></label>
							<div class="col-sm-3">
								{!!Form::select('city',$cities,null, ['id'=>'city','class' => 'form-control input-form radius_form_in_dashboard', 'required']) !!}
							</div>
						</div>
					</div>
					<div class="col-sm-12" style="padding-right: 4%;margin-bottom: 17px;">
						<div class="form-group">
							<div class="col-sm-6"></div>
							<label for="inputKodePos" class="col-sm-3 label_form_in_dashboard"> Kode Pos</label>
							<div class="col-sm-3">
								{!! Form::number('zipcode', null, ['id'=>'zipcode','class' => 'form-control input-form radius_form_in_dashboard', 'placeholder'=>'Kode Pos']) !!}
							</div>
						</div>
					</div>
					<div class="col-sm-12" style="padding-right: 4%;margin-bottom: 17px;">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-3 label_form_in_dashboard">Nomor Telepon Penerima</label>
							<div class="col-sm-6">
								{!! Form::text('recipientPhone', null, ['class' => 'form-control input-form radius_form_in_dashboard', 'placeholder'=>'Masukkan nomor telepon penerima']) !!}
							</div>
						</div>
					</div>
					
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-md-3 pull-right">
					<button type="submit" class="btn btn-base btn-lg btn-block">Simpan</button>
				</div>
			</div>
		</div>
	</div>
	{!! Form::close() !!}
</div>
@endsection